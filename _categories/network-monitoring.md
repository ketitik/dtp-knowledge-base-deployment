---
key: network-monitoring
name: Network Monitoring
icon: network
order: 3
sub_category:
    - name: Dashboard
      key: dashboard
      icon: dashboard
    - name: Link Utilization
      key: link-utilization
      icon: calendar
    - name: Starred Link
      key: starred-link
      icon: calendar
    - name: CPU Status
      key: cpu-status
      icon: calendar
    - name: RAM Status
      key: ram-status
      icon: calendar
    - name: Storage Status
      key: storage-status
      icon: calendar
    - name: DiskIO Status
      key: diskio-status
      icon: calendar
    - name: Network Map
      key: network-map
      icon: calendar
    - name: Location
      key: location
      icon: calendar
    - name: Network Device
      key: network-device
      icon: calendar
    - name: Network Probe
      key: network-probe
      icon: calendar
    - name: Predictive Analysis
      key: predictive-analysis
      icon: calendar
    - name: Reporting
      key: reporting
      icon: calendar
    - name: Alerting
      key: alerting
      icon: calendar
---