---
key: website-monitoring
name: Website Monitoring
icon: website-monitoring
order: 3
sub_category:
    - name: General
      key: general
      icon: inventory
    - name: Website
      key: website
      icon: calendar
    - name: API
      key: api
      icon: calendar
    - name: Paused
      key: paused
      icon: calendar
    - name: Reporting
      key: reporting-web-api
      icon: calendar
    - name: Web / API Probe
      key: web-api-probe
      icon: calendar
    # - name: Alerting
    #   key: alerting-web-api
    #   icon: calendar
---