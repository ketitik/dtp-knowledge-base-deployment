---
key: getting-started
name: Getting Started
icon: rocket
order: 1
sub_category:
    - name: Pengenalan Netmonk Prime
      key: pengenalan-netmonk-prime
      icon: network-summary
    - name: Panduan Pengguna Baru
      key: panduan-pengguna-baru
      icon: website-monitoring
    - name: Unduh Manual Book
      key: unduh-manual-book
      icon: calendar
---