---
key: administration
name: Administration
icon: board
order: 3
sub_category:
    - name: Akun Customer Portal
      key: akun-customer-portal
      icon: inventory
    - name: Akun Netmonk Prime
      key: akun-netmonk-prime
      icon: calendar
    - name: Subscription
      key: subscription
      icon: calendar
    - name: Payment / Pembayaran
      key: payment
      icon: calendar

---