---
title: 'Di mana saya dapat menemukan daftar prediksi tren penggunaan semua indeks storage yang termonitor Netmonk Prime?'
category: 'predictive-analysis'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Akses menu Predictive Analysis → Storage'
---

# Di mana saya dapat menemukan daftar prediksi tren penggunaan semua indeks storage yang termonitor Netmonk Prime?

1. Akses menu **Predictive Analysis → Storage**

    ![Menu Predictive Analysis Storage](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B12%5Dpredictive-analysis/menu-predictive-analysis-storage-v2.png)

2. Maka akan tampil daftar prediksi tren penggunaan semua indeks *storage* yang termonitor oleh sistem seperti berikut:
   
    ![Predictive Analysis Storage](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B12%5Dpredictive-analysis/predictive-storage-v2.png)

   Keterangan:
   a. *Hostname* yang terkonfigurasi di perangkat.
   b. Alamat IP dari perangkat yang dimonitor.
   c. Nama setiap indeks *storage* dari perangkat yang dimonitor.
   d. Kapasitas penyimpanan maksimal dari masing-masing *storage*.
   e. Ambang batas penggunaan *storage*.
   f. Jumlah hari yang menunjukkan berapa lama lagi penggunaan *storage* akan mencapai ambang batas. Jika tren diprediksi menurun, keterangan yang muncul adalah *downward trend*.