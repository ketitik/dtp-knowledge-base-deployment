---
title: 'Bagaimana cara melihat detail web/API probe yang sudah saya daftarkan?'
category: 'web-api-probe'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Akses menu Inventory → Web/API Probe'
---

# Bagaimana cara melihat detail web/API probe yang sudah saya daftarkan?

1. Akses menu **Inventory --> Web/API Probe**

    ![Menu Web Probe](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/web-api-monitoring/%5B6%5Dweb-api-probe/menu-web-probe.png)

2. Dari halaman utama *Web/API Probe Inventory*, klik tombol **View** di salah satu baris data *web/API probe*,

    ![Tombol Detail Web Probe](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/web-api-monitoring/%5B6%5Dweb-api-probe/tombol-view.png)

3. Selanjutnya akan tampil detail *web/API probe* seperti berikut:

    ![View Web Probe](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/web-api-monitoring/%5B6%5Dweb-api-probe/view-web-probe.png)

4. Klik tombol **Close** untuk menutup detail *web/API probe* atau tombol **Go to Edit** untuk mengedit data *web/API probe* yang bersangkutan.
