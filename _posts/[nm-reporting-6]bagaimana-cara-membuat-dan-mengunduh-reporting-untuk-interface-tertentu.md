---
title: 'Bagaimana cara membuat dan mengunduh reporting untuk interface tertentu?'
category: 'reporting'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Akses menu Network Reporting → Interface Reporting'
---

# Bagaimana cara membuat dan mengunduh reporting untuk interface tertentu?

1. Akses menu **Network Reporting → Interface Reporting**.

    ![Menu Interface Reporting](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B13%5Dreporting/menu-interface-reporting-v2.png)

2. Dari halaman utama menu *Interface Reporting*, klik tombol **Generate Report** di salah satu baris data,

    ![Tombol Generate Report](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B13%5Dreporting/tombol-generate-report.png)

3. Selanjutnya akan muncul *form* detail *interface* yang bersangkutan seperti contoh berikut:

    ![Form Generate Interface Reporting](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B13%5Dreporting/form-generate-interface-reporting.png)

4. Isi kolom **Title** dan **Prepared for** jika masih kosong, atau lakukan perubahan jika diperlukan. Selanjutnya, isi periode waktu laporan yang yang diharapkan pada bagian **Period**.

    ![Select Period](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B13%5Dreporting/select-period-interface-reporting.png)

5. Klik tombol **Generate** untuk melanjutkan pembuatan laporan,

    ![Tombol Generate](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B13%5Dreporting/tombol-generate-report.png)

6. Selanjutnya akan muncul *preview* dari laporan yang ingin dibuat di *tab* baru *browser* seperti berikut:

    ![Preview Interface Reporting](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B13%5Dreporting/preview-interface-reporting.png)


    Keterangan:
    a. Judul laporan, pihak yang akan menerima laporan, dan periode waktu laporan yang diharapkan.
    
    b. Tanggal kapan laporan tersebut dibuat.
    
    c. Tabel nilai MTTR (besarnya rasio *link/port/interface* yang mati) dan *availability* dari *link/port/interface* selama periode waktu tertentu beserta nilai rata-ratanya.

    d. Grafik *availability* dari *link/port/interface* pada periode waktu tertentu.

7. Terakhir, klik tombol **Download** untuk mengunduh laporan yang telah dibuat ke dalam file berformat PDF, atau tombol **Print** jika ingin melihat laporan di *tab* baru *browser*.