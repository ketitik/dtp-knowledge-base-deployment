---
title: 'Bagaimana cara mengedit data web/API probe yang sudah saya daftarkan?'
category: 'web-api-probe'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Akses menu Inventory → Web/API Probe'
---

# Bagaimana cara mengedit data web/API probe yang sudah saya daftarkan?

1. Akses menu **Inventory → Web/API Probe**.

    ![Menu Web Probe](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/web-api-monitoring/%5B6%5Dweb-api-probe/menu-web-probe.png)

2. Dari halaman utama *Web/API Probe Inventory*, klik tombol **Edit** di salah satu baris data *web/API probe* atau melalui halaman detail *web/API probe*,

    ![Tombol Edit Web Probe](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/web-api-monitoring/%5B6%5Dweb-api-probe/tombol-edit.png) atau ![Tombol Go to Edit](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/web-api-monitoring/%5B6%5Dweb-api-probe/go-to-edit.png)

3. Selanjutnya akan tampil *form* untuk mengedit data seperti berikut:

    ![Form Edit Web Probe](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/web-api-monitoring/%5B6%5Dweb-api-probe/edit-web-probe.png)

4. Edit data sesuai kebutuhan, kemudian klik tombol **Update** untuk menyimpan perubahan pada data.