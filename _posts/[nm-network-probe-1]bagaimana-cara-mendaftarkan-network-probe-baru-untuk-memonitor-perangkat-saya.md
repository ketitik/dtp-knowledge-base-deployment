---
title: 'Bagaimana cara mendaftarkan network probe baru untuk memonitor perangkat saya?'
category: 'network-probe'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: ' Akses menu Inventory → Network Probe'
---

# Bagaimana cara mendaftarkan network probe baru untuk memonitor perangkat saya?

1. Akses menu **Network Inventory → Network Probe**

    ![Menu Network Probe](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B11%5Dnetwork-probe/menu-network-probe-v2.png)

2. Dari halaman utama *Network Probe Inventory*, klik tombol **Add Network Probe**,

    ![Tombol Tambah Network Probe](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B11%5Dnetwork-probe/tombol-tambah-network-probe.png)

3. Selanjutnya akan muncul *form* untuk mengisi detail *network probe* yang akan ditambahkan. Secara *default*, kolom **SNMP Version** akan terisi dengan pilihan SNMP versi 2.

    ![Form Tambah Network Probe](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B11%5Dnetwork-probe/form-tambah-network-probe.png)

4. Isi data pada *form* tersebut dengan rincian sebagai berikut:

    | **Jenis Isian** | **Keterangan** |
    | ----- | ----- |
    | SNMP Version | Versi dari SNMP, tersedia 2 pilihan versi yaitu versi 2 dan 3.|
    | Probe Name | Nama dari *network probe* yang akan bertugas mengambil data dari perangkat jaringan.|
    | Scarpe Interval (minutes) | Interval waktu pengambilan data dalam satuan detik. |
    | SNMP Community | Komunitas atau grup dari SNMP.|
    | Message Broker Address | Alamat *message broker* yang bertugas mengatur antrian dan lalu lintas data antar servis di Netmonk Prime. Secara *default*, alamat *message broker* yaitu sebagai berikut: message-broker.internal.netmonk.id:9092 |
    | Location | Lokasi dari *network probe* yang akan ditambahkan. |

5. Jika memilih SNMP versi 3, terdapat tambahan kolom yang harus diisi karena SNMP versi 3 mendukung otentikasi dan *privacy* atau enkripsi.

    ![Form Tambah Network Probe SNMP Versi 3](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B11%5Dnetwork-probe/form-tambah-network-probe-snmp3.png)

6. Isi kolom tambahan tersebut dengan rincian sebagai berikut:
   
    | **Jenis Isian** | **Keterangan** |
    | ----- | ----- |
    | Security Level | Level keamanan yang dipilih. Terdapat 3 jenis *security level* yaitu authPriv yang menggunakan otentikasi dan *privacy*, authNoPriv yang hanya menggunakan otentikasi, dan noAuthNoPriv yang tidak menggunakan keduanya. |
    | SNMP Username | *Username* untuk mengakses SNMP.|
    | SNMP Password | *Password* untuk mengakses SNMP.|
    | Authentication Protocol | Jenis protokol otentikasi yang digunakan. |
    | Privacy Protocol | Jenis protokol *privacy* yang digunakan. |
    | Provacy Key | *Key* yang digunakan untuk enkripsi.|

7. Klik tombol **Add** untuk menambahkan *network probe* baru.