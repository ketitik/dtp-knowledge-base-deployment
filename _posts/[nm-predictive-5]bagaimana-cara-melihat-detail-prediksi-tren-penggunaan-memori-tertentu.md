---
title: 'Bagaimana cara melihat detail prediksi tren penggunaan memori tertentu?'
category: 'predictive-analysis'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Akses menu Predictive Analysis → Memory'
---

# Bagaimana cara melihat detail prediksi tren penggunaan memori tertentu?

1. Akses menu **Predictive Analysis → Memory**

    ![Menu Predictive Analysis Memory](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B12%5Dpredictive-analysis/menu-predictive-analysis-memory-v2.png)

2. Dari halaman utama menu *Predictive Analysis Memory*, klik tombol **View** di salah satu baris data.
   
    ![Tombol View Detail](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B12%5Dpredictive-analysis/tombol-view-detail.png)

3. Maka akan muncul tampilan seperti berikut:
   
   ![Detail Predictive Memory](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B12%5Dpredictive-analysis/detail-predictive-memory-v2.png)

   Keterangan:
   a. Informasi dasar seperti nama perangkat, alamat IP, dan indeks memori.
   b. Jumlah hari yang menunjukkan berapa lama lagi penggunaan memori akan mencapai ambang batas.
   c. Persentase penggunaan memori dari total kapasitas.
   d. Grafik tren penggunaan memori jika diasumsikan penggunaan memori di masa mendatang di bawah 50% (diwakili warna hijau), antara 50% dan 80% (diwakili warna kuning), dan di atas 80% (diwakili warna merah), serta *threshold* atau ambang batas (diwakili warna biru tua) dan grafik *history* penggunaan memori (diwakili warna biru muda).