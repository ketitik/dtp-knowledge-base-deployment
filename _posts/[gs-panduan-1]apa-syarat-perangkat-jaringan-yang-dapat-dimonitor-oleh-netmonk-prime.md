---
title: 'Apa syarat perangkat yang dapat dimonitor oleh Netmonk Prime?'
category: 'panduan-pengguna-baru'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Perangkat jaringan harus mendukung pemanfaatan protokol ICMP dan SNMP'
---

# Apa syarat perangkat yang dapat dimonitor oleh Netmonk Prime?

Perangkat jaringan harus mendukung pemanfaatan protokol ICMP dan SNMP untuk memonitoring perangkat tersebut. Protokol ICMP digunakan untuk mendeteksi status perangkat apakah hidup *(up)* atau mati *(down)*, sedangkan SNMP digunakan untuk mengambil informasi-informasi dari perangkat seperti trafik, konfigurasi alamat IP perangkat, dan *uptime* perangkat.