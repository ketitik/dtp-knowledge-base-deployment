---
title: 'Bagaimana cara menghapus Network Map yang sudah saya buat?'
category: 'network-map'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Akses menu Dashboard → Network Map'
---

# Bagaimana cara menghapus Network Map yang sudah saya buat?

1. Akses menu **Dashboard → Network Map**

    ![Menu Network Map](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B8%5Dnetwork-map/menu-network-map.png)

2. Dari halaman utama *Dashboard Network Map*, klik tombol **Delete** di salah satu baris data map,

    ![Tombol Delete Network Map](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B8%5Dnetwork-map/tombol-delete-network-map.png)

3. Selanjutnya akan tampil kotak dialog seperti berikut:

    ![Hapus Data](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B8%5Dnetwork-map/hapus-data.png)

4. Klik tombol **Delete** untuk melanjutkan proses hapus.	