---
title: 'Cara menggunakan Netmonk Prime: Panduan ringkas untuk memulai'
category: 'pengenalan-netmonk-prime'
is_sticky: true
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Selamat datang di Netmonk Prime - Aplikasi monitoring yang bersifat proactive dan preventive maintenance.'
---

# Cara menggunakan Netmonk Prime: Panduan ringkas untuk memulai

Selamat datang di Netmonk Prime - Aplikasi monitoring yang bersifat *proactive* dan *preventive maintenance* yang dapat digunakan untuk memonitoring beberapa kebutuhan, yaitu monitoring perangkat jaringan, *website* atau *API* berbasis HTTP/HTTPS, dan server. 

## Network Monitoring
Untuk membantu Anda memulai, lihat video di bawah untuk tur singkat *Network Monitoring*:

<!---
<iframe width="550" height="300"
src="https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/getting-started/%5B1%5Dpengenalan-netmonk-prime/start-guide-network-monitoring.mp4">
</iframe>
-->

## Website/API Monitoring
Untuk membantu Anda memulai, lihat video di bawah untuk tur singkat *Website/API Monitoring*:

<!--
<iframe width="550" height="300"
src="https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/getting-started/%5B1%5Dpengenalan-netmonk-prime/start-guide-web-api-monitoring.mp4">
</iframe>
-->

## Linux Server Monitoring
Untuk membantu Anda memulai, lihat video di bawah untuk tur singkat *Linux Server Monitoring*:

<!--
<iframe width="550" height="300" src=""> </iframe>
-->


