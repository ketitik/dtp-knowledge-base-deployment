---
title: 'Jika saya ada kendala terkait subscription, apa saja yang bisa saya tanyakan ke tim support netmonk secara langsung?'
category: 'subscription'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Anda dapat menanyakan dan kami bisa support langsung antara lain:'
---

# Jika saya ada kendala terkait subscription, apa saja yang bisa saya tanyakan ke tim support netmonk secara langsung?

Anda dapat menanyakan dan kami bisa support langsung antara lain:
- Menanyakan maupun meminta penawaran paket khusus Netmonk Prime
- Menanyakan jika *link* Netmonk Prime tidak bisa diakses
- Jika pertanyaan-pertanyaan tersebut tidak ada di *list knowledge center* kami
