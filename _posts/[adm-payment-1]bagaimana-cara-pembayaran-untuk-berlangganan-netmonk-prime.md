---
title: 'Bagaimana cara pembayaran untuk berlangganan Netmonk Prime?'
category: 'payment'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Pembayaran dilakukan melalui rekening yang tercantum'
---

# Bagaimana cara pembayaran untuk berlangganan Netmonk Prime?

Pembayaran dilakukan melalui rekening yang tercantum pada halaman **Detail Pembayaran Order**. Pastikan nominal yang dikirimkan sesuai dengan total pembayaran hingga 3 digit terakhir. Tiga digit terakhir tersebut merupakan kode unik yang digunakan untuk mempermudah tim kami dalam memvalidasi pembayaran yang sudah Anda lakukan. Untuk lebih jelasnya, ikuti langkah berikut:

1. Masuk ke halaman *subscription* melalui menu **Subscription** seperti berikut:

    ![Menu Subscription](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/subscription/menu-subscription.png)

2. Klik tombol **Detail** pada data order yang belum dilakukan pembayaran seperti contoh berikut:
   
    ![Status Order Butuh Pembayaran](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/subscription/paket-berbayar-butuh-pembayaran.png)

3.  Setelah masuk halaman **Detail Subscription**, klik pada tulisan **Klik untuk lihat cara pembayaran** seperti di bawah ini:

    ![Lihat Cara Pembayaran](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/subscription/lihat-cara-pembayaran.png)

4.  Selanjutnya, akan tampil **Detail Pembayaran Order** dari order yang bersangkutan seperti berikut:
   
    ![Detail Pembayaran Order](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/subscription/detail-order.png)

5.  Silakan melakukan pembayaran ke rekening yang tercantum di atas. Jika pembayaran sudah tervalidasi, order akan segera diproses. Waktu maksimal order tersebut diproses hingga teraktivasi adalah 5 hari kerja.