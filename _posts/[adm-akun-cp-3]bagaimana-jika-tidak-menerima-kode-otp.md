---
title: 'Bagaimana jika saya tidak menerima kode OTP?'
category: 'akun-customer-portal'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Pastikan nomor handphone yang Anda inputkan sudah benar'
---

# Bagaimana jika saya tidak menerima kode OTP?

Pastikan nomor *handphone* yang Anda inputkan sudah benar. Jika nomor sudah benar tetapi Anda masih belum menerima pesan *WhatsApp* atau SMS yang berisi kode OTP, klik teks **Kirim Ulang OTP** seperti berikut:

![Kirim Ulang OTP](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/akun-cp/input-kode-otp.png)
