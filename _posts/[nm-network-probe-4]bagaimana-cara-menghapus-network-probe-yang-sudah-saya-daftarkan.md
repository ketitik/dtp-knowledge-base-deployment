---
title: 'Bagaimana cara menghapus network probe yang sudah saya daftarkan?'
category: 'network-probe'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: ' Akses menu Network Inventory → Network Probe'
---

# Bagaimana cara menghapus network probe yang sudah saya daftarkan?

1. Akses menu **Network Inventory → Network Probe**.

    ![Menu Network Probe](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B11%5Dnetwork-probe/menu-network-probe-v2.png)

2. Dari halaman utama *Network Probe Inventory*, klik tombol **Delete** di salah satu baris data *netwrok probe*,

    ![Tombol Delete](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B11%5Dnetwork-probe/tombol-delete.png)

3. Selanjutnya akan tampil kotak dialog seperti berikut:

    ![Hapus Data](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B11%5Dnetwork-probe/hapus-data.png)

4. Klik tombol **Delete** untuk melanjutkan proses hapus.