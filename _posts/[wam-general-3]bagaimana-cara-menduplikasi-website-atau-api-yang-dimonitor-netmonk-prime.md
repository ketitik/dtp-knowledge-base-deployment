---
title: 'Bagaimana cara menduplikasi website atau API yang dimonitor Netmonk Prime?'
category: 'general'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Akses menu Web/API Monitoring → Web/API Status'
---

# Bagaimana cara menduplikasi website atau API yang dimonitor Netmonk Prime?

1. Akses menu **Web/API Monitoring → Web/API Status**.

    ![Menu Web Status](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/web-api-monitoring/%5B1%5Dgeneral/menu-web-status.png)

2. Dari halaman utama *Web/API Status*, klik tombol Duplicate Data di salah satu baris data.

    ![Tombol Duplicate Web](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/web-api-monitoring/%5B1%5Dgeneral/tombol-duplicate-web.png)

3. Selanjutnya akan tampil *form* untuk mengedit data *website* atau *API* sebelum diduplikasi:

    ![Form Duplicate Web](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/web-api-monitoring/%5B1%5Dgeneral/form-duplicate-web.png)

4. Klik tombol **Duplicate** untuk melanjutkan proses duplikasi.