---
title: 'Bagaimana cara membuat dan mengunduh reporting untuk perangkat yang dimonitor?'
category: 'reporting'
is_sticky: true
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Akses menu Network Reporting → Device Reporting, isi kolom Title dengan judul laporan sesuai keinginan Anda.'
---

# Bagaimana cara membuat dan mengunduh reporting untuk perangkat yang dimonitor?

1. Akses menu **Network Reporting → Device Reporting**.

    ![Menu Device Reporting](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B13%5Dreporting/menu-device-reporting-v2.png)

2. Dari halaman utama menu *Device Reporting*, isi kolom **Title** dengan judul laporan sesuai keinginan Anda. Secara *default*, isi kolom **Title** adalah sebagai berikut:

    ![Kolom Title](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B13%5Dreporting/kolom-title.png)

3. Pada bagian **Node**, pilih **All** jika ingin membuat laporan untuk semua perangkat yang termonitor.

    ![Kolom All Node](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B13%5Dreporting/kolom-all-node.png)

4. Sedangkan jika ingin membuat laporan untuk perangkat tertentu saja, pilih **Specific**, kemudian tambahkan perangkat yang diinginkan melalui *dropdown* **Select** seperti contoh berikut:

    ![Kolom Specific Node](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B13%5Dreporting/kolom-specific-node1.png) ![Kolom Specific Node](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B13%5Dreporting/kolom-specific-node2.png)

5. Klik kolom **Time Range**, maka akan muncul pilihan periode *(Quick Range)* atau rentang waktu *(Custom Range)* untuk menampilkan data sesuai dengan kebutuhan.

    ![Kolom Time Range](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B13%5Dreporting/kolom-time-range.png)

6. Pilih data apa saja yang ingin ditampilkan di dalam laporan dengan mencentang *checkbox* yang terdapat di bagian **Data Displayed** berikut:

    ![Kolom Data Displayed](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B13%5Dreporting/kolom-data-displayed.png)

7. Klik tombol **Generate** untuk membuat laporannya.

    ![Tombol Generate](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B13%5Dreporting/tombol-generate.PNG)

8. Selanjutnya akan muncul *preview* dari laporan yang dibuat, seperti contoh berikut:
   
   ![Preview Device Reporting](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B13%5Dreporting/preview-device-reporting.png)


Berikut video singkat terkait cara membuat dan mengunduh reporting untuk perangkat yang dimonitor:

<iframe width="550" height="300"
src="https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B13%5Dreporting/cara-membuat-dan-mengunduh-reporting-untuk-perangkat-yang-dimonitor.mp4">
</iframe>