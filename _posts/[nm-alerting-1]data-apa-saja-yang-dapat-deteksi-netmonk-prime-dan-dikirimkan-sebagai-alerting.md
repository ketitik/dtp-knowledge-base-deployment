---
title: 'Data apa saja yang dapat dideteksi Netmonk Prime dan dikirimkan sebagai alerting?'
category: 'alerting'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Netmonk Prime akan mengirimkan alert melalui Messaging App'
---

# Data apa saja yang dapat dideteksi Netmonk Prime dan dikirimkan sebagai alerting?

Netmonk Prime akan mengirimkan *alert* melalui *Messaging App* sehingga tim IT Network tidak perlu berada di depan layar monitor 24x7 untuk mengetahui kondisi kritis yang terjadi di jaringan. Terdapat dua jenis kanal yang dapat digunakan untuk mengirimkan *alert* yaitu *telegram bot* dan *email SMTP*. Beberapa parameter atau data yang dapat dideteksi dan dikirim sebagai *alert* antara lain:
- Perangkat yang terdeteksi tidak dapat diraih (mati atau koneksi putus).
- *Link/Port/Interface* yang penggunaannya kritis atau di atas ambang batas (di atas 80%).
- CPU, RAM (Memori), dan *Storage* (Media Penyimpanan) yang penggunaannya kritis atau di atas ambang batas (di atas 80%).
- *Website* atau *API* yang terdeteksi *down*.
- *Website* atau *API* dengan *response time* di atas ambang batas atau *threshold* yang sudah ditentukan.
- *Linux Server* yang terdeteksi *down*.
- *Linux Server* dengan penggunaan *CPU* dan *Memory* melebihi batas normal.