---
title: 'Bagaimana cara mengedit data network probe yang sudah saya daftarkan?'
category: 'network-probe'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: ' Akses menu Network Inventory → Network Probe'
---

# Bagaimana cara mengedit data network probe yang sudah saya daftarkan?

1. Akses menu **Network Inventory → Network Probe**.

    ![Menu Network Probe](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B11%5Dnetwork-probe/menu-network-probe-v2.png)

2. Dari halaman utama *Network Probe Inventory*, klik tombol **Edit** di salah satu baris data *network probe*,

    ![Tombol Edit](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B11%5Dnetwork-probe/tombol-edit.png)

3. Selanjutnya akan tampil *form* untuk mengedit data seperti berikut:

    ![Form Edit Network Probe](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B11%5Dnetwork-probe/form-edit-network-probe.png)

4. Edit data sesuai kebutuhan, kemudian klik tombol **Save** untuk menyimpan perubahan pada data.