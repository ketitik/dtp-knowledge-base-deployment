---
title: 'Bagaimana registrasi akun Customer Portal?'
category: 'panduan-pengguna-baru'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Buka Website Netmonk'
---

# Bagaimana registrasi akun Customer Portal?

1. Buka website Netmonk di alamat [berikut](https://netmonk.id/) 
    
   ![Netmonk.id](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/getting-started/%5B2%5Dpanduan-pengguna/netmonk-id.png)

2. Klik tombol **Free Trial**, maka akan tampil *form* untuk melakukan registrasi atau pendaftaran akun Customer Portal:

    ![Form Registrasi](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/getting-started/%5B2%5Dpanduan-pengguna/form-registrasi-cp.png)

3. Isi data pada *form* tersebut dengan nama lengkap Anda sebagai PIC perusahaan, alamat email, serta kata sandi atau *password* yang nantinya digunakan ketika *login* ke Customer Portal. Kata sandi setidaknya harus terdiri dari 8 karakter, dengan kombinasi yang mengandung huruf besar, huruf kecil, dan angka.
   
4. Klik tombol **Buat Akun**, maka pihak Netmonk akan mengirimkan email untuk melakukan verifikasi email yang Anda daftarkan

    ![Tombol Buat Akun](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/getting-started/%5B2%5Dpanduan-pengguna/buat-akun-cp.png) 

5. Buka email Anda, jika Anda sudah mendapatkan email seperti berikut, klik tombol **Verify Email Address**
   
    ![Verify Email Address](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/getting-started/%5B2%5Dpanduan-pengguna/verify-email-address.png)

6. Setelah alamat email Anda terverifikasi, Anda akan diarahkan ke halaman *login* Customer Portal seperti berikut:

    ![Login Customer Portal](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/getting-started/%5B2%5Dpanduan-pengguna/login-cp.png)