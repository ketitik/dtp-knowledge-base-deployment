---
title: 'Di mana saya dapat menemukan daftar penggunaan Disk dari semua server yang termonitor Netmonk Prime?'
category: 'diskio-status'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Dari Dashboard Network Summary mode operasional'
---

# Di mana saya dapat menemukan daftar penggunaan Disk dari semua server yang termonitor Netmonk Prime?

Dari **Dashboard Network Summary** mode operasional, klik **View All** pada bagian **DiskIO Status** atau akses melalui menu **Network Status → DiskIO Status**. Berikut contoh halaman utama *DiskIO Status* yang menampilkan daftar penggunaan *disk* dari semua server yang termonitor:

![Disk IO Status](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B7%5Ddiskio-status/diskio-status-v2.png)

Keterangan:
a. Rata-rata operasi *input/output disk* di semua server yang terdeteksi.
b. Jumlah *disk* dengan persentase rata-rata operasi *input/output* di bawah 50% .
c. Jumlah *disk* dengan persentase rata-rata operasi *input/output* di antara 50% s.d 80% .
d. Jumlah *disk* dengan persentase rata-rata operasi *input/output* di atas 80%.
e. Nama server yang dimonitor.
f. Nama server yang terdaftar di *inventory*.
h. Nama masing-masing *disk*.
i. Total data yang ditulis ke dalam *disk*.
j. Total data yang dibaca oleh *disk*.
k. Persentase rata-rata operasi *input/output* suatu *disk*.
l. Kategori status rata-rata operasi *input/output disk* yang mewakili rata-rata beban *disk*.