---
title: 'Mengapa jumlah link/port/interface yang tercantum pada counter halaman utama menu Link Utilization berbeda dengan jumlah keseluruhan yang terdeteksi oleh Netmonk Prime?'
category: 'link-utilization'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Perbedaan jumlah link/port/interface yang tercantum pada counter'
---

# Mengapa jumlah link/port/interface yang tercantum pada counter halaman utama menu Link Utilization berbeda dengan jumlah keseluruhan yang terdeteksi oleh Netmonk Prime?

Perbedaan jumlah *link/port/interface* yang tercantum pada *counter* dengan jumlah keseluruhan perangkat yang terdeteksi terjadi karena *counter* tidak menghitung *link/port/interface* yang sedang tidak tersedia, yang informasinya dapat dilihat dari isi kolom **Utilization** dengan nilai N/A (*Not Available*).