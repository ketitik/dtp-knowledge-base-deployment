---
title: 'Data apa saja yang ditampilkan di dashboard Network Monitoring?'
category: 'dashboard'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Terdapat dua jenis dashboard'
---

# Data apa saja yang ditampilkan di dashboard Network Monitoring?

Terdapat dua jenis *dashboard* yaitu *dashboard* untuk operasional jaringan yang menampilkan rangkuman kondisi terkini dari perangkat jaringan yang dimonitor, serta *dashboard* yang ditujukan untuk pihak managerial dan disajikan dalam format yang lebih ringkas sehingga mudah dimengerti. Beberapa data yang ditampilkan di halaman utama *dashboard* yaitu seperti berikut:

a. **Device Status**, menampilkan informasi berupa jumlah perangkat yang hidup (*Up*) dan mati (*Down*) dan daftar perangkat yang terdeteksi mati.
  
b. **Link Utilization**, menampilkan informasi berupa jumlah *link/port/interface* yang telah dikelompokkan sesuai persentase penggunaannya (*Normal, Warning, Critical*) dan daftar *Top 10 link/port/interface* dengan penggunaan tertinggi.

c. **Starred Link Status**, menampilkan informasi berupa total penggunaan *link/port/interface* yang dikategorikan sebagai *starred link* dan daftar penggunaan masing-masing *link/port/interface* tersebut.

d. **CPU Status**, menampilkan informasi berupa jumlah indeks CPU yang telah dikelompokkan sesuai penggunaannya (*Normal, Warning, Critical*) dan daftar *Top 10* penggunaan CPU tertinggi.

e. **RAM Status**, menampilkan informasi berupa jumlah indeks memori/RAM yang telah dikelompokkan sesuai penggunaannya (*Normal, Warning, Critical*) dan daftar *Top 10* penggunaan memori tertinggi.

f. **Storage Status**, menampilkan informasi berupa jumlah media penyimpanan/*Storage* yang telah dikelompokkan sesuai penggunaannya (*Normal, Warning, Critical*) dan daftar *Top 10* penggunaan media penyimpanan tertinggi.

Untuk lebih jelasnya, Anda dapat mengakses menu **Dashboard → Network Summary**.