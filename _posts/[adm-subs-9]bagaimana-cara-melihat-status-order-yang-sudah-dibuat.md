---
title: 'Bagaimana cara melihat status order yang sudah dibuat?'
category: 'subscription'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Masuk ke halaman subscription'
---

# Bagaimana cara melihat status order yang sudah dibuat?

1. Masuk ke halaman *subscription* melalui menu **Subscription** seperti berikut:
   
   ![Menu Subscription](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/subscription/menu-subscription.png)

2. Status order dapat dilihat di halaman utama menu **Subscription** pada kolom Status **Orderan Terakhir**

    ![List Subscription](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/subscription/list-subscription-status.png)

3. Untuk melihat rinciannya, klik tombol **Detail** pada data order yang bersangkutan. Status order terdapat di bagian **Rincian Order Subscription**. Berikut contoh tampilan rincian status order untuk Netmonk Prime versi *Free Trial* dan versi berbayar:

    ![Rincian Order Free Trial](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/subscription/rincian-order-free-trial.png)

    ![Rincian Order Berbayar](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/subscription/rincian-order-berbayar.png)

4. Waktu maksimal order diproses hingga teraktivasi adalah 2 hari kerja untuk Netmonk Prime versi *Free Trial* dan 5 hari kerja untuk Netmonk Prime versi berbayar.