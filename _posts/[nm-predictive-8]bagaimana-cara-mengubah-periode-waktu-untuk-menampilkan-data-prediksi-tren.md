---
title: 'Bagaimana cara mengubah periode waktu untuk menampilkan data prediksi tren sesuai dengan kebutuhan saya?'
category: 'predictive-analysis'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Akses sub menu Predictive Analysis sesuai kebutuhan Anda'
---

# Bagaimana cara mengubah periode waktu untuk menampilkan data prediksi tren sesuai dengan kebutuhan saya?

Akses sub menu **Predictive Analysis** sesuai kebutuhan Anda, klik tombol **View** di salah satu baris data sehingga sistem beralih ke halaman detail. Dari halaman detail tersebut, klik pada kolom **Next 1 month** yang berada di kanan atas seperti berikut, maka akan muncul pilihan untuk menampilkan data sesuai dengan kebutuhan.

![Custom Period Predictive Analysis](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B12%5Dpredictive-analysis/custom-period-predictive-analysis.png)