---
title: 'Bagaimana cara mendaftarkan pengguna baru di dalam Netmonk Prime?'
category: 'akun-netmonk-prime'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Akses menu Settings → Users'
---

# Bagaimana cara mendaftarkan pengguna baru di dalam Netmonk Prime?

1. Akses menu **Settings → Users**

	![settings-users](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/akun-np/settings-users.png)


2. Dari halaman utama menu *Users Setting* atau manajemen pengguna, klik tombol **Add New User**.

	![add-new-user](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/akun-np/add-new-user.png)

3. Selanjutnya akan muncul *form* untuk mengisi detail pengguna yang akan ditambahkan.

	![add-user-fill](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/akun-np/add-user-fill.png)

4. Isi data pada *form* tersebut dengan rincian sebagai berikut:

	<table>
		<thead>
			<tr>
				<th>Jenis Isian</th>
				<th>Keterangan</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Username</td>
				<td>Nama pengguna yang digunakan untuk <i>login</i>.</td>
			</tr>
			<tr>
				<td>Password</td>
				<td>Kata sandi yang digunakan untuk <i>login</i>.</td>
			</tr>
			<tr>
				<td>Role</td>
				<td>Tingkatan akun menyesuaikan hak akses yang diberikan. 
					Terdapat tiga tingkatan akun yaitu <i>root</i>, <i>admin</i>, dan <i>user</i>.</td>
			</tr>
			<tr>
				<td>Description</td>
				<td>Informasi tambahan terkait pengguna yang akan didaftarkan (opsional).</td>
			</tr>
		</tbody>
	</table>

5. Klik tombol **Add** untuk menambahkan pengguna baru.