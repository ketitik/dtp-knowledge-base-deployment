---
title: 'Alerting dikirimkan menggunakan apa?'
category: 'alerting'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Saat ini, Netmonk Prime mendukung 2 tipe media'
---

# Alerting dikirimkan menggunakan apa?

Saat ini, Netmonk Prime mendukung 2 tipe media untuk *alerting*, yaitu Telegram dan Email.