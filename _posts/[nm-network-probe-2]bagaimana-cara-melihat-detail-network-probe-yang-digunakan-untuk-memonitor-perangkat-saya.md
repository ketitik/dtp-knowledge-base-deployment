---
title: 'Bagaimana cara melihat detail network probe yang digunakan untuk memonitor perangkat saya?'
category: 'network-probe'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: ' Akses menu Network Inventory → Network Probe'
---

# Bagaimana cara melihat detail network probe yang digunakan untuk memonitor perangkat saya?

1. Akses menu **Network Inventory → Network Probe**.

    ![Menu Network Probe](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B11%5Dnetwork-probe/menu-network-probe-v2.png)

2. Dari halaman utama *Network Probe Inventory*, klik tombol **View** di salah satu baris data *network probe*,

    ![Tombol View](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B11%5Dnetwork-probe/tombol-view.png)

3. Selanjutnya akan tampil detail *network probe* seperti berikut:

    ![Form View Network Probe](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B11%5Dnetwork-probe/view-network-probe.png)

4. Klik tombol **Close** untuk menutup detail *network probe*.
