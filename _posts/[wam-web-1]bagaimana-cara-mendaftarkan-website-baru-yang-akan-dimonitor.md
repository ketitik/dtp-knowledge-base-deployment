---
title: 'Bagaimana cara mendaftarkan website baru yang akan dimonitor?'
category: 'website'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Akses modul Web/API Monitoring → Web/API Status'
---

# Bagaimana cara mendaftarkan website baru yang akan dimonitor?

1. Akses modul **Web/API Monitoring → Web/API Status**.

    ![Menu Web Status](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/web-api-monitoring/%5B2%5Dwebsite/menu-web-status-v2.png)

2. Dari halaman utama *Web/API Status*, klik tombol **Add Web/API**,

    ![Tombol Tambah Web](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/web-api-monitoring/%5B2%5Dwebsite/tombol-tambah-web.png)

3. Selanjutnya akan muncul *form* untuk mengisi detail *website* yang akan dimonitor.

    ![Form Tambah Web](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/web-api-monitoring/%5B2%5Dwebsite/form-tambah-web.png)

4. Isi data pada *form* tersebut dengan rincian sebagai berikut:

    | **Jenis Isian** | **Keterangan** |
    | ----------- | ----------- |
    | Name  | Nama dari *website* yang akan dimonitor.| 
    | Interval check | Rentang waktu diperiksanya status *website* secara berkala. |
    | Probe | Pilihan *probe* yang akan bertugas mengambil data dari *website* yang dimonitor. Jika ingin menambahkan *probe* baru, klik **Add New Probe**. |
    | URL/IP | Alamat URL atau alamat IP dari *website* yang akan dimonitor. Mendukung 2 protokol, yaitu HTTP dan HTTPS. |
    | Tags | Kata kunci yang ditambahkan untuk mendeskripsikan *website* yang akan dimonitor. (Opsional) |
    | Alert Threshold | Ambang batas *response time* yang ditentukan untuk *website* yang akan dimonitor (diisi dalam satuan *milisecond*) |

5. Selanjutnya, tambahkan *request* dan kondisi *response* yang diharapkan (opsional) dengan rincian sebagai berikut:

    <table>
    <tr>
        <th colspan="2">Jenis Isian</th>
        <th>Keterangan</th>
    </tr>
    <tr>
        <td rowspan="5">Request</td>
        <td>Method</td>
        <td>Request <i>method</i> yang menunjukkan tindakan yang diinginkan untuk dilakukan terhadap <i>website</i> yang akan dimonitor.</td>
    </tr>
    <tr>
        <td>Request headers</td>
        <td>Request <i>header</i> yang dapat digunakan dalam HTTP <i>request</i> untuk memberikan informasi tentang konteks permintaan.</td>
    </tr>
    <tr>
        <td>Body</td>
        <td>Data opsional yang terkait dengan <i>request</i>.</td>
    </tr>
    <tr>
        <td>Username</td>
        <td><i>Username</i> yang digunakan untuk mengakses halaman <i>website</i> jika <i>website</i> memerlukan autentikasi.</td>
    </tr>
    <tr>
        <td>Password</td>
        <td><i>Password</i> yang digunakan untuk mengakses halaman <i>website</i> jika <i>website</i> memerlukan autentikasi.</td>
    </tr>
    <tr>
        <td rowspan="3">Response Codition</td>
        <td>Status Code</td>
        <td>Kode status HTTP yang diharapkan keluar sebagai <i>response</i> dari <i>website</i> yang dimonitor.</td>
    </tr>
    <tr>
        <td>Match Header</td>
        <td><i>Response header</i> yang diharapkan keluar sebagai <i>response</i> dari <i>website</i> yang dimonitor.</td>
    </tr>
    <tr>
        <td>Match Body</td>
        <td>Data di dalam <i>response body</i> yang diharapkan keluar sebagai <i>response</i> dari <i>website</i> yang dimonitor.</td>
    </tr>
    <tr>
        <td colspan="3">Keterangan:
        <br>
        Jika <i>Status Code, Match Header,</i> atau <i>Match Body</i> yang diinputkan tidak ada di dalam <i>response</i>, maka status <i>website</i> tersebut dianggap <i>down</i> karena tidak sesuai dengan kondisi yang diharapkan. </td>
    </tr>
    </table>

6. Klik tombol **Save** untuk menambahkan *web* baru.


## Contoh Menambahkan Website Baru

Sebagai contoh skenario, Anda ingin memonitor halaman “About” dari *website* X yang beralamat di https://xxx.com/about setiap 5 menit sekali. Anda ingin memastikan bahwa halaman tersebut berjalan dengan lancar dalam arti memberikan kode respon 200 dan konten di dalamnya mengandung kata “Perusahaan”. Maka contoh isi *form* **Add Web/API** pada skenario ini adalah sebagai berikut:

![Contoh Tambah Web](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/web-api-monitoring/%5B2%5Dwebsite/contoh-tambah-web1.png) ![Contoh Tambah Web](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/web-api-monitoring/%5B2%5Dwebsite/contoh-tambah-web2.png)
![Contoh Tambah Web](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/web-api-monitoring/%5B2%5Dwebsite/contoh-tambah-web3.png)

Jika saat dimonitor, halaman *website* di atas tidak memberikan kode respon 200 atau kata ”Perusahaan” tidak ditemukan pada kontennya, maka status *website* tersebut dianggap *down* karena tidak sesuai dengan kondisi yang Anda harapkan.

