---
title: 'Apa itu Netmonk Prime?'
category: 'pengenalan-netmonk-prime'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Netmonk Prime merupakan dashboard monitoring'
---

# Apa itu Netmonk Prime?

Netmonk Prime merupakan *dashboard monitoring* yang bersifat *proactive* dan *preventive maintenance* yang dapat digunakan untuk memonitoring beberapa kebutuhan. Saat ini, Netmonk Prime terdiri dari 3 modul utama.

1. **Network Monitoring**, memantau kondisi suatu jaringan sehingga dapat membantu Manager IT dan Tim Operasional IT dalam mengelola jaringan, audit performa jaringan, serta pengambilan keputusan yang lebih baik pada saat optimalisasi sumber daya infrastruktur IT jaringan.
   
2. **Web/API Monitoring**, memantau fungsionalitas dan kinerja dari *website* atau API berbasis HTTP/HTTPS secara konsisten untuk memastikan proses bisnis berjalan secara optimal sehingga dapat mendukung *Customer Experience* yang lebih baik.
   
3. **Linux Server Monitoring**, mendeteksi visibilitas dan performansi *Linux Server* baik fisik maupun virtual untuk memastikan *Linux Server* tersebut beroperasi sesuai ekspektasi agar pengelolaan infrastruktur IT menjadi lebih baik dan mendukung bisnis berjalan dengan optimal.

