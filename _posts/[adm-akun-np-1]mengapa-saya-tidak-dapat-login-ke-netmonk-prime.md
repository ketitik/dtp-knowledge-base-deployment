---
title: 'Mengapa saya tidak dapat login ke Netmonk Prime?'
category: 'akun-netmonk-prime'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Periksa kembali username dan password'
---

# Mengapa saya tidak dapat login ke Netmonk Prime?

Periksa kembali *username* dan *password* yang Anda inputkan, jika pesan kesalahan yang muncul pada halaman *login* adalah seperti berikut:

![gagal login](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/akun-np/gagal-login.PNG)

Jika Anda baru pertama kali login ke Netmonk Prime,
pastikan *username* dan *password* tersebut sudah sesuai dengan yang tercantum pada email aktivasi
atau dapat dilihat di [Customer Portal](https://netmonk.id/portal/customer/login)
di halaman **Detail Subscription** seperti contoh berikut:

![detail-subscription](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/akun-np/detail-subscription.png)