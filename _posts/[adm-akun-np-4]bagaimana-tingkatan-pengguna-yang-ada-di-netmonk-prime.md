---
title: 'Bagaimana tingkatan pengguna yang ada di Netmonk Prime?'
category: 'akun-netmonk-prime'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Terdapat tiga tingkatan pengguna'
---

# Bagaimana tingkatan pengguna yang ada di Netmonk Prime?

Terdapat tiga tingkatan pengguna (user), yaitu sebagai berikut:

<table>
	<thead>
		<tr>
			<th rowspan=2 colspan=2>Hak Akses (Rule)</th>
			<th colspan=3>Role</th>
		</tr>
		<tr>
			<th>Root</th>
			<th>Admin</th>
			<th>User</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td colspan=5>Modul Network Monitoring</td>
		</tr>
		<tr>
			<td>1.</td>
			<td>Mengakses Halaman <i>Dashboard Network Summary</i></td>
			<td>✓</td>
			<td>✓</td>
			<td>✓</td>
		</tr>
		<tr>
			<td>2.</td>
			<td>Mengakses Halaman <i>Dashboard Network Map</i></td>
			<td>✓</td>
			<td>✓</td>
			<td>✓</td>
		</tr>
		<tr>
			<td>3.</td>
			<td>Mengakses Halaman <i>Device Status</i></td>
			<td>✓</td>
			<td>✓</td>
			<td>✓</td>
		</tr>
		<tr>
			<td>4.</td>
			<td>Mengakses Halaman <i>Link Utilization</i></td>
			<td>✓</td>
			<td>✓</td>
			<td>✓</td>
		</tr>
		<tr>
			<td>5.</td>
			<td>Mengakses Halaman <i>Starred Link</i></td>
			<td>✓</td>
			<td>✓</td>
			<td>✓</td>
		</tr>
		<tr>
			<td>6.</td>
			<td>Mengakses Halaman <i>CPU Status</i></td>
			<td>✓</td>
			<td>✓</td>
			<td>✓</td>
		</tr>
		<tr>
			<td>7.</td>
			<td>Mengakses Halaman <i>RAM Status</i></td>
			<td>✓</td>
			<td>✓</td>
			<td>✓</td>
		</tr>
		<tr>
			<td>8.</td>
			<td>Mengakses Halaman <i>Storage Status</i></td>
			<td>✓</td>
			<td>✓</td>
			<td>✓</td>
		</tr>
		<tr>
			<td>9.</td>
			<td>Mengakses Halaman <i>DiskIO Status</i></td>
			<td>✓</td>
			<td>✓</td>
			<td>✓</td>
		</tr>
		<tr>
			<td>10.</td>
			<td>Mengakses Halaman <i>Predictive Analysis Link</i></td>
			<td>✓</td>
			<td>✓</td>
			<td>✓</td>
		</tr>
		<tr>
			<td>11.</td>
			<td>Mengakses Halaman <i>Predictive Analysis Memory</i></td>
			<td>✓</td>
			<td>✓</td>
			<td>✓</td>
		</tr>
		<tr>
			<td>12.</td>
			<td>Mengakses Halaman <i>Predictive Analysis Storage</i></td>
			<td>✓</td>
			<td>✓</td>
			<td>✓</td>
		</tr>
		<tr>
			<td>13.</td>
			<td>Mengakses Halaman <i>Device Reporting</i></td>
			<td>✓</td>
			<td>✓</td>
			<td>✓</td>
		</tr>
		<tr>
			<td>14.</td>
			<td>Mengakses Halaman <i>Interface Reporting</i></td>
			<td>✓</td>
			<td>✓</td>
			<td>✓</td>
		</tr>
		<tr>
			<td>15.</td>
			<td>Mengakses Halaman <i>Network Device</i></td>
			<td>✓</td>
			<td>✓</td>
			<td>✓</td>
		</tr>
		<tr>
			<td>16.</td>
			<td>Mengakses Halaman <i>Network Probe</i></td>
			<td>✓</td>
			<td>✓</td>
			<td>✓</td>
		</tr>
		<tr>
			<td>17.</td>
			<td>Menambah, Mengubah, dan Menghapus Data <i>Network Map</i></td>
			<td>✓</td>
			<td>✓</td>
			<td>-</td>
		</tr>
		<tr>
			<td>18.</td>
			<td>Menambah dan Menghapus Data <i>Starred Link</i></td>
			<td>✓</td>
			<td>✓</td>
			<td>-</td>
		</tr>
		<tr>
			<td>19.</td>
			<td>Menambah, Mengubah, dan Menghapus <i>Network Device</i></td>
			<td>✓</td>
			<td>✓</td>
			<td>-</td>
		</tr>
		<tr>
			<td>20.</td>
			<td>Menambah, Mengubah, dan Menghapus Data <i>Network Probe</i></td>
			<td>✓</td>
			<td>✓</td>
			<td>-</td>
		</tr>
		<tr>
			<td colspan=5>Modul Web/API Monitoring</td>
		</tr>
		<tr>
			<td>1.</td>
			<td>Melihat <i>Web/API Status</i> dan Men-download <i>Report</i></td>
			<td>✓</td>
			<td>✓</td>
			<td>✓</td>
		</tr>
		<tr>
			<td>2.</td>
			<td>Menambah, Mengedit <i>Duplicate, Pause</i> dan Menghapus Data <i>Web/API</i></td>
			<td>✓</td>
			<td>✓</td>
			<td>✓</td>
		</tr>
		<tr>
			<td>3.</td>
			<td>Mengakses Halaman <i>Web/API Probe</i></td>
			<td>✓</td>
			<td>✓</td>
			<td>✓</td>
		</tr>
		<tr>
			<td>4.</td>
			<td>Menambah, Mengubah, dan Menghapus <i>Web/API Probe</i></td>
			<td>✓</td>
			<td>✓</td>
			<td>-</td>
		</tr>
		<tr>
			<td colspan=5>Modul Linux Server Monitoring</td>
		</tr>
		<tr>
			<td>1.</td>
			<td>Mengakses Halaman <i>Dashboard</i></td>
			<td>✓</td>
			<td>✓</td>
			<td>✓</td>
		</tr>
		<tr>
			<td>2.</td>
			<td>Mengakses Halaman <i>Linux Server Status</i> </td>
			<td>✓</td>
			<td>✓</td>
			<td>✓</td>
		</tr>
		<tr>
			<td>3.</td>
			<td>Melihat Detail<i>Linux Server Status</i> dan Men-download <i>Report</i></td>
			<td>✓</td>
			<td>✓</td>
			<td>✓</td>
		</tr>
		<tr>
			<td>4.</td>
			<td>Menambah dan Mengubah Data <i>Linux Server</i></td>
			<td>✓</td>
			<td>✓</td>
			<td>-</td>
		</tr>
		<tr>
			<td>5.</td>
			<td><i>Install</i> dan <i>Update Agent</i></td>
			<td>✓</td>
			<td>✓</td>
			<td>-</td>
		</tr>
		<tr>
			<td colspan=5>Halaman Setting</td>
		</tr>
		<tr>
			<td>1.</td>
			<td>Mengakses Halaman <i>Alert</i></td>
			<td>✓</td>
			<td>✓</td>
			<td>✓</td>
		</tr>
		<tr>
			<td>2.</td>
			<td>Mengakses Halaman <i>User</i> (Pengguna)</td>
			<td>✓</td>
			<td>✓</td>
			<td>-</td>
		</tr>
		<tr>
			<td>3.</td>
			<td>Mengakses Halaman <i>Location</i> (Lokasi)</td>
			<td>✓</td>
			<td>✓</td>
			<td>✓</td>
		</tr>
		<tr>
			<td>5.</td>
			<td>Menambah, Mengubah, dan Menghapus Data Pengguna</td>
			<td>✓</td>
			<td>-</td>
			<td>-</td>
		</tr>
		<tr>
			<td>4.</td>
			<td>Menambah dan Menghapus Data Loaksi</td>
			<td>✓</td>
			<td>✓</td>
			<td>-</td>
		</tr>
		<tr>
			<td colspan=5>Halaman Help</td>
		</tr>
		<tr>
			<td>1.</td>
			<td>Mengakses <i>Guideline</i> atau Petunjuk Penggunaan</td>
			<td>✓</td>
			<td>✓</td>
			<td>✓</td>
		</tr>
		<tr>
			<td>2.</td>
			<td>Mengakses <i>Knowledge Center</i></td>
			<td>✓</td>
			<td>✓</td>
			<td>-</td>
		</tr>
		<tr>
			<td>3.</td>
			<td>Mengakses <i>Support Center</i></td>
			<td>✓</td>
			<td>✓</td>
			<td>✓</td>
		</tr>
		<tr>
			<td colspan=5>Halaman System Performance</td>
		</tr>
		<tr>
			<td>1.</td>
			<td>Mengakses Halaman <i>System Performance</i></td>
			<td>✓</td>
			<td>✓</td>
			<td>✓</td>
		</tr>
	</tbody>
</table>
