---
title: 'Bagaimana cara melihat detail penggunaan Storage tertentu?'
category: 'storage-status'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Akses menu Network Monitoring → Storage Status'
---

# Bagaimana cara melihat detail penggunaan Storage tertentu?

1. Akses menu **Network Monitoring → Storage Status**

    ![Menu Storage Status](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B6%5Dstorage-status/menu-storage-status-v2.png)

2. Dari halaman utama menu *Storage Status*, klik tombol **View** di salah satu baris perangkat.

    ![Tombol View Detail](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B6%5Dstorage-status/tombol-view-detail.png)

3. Maka akan muncul tampilan seperti berikut:
   
   ![Detail Storage Status](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B6%5Dstorage-status/detail-storage-status-v2.png)

   Keterangan:
   a. Informasi dasar seperti nama perangkat, alamat IP, dan indeks *storage*.
   b. Grafik *history* penggunaan *storage* pada periode waktu tertentu.
   c. Kategori status penggunaan dan persentase penggunaan terakhir *storage* dari total kapasitas *storage*.