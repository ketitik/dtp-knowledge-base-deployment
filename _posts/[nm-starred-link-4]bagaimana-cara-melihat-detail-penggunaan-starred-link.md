---
title: 'Bagaimana cara melihat detail penggunaan dari link/port/interface yang dikategorikan sebagai starred link?'
category: 'starred-link'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Akses menu menu Network Status → Starred Link'
---

# Bagaimana cara melihat detail penggunaan dari link/port/interface yang dikategorikan sebagai starred link?

1. Akses menu menu **Network Status → Starred Link**
   
    ![Menu Starred Link](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B3%5Dstarred-link/menu-starred-link-v2.png)

2. Dari halaman utama menu Starred Link, klik tombol **View** di salah satu baris data

    ![Tombol View Detail](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B3%5Dstarred-link/tombol-view-detail.png)

3. Maka akan muncul tampilan seperti berikut:

    ![Detail Starred Link](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B3%5Dstarred-link/detail-starred-link-v2.png)

    Keterangan:
    a. Informasi dasar seperti nama perangkat, alamat IP, dan nama dari *starred link*.
    b. Rata-rata trafik paket data yang keluar dari total kapasitas.
    c. Rata-rata trafik paket data yang masuk dari total kapasitas.
    d. Grafik *history* trafik paket data yang masuk (diwakili warna biru) dan keluar (diwakili warna hijau) pada periode waktu tertentu.
    e. Grafik *history* jumlah paket data yang mengalami *error* saat diterima pada periode waktu tertentu.
    f. Grafik *history* jumlah paket data yang mengalami *discard* atau ditolak oleh penerima pada periode waktu tertentu.
    g. Grafik *history* status *starred link* pada periode waktu tertentu.
    h. *Ratio availability* atau tingkat ketersediaan *link/port/interface* yang dikategorikan sebagai *starred link*.