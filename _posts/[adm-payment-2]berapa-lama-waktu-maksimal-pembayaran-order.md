---
title: 'Berapa lama waktu maksimal pembayaran order?'
category: 'payment'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Waktu maksimal pembayaran order'
---

# Berapa lama waktu maksimal pembayaran order?

Waktu maksimal pembayaran order adalah 2 x 24 jam terhitung sejak order dibuat.
