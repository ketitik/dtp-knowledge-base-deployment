---
title: 'Kapan invoice order dan faktur pajak keluar?'
category: 'payment'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Invoice keluar setelah order teraktivasi'
---

# Kapan invoice order dan faktur pajak keluar?

*Invoice* keluar setelah order teraktivasi, sedangkan faktur pajak keluar maksimal H+7 setelah order teraktivasi.