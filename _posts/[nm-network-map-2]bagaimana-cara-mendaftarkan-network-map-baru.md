---
title: 'Bagaimana cara mendaftarkan Network Map baru?'
category: 'network-map'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Akses menu Dashboard → Network Map'
---

# Bagaimana cara mendaftarkan Network Map baru?

1. Akses menu **Dashboard → Network Map**

    ![Menu Network Map](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B8%5Dnetwork-map/menu-network-map.png)

2. Dari halaman utama *Dashboard Network Map*, klik tombol **Add Map**,

    ![Tombol Add Map](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B8%5Dnetwork-map/tombol-tambah-network-map.png)

3. Selanjutnya akan muncul *form* untuk mengisi detail *map* yang akan ditambahkan.
   
    ![Form Tambah Network Map](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B8%5Dnetwork-map/form-tambah-network-map.png)

4. Isi data pada *form* tersebut dengan rincian sebagai berikut:

    | **Jenis Isian** | **Keterangan** |
    | ----- | ----- |
    | Name | Nama dari *map* yang ingin ditambahkan ke *dashboard*. |
    | Map Description | Informasi tambahan terkait *map* yang akan ditambahkan (opsional). |

5. Klik tombol **Add** untuk menambahkan *map* baru.