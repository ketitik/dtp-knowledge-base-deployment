---
title: 'Bagaimana cara mengganti password akun?'
category: 'akun-customer-portal'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Pilih menu Pengaturan → Kata Sandi'
---

# Bagaimana cara mengganti password akun?

Pilih menu **Pengaturan → Kata Sandi**, isi *form* yang tersedia dengan kata sandi saat ini dan kata sandi yang baru, kemudian konfirmasi untuk menyimpan kata sandi baru tersebut. Untuk lebih jelasnya, ikuti langkah berikut:

1. Masuk ke halaman pengaturan akun melalui menu **Pengaturan → Kata Sandi**

    ![Menu Kata Sandi](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/akun-cp/menu-kata-sandi.png)

2. Selanjutnya, akan tampil *form* untuk melakukan perubahan kata sandi pengguna sebagai berikut:

    ![Ubah Kata Sandi](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/akun-cp/ubah-kata-sandi.png)

3. Isi kolom **Kata Sandi Saat ini** dengan kata sandi Anda saat ini

    ![Kata Sandi Saat ini](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/akun-cp/kata-sandi-saat-ini.png)

4. Isi kolom **Kata Sandi Baru** dengan kata sandi baru Anda. Pastikan kata sandi tersebut setidaknya terdiri dari 8 karakter, mengandung huruf besar, huruf kecil, serta angka

    ![Kata Sandi Baru](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/akun-cp/kata-sandi-baru.png)

5. Jika kata sandi baru Anda tidak memenuhi ketentuan di atas, maka akan muncul pesan kesalahan seperti contoh berikut:

    ![Invalid Kata Sandi](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/akun-cp/invalid-kata-sandi.png)

6. Selanjutnya, konfirmasi kata sandi baru dengan mengetikkannya kembali di kolom **Konfirmasi Kata Sandi Baru**

    ![Konfirmasi Kata Sandi](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/akun-cp/konfirmasi-kata-sandi.png)

7. Klik tombol **Ubah Kata Sandi** untuk menyimpan kata sandi baru Anda

    ![Tombol Ubah Kata Sandi](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/akun-cp/tombol-ubah-kata-sandi.png)

8. Jika isi kata sandi baru di kolom **Kata Sandi Baru** dan **Konfirmasi Kata Sandi Baru** berbeda, maka akan muncul pesan kesalahan seperti berikut:

    ![Konfirmasi Kata Sandi Tidak Cocok](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/akun-cp/konfirmasi-kata-sandi-tidak-cocok.png)

9. Jika benar, maka akan muncul notifikasi bahwa kata sandi berhasil diubah seperti contoh berikut: 
    
    ![Sukses Ubah Kata Sandi](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/akun-cp/sukses-ubah-kata-sandi.png)