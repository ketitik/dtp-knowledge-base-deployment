---
title: 'Apa spesifikasi dari server atau komputer yang akan diinstal probe Netmonk Prime?'
category: 'panduan-pengguna-baru'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Spesifikasi yang kami sarankan'
---

# Apa spesifikasi dari server atau komputer yang akan diinstal probe Netmonk Prime?

Spesifikasi yang kami sarankan adalah server atau komputer bersistem operasi Ubuntu Server, dengan Multi Core vCPU, RAM 2 GB, Media Penyimpanan 20 GB, dan mendukung koneksi jaringan Gigabit Ethernet dan akses internet.
