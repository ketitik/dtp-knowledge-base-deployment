---
title: 'Bagaimana cara menghapus data pengguna yang sudah saya daftarkan?'
category: 'akun-netmonk-prime'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Akses menu Settings → Users'
---

# Bagaimana cara menghapus data pengguna yang sudah saya daftarkan?

1. Akses menu **Settings → Users**

    ![settings-users](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/akun-np/settings-users.png)

2. Dari halaman utama menu *Users Setting* atau manajemen pengguna, klik tombol **Delete** di salah satu baris data pengguna.

    ![delete-user-button](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/akun-np/delete-user-button.png)

3. Selanjutnya akan tampil kotak dialog seperti berikut:

    ![delete-prompt-generic](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/akun-np/delete-prompt-generic.png)

4. Klik tombol **Delete** untuk melanjutkan proses hapus.