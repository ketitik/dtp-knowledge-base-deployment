---
title: 'Bagaimana cara menghapus interface reporting tertentu?'
category: 'reporting'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Akses menu Network Reporting → Interface Reporting'
---

# Bagaimana cara menghapus interface reporting tertentu?

1. Akses menu **Network Reporting → Interface Reporting**.

    ![Menu Interface Reporting](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B13%5Dreporting/menu-interface-reporting-v2.png)

2. Dari halaman utama menu *Interface Reporting*, klik tombol **Remove** di salah satu baris data,

    ![Tombol Remove](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B13%5Dreporting/tombol-remove-interface-reporting.png)

3. Selanjutnya akan tampil kotak dialog seperti berikut:

    ![Hapus Data](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B13%5Dreporting/hapus-data.png)

4. Klik tombol **Delete** untuk melanjutkan proses hapus atau tombol **Cancel** untuk membatalkan.