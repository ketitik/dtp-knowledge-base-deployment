---
title: 'Bagaimana cara melihat detail prediksi tren dari link/port/interface tertentu?'
category: 'predictive-analysis'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Akses menu Predictive Analysis → Link'
---

# Bagaimana cara melihat detail prediksi tren dari link/port/interface tertentu?

1. Akses menu **Predictive Analysis → Link**

    ![Menu Predictive Analysis Link](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B12%5Dpredictive-analysis/menu-predictive-analysis-link-v2.png)

2. Dari halaman utama menu *Predictive Analysis Link*, klik tombol **View** di salah satu baris data.

    ![Tombol View](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B12%5Dpredictive-analysis/tombol-view-detail.png)

3. Maka akan muncul tampilan seperti berikut:

    ![Detail Predictive Analysis Link](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B12%5Dpredictive-analysis/detail-predictive-link-v2.png)

    Keterangan:
    a. Informasi dasar seperti nama perangkat, alamat IP, dan nama *link/port/interface*.

    b. Jumlah hari yang menunjukkan berapa lama lagi trafik akan mencapai ambang batas.

    c. Persentase trafik data dari total kapasitas.

    d. Grafik tren trafik untuk *upstream* jika diasumsikan trafik data di masa mendatang di bawah 50% (diwakili warna biru), antara 50% dan 80% (diwakili warna kuning), dan di atas 80% (diwakili warna hijau), serta *threshold* atau ambang batas (diwakili warna merah) dan grafik *history* trafik data untuk *upstream* (diwakili warna abu).

    e. Grafik tren trafik untuk *downstream* jika diasumsikan trafik data di masa mendatang di bawah 50% (diwakili warna biru), antara 50% dan 80% (diwakili warna kuning), dan di atas 80% (diwakili warna hijau), serta *threshold* atau ambang batas (diwakili warna merah) dan grafik *history* trafik data untuk downstream (diwakili warna abu).