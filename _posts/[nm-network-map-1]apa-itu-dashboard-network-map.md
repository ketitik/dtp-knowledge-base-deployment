---
title: 'Apa itu dashboard Network Map?'
category: 'network-map'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Dashboard Network Map menampilkan overview'
---

# Apa itu dashboard Network Map?

*Dashboard Network Map* menampilkan *overview* atau gambaran persebaran perangkat-perangkat yang dimonitor sehingga mempermudah pengguna dalam melihat perangkat mana yang sedang bermasalah dan perlu pengawasan. Halaman tersebut dapat diakses melalui menu **Dashboard → Network Map**. Berikut contoh tampilan halaman utama *Dashboard Network Map*:

![Dashboard Network Map](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B8%5Dnetwork-map/network-map.png)