---
title: 'Bagaimana cara melihat detail prediksi tren penggunaan storage tertentu?'
category: 'predictive-analysis'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Akses menu Predictive Analysis → Storage'
---

# Bagaimana cara melihat detail prediksi tren penggunaan storage tertentu?

1. Akses menu **Predictive Analysis → Storage**

    ![Menu Predictive Analysis Storage](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B12%5Dpredictive-analysis/menu-predictive-analysis-storage-v2.png)

2. Dari halaman utama menu *Predictive Analysis Storage*, klik tombol **View** di salah satu baris data.

    ![Tombol View Detail](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B12%5Dpredictive-analysis/tombol-view-detail.png)

3. Maka akan muncul tampilan seperti berikut:

    ![Detail Predictive Storage](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B12%5Dpredictive-analysis/detail-predictive-storage-v2.png)

    Keterangan:
    a. Informasi dasar seperti nama perangkat, alamat IP, dan indeks *storage*.
    b. Jumlah hari yang menunjukkan berapa lama lagi penggunaan *storage* akan mencapai ambang batas.
    c. Persentase penggunaan *storage* dari total kapasitas penyimpanan.
    d. Grafik tren penggunaan *storage* jika diasumsikan penggunaan *storage* di masa mendatang di bawah 50% (diwakili warna hijau), antara 50% dan 80% (diwakili warna kuning), dan di atas 80% (diwakili warna merah), serta *threshold* atau ambang batas (diwakili warna biru tua) dan grafik *history* penggunaan *storage* (diwakili warna biru muda).