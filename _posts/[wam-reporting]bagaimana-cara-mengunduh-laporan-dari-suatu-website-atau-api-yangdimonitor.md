---
title: 'Bagaimana cara mengunduh laporan dari suatu website atau API yang dimonitor?'
category: 'reporting-web-api'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Akses menu Web/API Monitoring → Web/API Status'
---

# Bagaimana cara mengunduh laporan dari suatu website atau API yang dimonitor?

1. Akses menu **Web/API Monitoring → Web/API Status**.

    ![Menu Web Status](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/web-api-monitoring/%5B5%5Dreporting/menu-web-status.png)

2. Dari halaman utama *Web/API Status*, masuk ke halaman *Detail Web/API Status* dari *website* atau *API* tertentu.
   
3. Klik tombol **Download** yang berada di sebelah kanan atas *dashboard*.

    ![Tombol Download Web Status](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/web-api-monitoring/%5B5%5Dreporting/tombol-download-web-status.png)

4. Selanjutnya, akan tampil *preview* laporan dari *website* atau *API* yang bersangkutan.

    ![Preview Web Status Reporting](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/web-api-monitoring/%5B5%5Dreporting/preview-web-status-reporting.png)

5. Klik tombol **Print report** untuk mengunduh laporan ke dalam file berformat PDF.