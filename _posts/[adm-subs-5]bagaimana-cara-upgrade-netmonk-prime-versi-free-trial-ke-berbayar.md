---
title: 'Bagaimana cara upgrade Netmonk Prime versi Free Trial ke versi berbayar?'
category: 'subscription'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Masuk ke halaman subscription'
---

# Bagaimana cara upgrade Netmonk Prime versi Free Trial ke versi berbayar?

1. Masuk ke halaman *subscription* melalui menu **Subscription** seperti berikut:

    ![Menu Subscription](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/subscription/menu-subscription.png)

2. Klik tombol **Detail** pada data order Netmonk Prime versi *Free Trial* yang ingin di-*upgrade* seperti contoh berikut:

    ![Order Free Trial yang Ingin di-Upgrade](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/subscription/free-trial-teraktivasi.png)

3. Setelah masuk ke halaman **Detail Subscription**, klik tombol **Perpanjang Masa Aktif**

    ![Tombol Perpanjang Masa Aktif](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/subscription/tombol-perpanjang-masa-aktif.png)

4. Selanjutnya, akan muncul detail paket yang dapat Anda pilih

    ![Pemilihan Detail Paket](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/subscription/pemilihan-detail-paket.png)

5. Pilih salah satu paket, kemudian klik tombol **Pilih**
   
6. Isi data pemesanan dengan data perusahaan Anda, kemudian klik tombol **Simpan & Lanjutkan**

    ![Melengkapi Data Pemesan](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/subscription/melengkapi-data-pemesan.png)

7. Berikutnya, akan muncul ringkasan order seperti contoh berikut:

    ![Ringkasan Order](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/subscription/ringkasan-order.png)

8. Pada bagian **Ringkasan Order** di atas, Anda dapat mengubah data paket atau data pemesanan dengan menekan tombol **Ubah** pada data yang bersangkutan, atau melanjutkan proses order dengan menekan tombol **Bayar order**

9.  Sebelum masuk ke bagian pembayaran, pastikan data yang Anda inputkan sudah sesuai, kemudian klik **Ya, saya yakin** pada kotak dialog berikut:

    ![Konfirmasi Order](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/subscription/konfirmasi-order.png)

10. Setelah itu, konfirmasi pembayaran order dengan menekan tombol **Bayar order**

    ![Konfirmasi Pembayaran Order](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/subscription/konfirmasi-pembayaran-order.png)

11. Selanjutnya, akan muncul detail pembayaran untuk order yang Anda buat seperti contoh berikut:

    ![Detail Pembayaran Order](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/subscription/detail-pembayaran-order.png)

12. Silakan melakukan pembayaran ke rekening yang tercantum di di atas. Jika pembayaran sudah tervalidasi, order akan segera diproses. Waktu maksimal order tersebut diproses hingga teraktivasi adalah 5 hari kerja. Status orderan dapat Anda lihat di halaman utama menu **Subscription** pada kolom **Status Order Terakhir** seperti contoh berikut:

    ![Status Order Masuk Antrian](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/subscription/paket-berbayar-masuk-antrian.png)

13. Dari gambar di atas, klik tombol **Detail**, maka Anda akan melihat rincian status order di bagian seperti berikut:

    ![Rincian Status Order](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/subscription/rincian-order-masuk-antrian.png)

14. Jika order sudah teraktivasi, Anda akan mendapatkan alamat Netmonk Prime beserta *username* dan *password* yang dapat Anda gunakan seperti pada tampilan berikut:

    ![Detail Subscription](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/subscription/detail-subscription.png)

15. Pada halaman **Detail Subscription** di atas, Anda juga dapat melihat faktur pajak dengan menekan tombol **Lihat Faktur** atau melihat *invoice* dari order yang sudah dilakukan dengan menekan tombol **Lihat Invoice** pada bagian **Rincian Order Subscription**

    ![Lihat Faktur dan Invoice](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/subscription/lihat-faktur-dan-invoice.png)

16. Jika kembali ke halaman utama menu subscription, status order akan berubah menjadi **Order Teraktivasi** dan status *subscription* berubah menjadi **Active**

    ![Status Order Teraktivasi](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/subscription/paket-berbayar-teraktivasi.png)
