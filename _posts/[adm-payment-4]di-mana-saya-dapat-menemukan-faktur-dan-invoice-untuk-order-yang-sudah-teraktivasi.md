---
title: 'Di mana saya dapat menemukan faktur dan invoice untuk order yang sudah teraktivasi?'
category: 'payment'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Masuk ke halaman subscription'
---

# Di mana saya dapat menemukan faktur dan invoice untuk order yang sudah teraktivasi?

1. Masuk ke halaman *subscription* melalui **Subscription** seperti berikut:

    ![Menu Subscription](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/subscription/menu-subscription.png)

2. Klik tombol **Detail** pada data order Netmonk Prime versi berbayar yang bersangkutan. Untuk mendapatkan faktur pajak dan invoice order, pastikan status order sudah teraktivasi

    ![List Subscription](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/subscription/list-subscription.png)

3. Setelah masuk halaman **Detail Subscription**, Anda bisa mendapatkan faktur pajak dengan menekan tombol **Lihat Faktur** atau mendapatkan *invoice* dari order yang sudah dilakukan dengan menekan tombol **Lihat Invoice** pada bagian **Rincian Order Subscription**

    ![Lihat Faktur dan Invoice](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/subscription/lihat-faktur-dan-invoice.png)