---
title: 'Bagaimana cara melihat detail penggunaan dari link/port/interface tertentu?'
category: 'link-utilization'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: ' Akses menu menu Network Monitoring → Link Utilization'
---

# Bagaimana cara melihat detail penggunaan dari link/port/interface tertentu?

1. Akses menu menu **Network Status → Link Utilization**

    ![Menu Link Utilization](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B2%5Dlink-utilization/menu-link-utilization-v2.png)

2. Dari halaman utama menu *Link Utilization*, klik tombol **View** di salah satu baris data

    ![Tombol View Detail](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B2%5Dlink-utilization/tombol-view-detail.png)

3. Maka akan muncul tampilan seperti berikut:
   
   ![Detail Link Utilization](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B2%5Dlink-utilization/detail-link-utilization-v2.png)

   Keterangan:
   a. Informasi dasar seperti nama perangkat, alamat IP, dan nama *link/port/interface*
   b. Rata-rata trafik paket data yang keluar dari total kapasitas.
   c. Rata-rata trafik paket data yang masuk dari total kapasitas.
   d. Grafik *history* trafik paket data yang masuk (diwakili warna biru) dan keluar (diwakili warna hijau) pada periode waktu tertentu.
   e. Grafik *history* jumlah paket data yang mengalami *error* saat diterima pada periode waktu tertentu.
   f. Grafik *history* jumlah paket data yang mengalami *discard* atau ditolak oleh penerima pada periode waktu tertentu.
   g. Grafik *history* status *link/port/interface* pada periode waktu tertentu.
   h. *Ratio availability* atau tingkat ketersediaan *link/port/interface* tertentu.