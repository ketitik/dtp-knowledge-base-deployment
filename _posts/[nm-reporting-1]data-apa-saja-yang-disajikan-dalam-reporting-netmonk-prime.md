---
title: 'Data apa saja yang disajikan dalam reporting Netmonk Prime?'
category: 'reporting'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Terdapat dua jenis reporting yang disediakan'
---

# Data apa saja yang disajikan dalam reporting Netmonk Prime?

Terdapat dua jenis reporting yang disediakan Netmonk Prime, yaitu *Device Reporting* dan *Interface Reporting*. *Device Reporting* menyajikan rangkuman terkait perangkat yang dimonitor selama periode tertentu, seperti *availability* perangkat, penggunaan *link/port/interface*, serta penggunaan CPU, RAM, dan *Storage*. Sedangkan *Interface Reporting* menyajikan laporan berupa *availability* atau ketersediaan *link/port/interface* tertentu dari suatu perangkat.

Pada *Device Reporting*, Anda dapat memilih data apa saja yang ingin ditampilkan di dalam laporan dengan mencentang *checkbox* yang terdapat di bagian **Data Displayed**, yang nantinya menghasilkan informasi berikut:

a. **Availability**, menampilkan informasi berupa daftar *node*, alamat IP masing-masing *node*, *uptime* atau waktu hidup setiap *node*, *ratio availability* untuk setiap *node* dari deteksi via ICMP dan SNMP *Uptime*.

b. **Link**, menampilkan informasi berupa daftar *link/port/interface*, alamat IP dan nama *node* dari masing-masing *interface*, persentase minimal dan maksimal dari trafik yang masuk dan keluar, serta rata-rata dari trafik tersebut.

c. **CPU**, menampilkan informasi berupa daftar indeks CPU, alamat IP dan nama *node* dari masing-masing CPU, serta persentase penggunaan minimal, maksimal, dan rata-rata setiap CPU.

d. **RAM**, menampilkan informasi berupa daftar indeks *memory*, alamat IP dan nama *node* dari masing-masing *memory*, serta persentase penggunaan minimal, maksimal, dan rata-rata setiap *memory*.

e. **Storage**, menampilkan informasi berupa daftar indeks *storage* atau media penyimpanan, alamat IP dan nama *node* dari masing-masing *storage*, serta persentase penggunaan minimal, maksimal, dan rata-rata setiap *storage*.