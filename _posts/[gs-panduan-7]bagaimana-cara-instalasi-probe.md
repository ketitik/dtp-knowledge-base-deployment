---
title: 'Bagaimana cara instalasi Probe?'
category: 'panduan-pengguna-baru'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Install Docker'
---

# Bagaimana cara instalasi Probe?

## Install Docker di Ubuntu

1. Perbarui sistem dengan perintah berikut:
    ```sh
    $ sudo apt update
    ```

2. Setelah sistem diperbarui, instal *package* yang dibutuhkan dengan menjalankan perintah ini:
    ```sh
    $ sudo apt install apt-transport-https ca-certificates curl software-properties-common
    ```

3. Masukkan perintah berikut untuk menambahkan *GPG Key*:
    ```sh
    $ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    ```

4. Jalankan perintah berikut untuk menambahkan repositori:
    ```sh
    $ sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
    ```

5. Selanjutnya, perbarui informasi sistem melalui perintah berikut:
    ```sh
    $ sudo apt update
    ```

6. Lakukan instalasi melalui *Docker* repositori, dan bukan *default* Ubuntu repositori dengan mengetikkan perintah ini:
    ```sh
    $ apt-cache policy docker-ce
    ```

7. Masukan perintah berikut untuk Instal *Docker*:
    ```sh
    $ sudo apt install docker-ce
    ```

8. Terakhir, cek status Docker setelah proses instalasi selesai:
    ```sh
    $ sudo systemctl status docker
    ```


## Install Docker Compose di Ubuntu

1. Perbarui sistem dengan perintah berikut:
    ```sh
    $ sudo apt update
    ```

2. Download versi stabil *Docker Compose* dengan menjalankan perintah di bawah ini:
    ```sh
    $ sudo curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
    ```

3. Ubah *permission* dari file *Docker Compose*:
    ```sh
    $ sudo chmod +x /usr/local/bin/docker-compose
    ```

4. Kemudian cek apakah *Docker Compose* sudah berhasil diinstal atau tidak dengan perintah berikut:
    ```sh
    $ docker-compose --version
    ```


## Instalasi Network Probe

Berikut petunjuk instalasi *network probe* tambahan yang bertugas mengumpulkan data dari perangkat jaringan yang akan dimonitor oleh Netmonk Prime:

1. Unduh *template* probe dengan menjalankan perintah berikut:
    ```sh
    $ wget "https://netmonk-public.s3-ap-southeast-1.amazonaws.com/ext-agent/ext-agent.zip"
    ```

2. Ekstraksi file zip yang telah diunduh:
    ```sh
    $ unzip ext-agent.zip
    $ cd ext-agent
    ```

3. Modifikasi file konfigurasi **hades-bea.yaml** yang terdapat pada folder **conf/bea-network/bea** sesuai kebutuhan:
    ```sh
    $ nano conf/bea-network/bea/hades-bea.yaml

        ```
        service_data:
        name: "<nama unik>"
        key: "<kata kunci>"
        ```
    ```

4. Modifikasi file konfigurasi **docker-compose.yaml** bagian **network-bea-network-hybrid** agar mengarah ke server Netmonk Cloud, serta berikan *comment* atau hapus bagian **network-bea-website-hybrid** jika Anda hanya ingin menginstal *network probe* saja seperti berikut:
    ```sh
    $ nano docker-compose.yaml

        ```
        services:
        netmonk-bea-network-hybrid:
        ```
        extra_hosts:
        - "message-broker.netmonk.id:<alamat IP yang diinfokan oleh tim Support Netmonk>"
        ```
        #netmonk-bea-website-hybrid:
        #```
    ```

5. Hidupkan *network probe* dengan perintah berikut:
    ```sh
    $ ./up.sh
    ```

6. Terakhir, daftarkan *network probe* tambahan ke menu **Inventory → Network Probe**


## Instalasi Web/API Probe

Berikut petunjuk instalasi *web/API probe* tambahan yang bertugas mengumpulkan data dari *website* atau *API* berbasis HTTP/HTTPS yang akan dimonitor oleh Netmonk Prime:

1. Unduh template probe dengan menjalankan perintah berikut:
    ```sh
    $ wget "https://netmonk-public.s3-ap-southeast-1.amazonaws.com/ext-agent/ext-agent.zip"
    ```

2. Ekstraksi file zip yang telah diunduh:
    ```sh
    $ unzip ext-agent.zip
    $ cd ext-agent
    ```

3. Modifikasi file konfigurasi **hades-bea.yaml** yang terdapat pada folder **conf/bea-website/bea** sesuai kebutuhan:
    ```sh
    $ nano conf/bea-website/bea/hades-bea.yaml

        ```
        service_data:
        name: "<nama unik>"
        key: "<kata kunci>"
        ```
    ```

4. Modifikasi file konfigurasi **docker-compose.yaml** bagian **network-bea-website-hybrid** agar mengarah ke server Netmonk Cloud, serta berikan *comment* atau hapus bagian **network-bea-network-hybrid** jika Anda hanya ingin menginstal *web/API probe* saja seperti berikut:
    ```sh
    $ nano docker-compose.yaml

        ```
        services:
        #netmonk-bea-network-hybrid:
        #```
        netmonk-bea-website-hybrid:
        ```
        extra_hosts:
        - "message-broker.netmonk.id:<alamat IP yang diinfokan oleh tim Support Netmonk>"
        ```
    ```

5. Hidupkan *web/API probe* dengan perintah berikut:
    ```sh
    $ ./up.sh
    ```

6. Terakhir, daftarkan *web/API probe* tambahan ke menu **Inventory → Web/API Probe**