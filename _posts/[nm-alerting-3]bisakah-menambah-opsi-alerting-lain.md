---
title: 'Bisakah menambah opsi alerting selain Telegram dan Email?'
category: 'alerting'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Jika Anda menginginkan opsi lain'
---

# Bisakah menambah opsi alerting selain Telegram dan Email?

Jika Anda menginginkan opsi lain sebagai media untuk menerima *alert*, Anda dapat menghubungi tim marketing kami melalui *WhatsApp* di nomor [berikut.](http://wa.me/6281382676435)