---
title: 'Apa perbedaan Subscription ID dan Order ID?'
category: 'subscription'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Subscription ID merupakan nomor unik'
---

# Apa perbedaan Subscription ID dan Order ID?

Subscription ID merupakan nomor unik yang merujuk pada suatu subscription. Dalam 1 subscription, Anda dapat melakukan banyak order seperti mencoba Netmonk Prime versi *Free Trial*, berlanggan Netmonk Prime versi berbayar, meng-*upgrade* Netmonk Prime *Free Trial* ke berbayar, atau memperpanjang masa aktif Netmonk Prime versi berbayar. Order-order tersebut dibedakan dengan Order ID yang merupakan nomor unik untuk membantu Anda dalam mengidentifikasi dan mencari order tertentu.