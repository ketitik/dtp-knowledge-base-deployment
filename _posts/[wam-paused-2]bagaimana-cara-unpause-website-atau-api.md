---
title: 'Bagaimana cara menjalankan kembali monitoring suatu website atau API yang sebelumnya dihentikan (Unpause)?'
category: 'paused'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Akses menu Web/API Monitoring → Web/API Status'
---

# Bagaimana cara menjalankan kembali monitoring suatu website atau API yang sebelumnya dihentikan (Unpause)?

1. Akses menu **Web/API Monitoring → Web/API Status**.

    ![Menu Web Status](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/web-api-monitoring/%5B4%5Dpaused/menu-web-status.png)

2. Dari halaman utama *Web/API Status*, klik tombol **Unpause Data** pada data *website* atau *API* yang sebelumnya di-*pause*.

    ![Tombol Unpause Web](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/web-api-monitoring/%5B4%5Dpaused/tombol-unpause-web.png)

3. Selanjutnya akan tampil kotak dialog seperti berikut:

    ![Unpause Web](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/web-api-monitoring/%5B4%5Dpaused/unpause-web.png)

4. Klik tombol **Unpause** untuk melanjutkan proses *unpause*.