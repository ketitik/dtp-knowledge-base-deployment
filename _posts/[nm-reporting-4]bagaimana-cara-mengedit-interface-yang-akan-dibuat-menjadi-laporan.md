---
title: 'Bagaimana cara mengedit data link/port/interface yang akan dibuat menjadi laporan?'
category: 'reporting'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Akses menu Network Reporting → Interface Reporting'
---

# Bagaimana cara mengedit data link/port/interface yang akan dibuat menjadi laporan?

1. Akses menu **Network Reporting → Interface Reporting**.

    ![Menu Interface Reporting](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B13%5Dreporting/menu-interface-reporting-v2.png)

2. Dari halaman utama menu *Interface Reporting*, klik tombol **Edit** di salah satu baris data,

    ![Tombol Edit Interface Reporting](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B13%5Dreporting/tombol-edit-interface-reporting.png)

3. Selanjutnya akan tampil *form* untuk mengedit data seperti berikut:

    ![Form Edit Interface Reporting](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B13%5Dreporting/form-edit-interface-reporting.png)

4. Edit data pada kolom **Title** dan **Prepared for** sesuai kebutuhan, kemudian klik tombol **Save** untuk menyimpan perubahan pada data.