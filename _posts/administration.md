---
title: 'Administration'
accordion: 'administration'
category: 'administration'
icon: reportings
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Apakah data akun saya dapat diedit?'
---

# AKUN CUSTOMER PORTAL

[1] Apakah data akun saya dapat diedit?

[2] Bagaimana cara mengganti password akun?

[3] Bagaimana jika saya tidak menerima kode OTP untuk verifikasi akun?

[4] Bagaimana jika saya gagal menginput kode OTP yang sudah saya terima?

## AKUN NETMONK PRIME

[1] Mengapa saya tidak dapat login ke Netmonk Basic?

[2] Bagaimana jika saya lupa kata sandi Netmonk Basic?

[3] Bagaimana cara mendaftarkan pengguna baru didalam Netmonk Basic?

[4] Bagaimana tingkatan pengguna yang ada di Netmonk Basic?

[5] Bagaimana cara mengedit data pengguna yang sudah saya daftarkan?

[6] Bagaimana cara menghapus data pengguna yang sudah saya daftarkan?

## SUBSCRIPTION

[1] Apa perbedaan Subscription ID dan Order ID?

[2] Bagaimana cara menambahkan Subscription untuk Netmonk Basic versi Free Trial?

[3] Apakah bisa menambah waktu free trial Netmonk Basic menjadi 14 hari?

[4] Bagaimana cara menambahkan Subscription untuk Netmonk Basic versi berbayar?

[5] Bagaimana cara upgrade Netmonk Basic versi Free Trial ke versi berbayar?

[6] Bagaimana cara melakukan perpanjangan masa aktif untuk Netmonk Basic berbayar?

[7] Bagaimana jika Subscription saya expired?

[8] Berapa lama waktu maksimal suatu order diproses hingga teraktivasi?

[9] Bagaimana cara melihat status order yang sudah dibuat?

[10] Bagaimana cara membatalkan status order yang sudah dibuat?

[11] Berapa lama waktu maksimal pembayaran order?

[12] Bagaimana cara melihat histori order yang pernah dibuat?

[13] Dimana saya dapat menemukan username, password, dan alamat Netmonk Basic yang sudah saya order?

## PAYMENT/PEMBAYARAN

[1] Bagaimana cara pembayaran untuk berlangganan Netmonk Basic ?

[2] Kapan invoice order dan faktur pajak keluar?

[3] Dimana saya dapat menemukan faktur dan invoice untuk order yang sudah teraktivasi?