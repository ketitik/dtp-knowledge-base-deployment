---
title: 'Bagaimana cara melihat detail penggunaan RAM tertentu?'
category: 'ram-status'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Akses menu Network Status → RAM Status'
---

# Bagaimana cara melihat detail penggunaan RAM tertentu?

1. Akses menu **Network Status → RAM Status**

    ![Menu RAM Status](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B5%5Dram-status/menu-ram-status-v2.png)

2. Dari halaman utama menu *RAM Status*, klik tombol **View** di salah satu baris perangkat.

    ![Tombol View Detail](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B5%5Dram-status/tombol-view-detail.png)

3. Maka akan muncul tampilan seperti berikut:
   
   ![Detail RAM Status](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B5%5Dram-status/detail-ram-status-v2.png)

   Keterangan:
   a. Informasi dasar seperti nama perangkat, alamat IP, dan indeks RAM.
   b. Grafik *history* penggunaan RAM pada periode waktu tertentu.
   c. Kategori status penggunaan dan persentase penggunaan terakhir RAM dari total kapasitas RAM.
