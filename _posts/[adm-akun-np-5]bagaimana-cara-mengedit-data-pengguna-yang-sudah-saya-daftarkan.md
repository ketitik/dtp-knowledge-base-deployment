---
title: 'Bagaimana cara mengedit data pengguna yang sudah saya daftarkan?'
category: 'akun-netmonk-prime'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Akses menu Settings → Users'
---

# Bagaimana cara mengedit data pengguna yang sudah saya daftarkan?

1. Akses menu **Settings → Users**

    ![settings-users](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/akun-np/settings-users.png)

2. Dari halaman utama menu *Users Setting* atau manajemen pengguna, klik tombol **Edit** di salah satu baris data pengguna.

    ![edit-user-button](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/akun-np/edit-user-button.png)

3. Selanjutnya akan tampil *form* untuk mengedit data seperti berikut:

    ![update-user-fill](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/akun-np/update-user-fill.png)

4. Edit data sesuai kebetuhan, kemudian klik tombol **Save** untuk menyimpan perubahan pada data.