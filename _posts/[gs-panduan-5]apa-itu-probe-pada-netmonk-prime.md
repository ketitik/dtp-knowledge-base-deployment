---
title: 'Apa itu probe pada Netmonk Prime?'
category: 'panduan-pengguna-baru'
is_sticky: true
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Probe merupakan perangkat lunak yang bertugas mengumpulkan data dari perangkat jaringan, website, atau API yang akan dimonitor oleh Netmonk Prime.'
---

# Apa itu probe pada Netmonk Prime?

*Probe* merupakan perangkat lunak yang bertugas mengumpulkan data dari perangkat jaringan, *website*, atau *API* yang akan dimonitor oleh Netmonk Prime. Terdapat dua jenis *probe*, yaitu *Network Probe* dan *Web/API Probe* yang masing-masing definisinya adalah sebagai berikut:

1. ***Network Probe*** merupakan perangkat lunak yang bertugas mengumpulkan data dari perangkat jaringan yang akan dimonitor oleh Netmonk Prime. Secara *default*, *network probe* diinstal bersama Netmonk Prime di *cloud*. Hal ini memungkinkan monitoring perangkat  jaringan yang memiliki alamat statis *IP Public*. Jika perangkat yang dimonitor berada di jaringan *private* atau tertutup, *network probe* perlu diinstal terpisah di server atau komputer yang berada di jaringan *private* tersebut dan dipastikan *network probe* dapat mengakses ke internet.
   
2. ***Web/API Probe*** merupakan perangkat lunak yang bertugas mengumpulkan data dari *website* atau API berbasis HTTP/HTTPS yang akan dimonitor oleh Netmonk Prime. Secara *default*, web/API probe diinstal bersama Netmonk Prime di *cloud*. Jika *website* atau *API* yang dimonitor berada di sebuah server atau komputer dan tidak di-publish ke internet,* web/API probe* perlu diinstal terpisah di server atau komputer tersebut dan dipastikan *web/API probe* dapat mengakses ke internet.
