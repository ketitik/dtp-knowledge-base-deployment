---
title: 'Mengapa perangkat yang sudah saya daftarkan di Network Device Inventory tidak muncul di dashboard?'
category: 'dashboard'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Pastikan Anda sudah melakukan sinkronisasi data'
---

# Mengapa perangkat yang sudah saya daftarkan di Network Device Inventory tidak muncul di dashboard?

Pastikan Anda sudah melakukan sinkronisasi data dengan menekan tombol **Synchronize** setelah Anda mendaftarkan perangkat baru, kemudian tunggu 5-15 menit hingga Netmonk Prime dapat memonitor perangkat Anda. Jika setelah 15 menit data perangkat masih tidak muncul di *dashboard*, periksa kembali data di *Network Device Inventory* dan pastikan data yang Anda inputkan sudah benar. Jika data perangkat tersebut sudah benar tetapi perangkat masih tidak muncul di *dashboard*, akses menu *Network Probe Inventory* dan pastikan status *network probe* yang Anda pilih untuk perangkat tersebut *online*. Untuk bantuan lebih lanjut, silakan hubungi kami melalui *Whatsapp* di nomor [berikut](http://wa.me/6281382676435).