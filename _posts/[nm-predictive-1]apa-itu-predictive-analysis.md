---
title: 'Apa itu Predictive Analysis?'
category: 'predictive-analysis'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Predictive Analysis atau halaman prediksi'
---

# Apa itu Predictive Analysis?

*Predictive Analysis* atau halaman prediksi menampilkan tren trafik semua *link/port/interface*, penggunaan *memory/RAM* dan *storage* atau media penyimpanan semua perangkat, serta informasi jumlah sisa hari sebelum *link/port/interface* atau perangkat tersebut mencapai ambang batas penggunaan. Prediksi dilakukan dengan memanfaatkan data historis yang tersimpan di dalam *database* sehingga pengguna dapat melakukan upaya-upaya pencegahan sebelum parameter-parameter pada *interface* dan perangkat yang termonitor oleh Netmonk Prime mencapai ambang batas dan menyebabkan koneksi melambat hingga menghambat kegiatan operasional di lingkungan pengguna.