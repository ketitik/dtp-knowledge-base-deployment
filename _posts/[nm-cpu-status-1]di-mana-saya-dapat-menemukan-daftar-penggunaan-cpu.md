---
title: 'Di mana saya dapat menemukan daftar penggunaan CPU dari semua perangkat yang termonitor Netmonk Prime?'
category: 'cpu-status'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Dari Dashboard Network Summary mode operasional'
---

# Di mana saya dapat menemukan daftar penggunaan CPU dari semua perangkat yang termonitor Netmonk Prime?

Dari **Dashboard Network Summary** mode operasional, klik **View All** pada bagian **CPU Status** atau akses melalui menu **Network Status → CPU Status**. Berikut contoh halaman utama *CPU Status* yang menampilkan daftar penggunaan CPU dari semua perangkat yang termonitor:

![Halaman CPU Status](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B4%5Dcpu-status/cpu-status-v2.png)

Keterangan:
a. Rata-rata penggunaan CPU di semua perangkat yang terdeteksi.
b. Jumlah CPU dengan penggunaan di bawah 50% .
c. Jumlah CPU dengan penggunaan di antara 50% s.d 80% .
d. Jumlah CPU dengan penggunaan di atas 80% .
e. *Hostname* yang terkonfigurasi di perangkat.
f. Nama perangkat yang terdaftar di *inventory*.
g. Indeks dari masing-masing CPU.
h. Persentase penggunaan masing-masing CPU.
i. Kategori status penggunaan CPU.
