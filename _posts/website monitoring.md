---
title: "Website Monitoring"
accordion: "website monitoring"
category: 'website-monitoring'
icon: website-monitoring
is_sticky: false
date: "2020-01-01"
excerpt: 'Apakah website monitoring hanya memonitor website saja?'
---

## GENERAL

[1] Apakah website monitoring hanya memonitor website saja?

[2] Data apa saja yang ditampilkan dihalaman setail Web Status?

[3] Bagaimana cara menduplikasi website atau API yang dimonitor Netmonk Basic?

[4] Bagaimana cara menghapus website atau API yang sudah saya daftarkan?

[5] Bagaimana cara melihat detail status dari suatu website atau API yang dimonitor?

## WEBSITE

[1] Bagaimana cara mendaftarkan website baru yang akan dimonitor?

[2] Bagaimana cara mengedit data website yang sudah saya daftarkan?

## API

[1] Bagaimana cara mendaftarkan API baru yang akan dimonitor?

[2] Bagaimana cara mengedit data API yang sudah saya daftarkan?

## PAUSED

[1] Bagaimana cara menghentikan monitoring suatu website atau API untuk sementara (Pause)?

[2] Bagaimana cara menjalankan kembali monitorng suatu website atau API yang sebelumnya dihentikan (Unpause)?

## REPORTING

[1] Bagaimana cara mengunduh laporan dari suatu website atau API yang dimonitor?

## WEB / API PROBE

[1] Bagaimana cara mendaftarkan web probe baru di Netmonk Basic?

[2] Bagaimana cara melihat detail web probe yang sudah saya daftarkan?

[3] Bagaimana cara mengedit data web probe yang sudah saya daftarkan?

[4] Bagaimana cara menghapus web probe yang sudah saya daftarkan?

## ALERTING