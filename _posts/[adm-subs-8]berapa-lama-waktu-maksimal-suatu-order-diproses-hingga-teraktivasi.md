---
title: 'Berapa lama waktu maksimal suatu order diproses hingga teraktivasi?'
category: 'subscription'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Waktu maksimal order diproses hingga teraktivasi'
---

# Berapa lama waktu maksimal suatu order diproses hingga teraktivasi?

Waktu maksimal order diproses hingga teraktivasi adalah 2 hari kerja untuk Netmonk Prime versi *Free Trial* dan 5 hari kerja untuk Netmonk Prime versi berbayar.