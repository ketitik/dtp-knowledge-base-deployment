---
title: 'Bagaimana cara mengetahui performa sistem atau server tempat Netmonk Prime diinstal?'
category: 'panduan-pengguna-baru'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Akses menu System Performance'
---

# Bagaimana cara mengetahui performa sistem atau server tempat Netmonk Prime diinstal?

Akses menu **System Performance** yang berada di paling bawah *sidebar*.

![Menu System Performance](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/getting-started/%5B2%5Dpanduan-pengguna/menu-system-performance.png)

Maka akan muncul halaman yang menyajikan informasi performa sistem atau *server* tempat Netmonk Prime diinstal seperti berikut:

a. Persentase penggunaan memori, baik *physical* maupun *virtual memory*, dari total kapasitas yang tersedia di sistem.

b. Persentase penggunaan setiap *storage* atau media penyimpanan dari total kapasitas yang tersedia di sistem.

c. Persentase penggunaan setiap CPU yang berjalan di sistem dan total rata-rata penggunaannya secara keseluruhan.

d. Total trafik data dari setiap *network* yang terdapat pada sistem.

e. Daftar semua proses atau *services* yang berjalan pada sistem dan statusnya.

Berdasarkan poin-poin di atas, berikut contoh halaman *System Performance*:

![Halaman System Performance](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/getting-started/%5B2%5Dpanduan-pengguna/system-performance.png)