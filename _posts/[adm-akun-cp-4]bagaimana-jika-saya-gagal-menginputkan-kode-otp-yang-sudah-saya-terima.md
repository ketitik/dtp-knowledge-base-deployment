---
title: 'Bagaimana jika saya gagal menginputkan kode OTP yang sudah saya terima?'
category: 'akun-customer-portal'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Periksa kembali kode OTP'
---

# Bagaimana jika saya gagal menginputkan kode OTP yang sudah saya terima?

Periksa kembali kode OTP yang Anda inputkan. Jika pada saat Anda menekan tombol **Verifikasi** muncul keterangan “Kode OTP yang Anda masukkan kadaluarsa”, klik teks **Kirim Ulang OTP**, maka sistem akan mengirimkan kode OTP baru melalui *WhatsApp* atau SMS. Inputkan kembali kode OTP tersebut melalui *form* berikut:

![Input ulang Kode OTP](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/akun-cp/input-kode-otp.png)

Selanjutnya akan muncul kotak dialog yang memberikan informasi bahwa order sudah berhasil dibuat dan Anda perlu menunggu hingga order teraktivasi.

![Order Berhasil Dibuat](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/akun-cp/order-berhasil-dibuat.png)