---
title: 'Di mana saya dapat menemukan daftar penggunaan Storage atau media penyimpanan dari semua perangkat yang termonitor Netmonk Prime?'
category: 'storage-status'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Dari Dashboard Network Summary mode operasional'
---

# Di mana saya dapat menemukan daftar penggunaan Storage atau media penyimpanan dari semua perangkat yang termonitor Netmonk Prime?

Dari **Dashboard Network Summary** mode operasional, klik **View All** pada bagian **Storage Status** atau akses melalui menu **Network Status → Storage Status**. Berikut contoh halaman utama *Storage Status* yang menampilkan daftar penggunaan *storage* dari semua perangkat yang termonitor:

![Storage Status](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B6%5Dstorage-status/storage-status-v2.png)

Keterangan:
a. Rata-rata penggunaan *storage* di semua perangkat yang terdeteksi.
b. Jumlah *storage* dengan penggunaan di bawah 50% .
c. Jumlah *storage* dengan penggunaan di antara 50% s.d 80% .
d. Jumlah *storage* dengan penggunaan di atas 80% .
e. *Hostname* yang terkonfigurasi di perangkat.
f. Nama perangkat yang terdaftar di *inventory*.
g. Indeks dari masing-masing *storage*.
h. Total kapasitas masing-masing *storage*.
i. Persentase penggunaan *storage*.
j. Kategori status penggunaan *storage*.