---
title: 'Apakah Netmonk Prime dapat digunakan untuk memonitor perangkat yang berada di jaringan private atau tertutup?'
category: 'panduan-pengguna-baru'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Jika perangkat yang akan dimonitor berada di jaringan private'
---

# Apakah Netmonk Prime dapat digunakan untuk memonitor perangkat yang berada di jaringan private atau tertutup?

Jika perangkat yang akan dimonitor berada di jaringan *private* atau tertutup, Anda perlu menyiapkan server atau komputer untuk kebutuhan instalasi *network probe* yang bertugas mengumpulkan data dari perangkat jaringan tersebut.