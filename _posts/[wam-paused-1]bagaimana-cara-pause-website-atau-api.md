---
title: 'Bagaimana cara menghentikan monitoring suatu website atau API untuk sementara (Pause)?'
category: 'paused'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Akses menu Web/API Monitoring → Web/API Status'
---

# Bagaimana cara menghentikan monitoring suatu website atau API untuk sementara (Pause)?

1. Akses menu **Web/API Monitoring → Web/API Status.**

    ![Menu Web Status](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/web-api-monitoring/%5B4%5Dpaused/menu-web-status.png)

2. Dari halaman utama *Web/API Status*, klik tombol **Pause Data** di salah satu baris data.

    ![Tombol Pause Data](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/web-api-monitoring/%5B4%5Dpaused/tombol-pause-data.png)

3. Tuliskan alasan mengapa Anda ingin menghentikan sementara proses *monitoring* pada *form* berikut:

    ![Form Pause Data](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/web-api-monitoring/%5B4%5Dpaused/form-pause-data.png)

4. Klik tombol **Pause** untuk melanjutkan proses pause.