---
title: 'Bagaimana cara melihat detail perangkat yang sudah saya daftarkan?'
category: 'network-device'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Akses menu Network Inventory → Network Device'
---

# Bagaimana cara melihat detail perangkat yang sudah saya daftarkan?

1. Akses menu **Network Inventory → Network Device**

    ![Menu Network Device](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B10%5Dnetwork-device/menu-network-device-v2.png)

2. Dari halaman utama *Network Device Inventory*, klik tombol **View** di salah satu baris data perangkat,

    ![Tombol View](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B10%5Dnetwork-device/tombol-view.png)

3. Selanjutnya akan tampil detail perangkat seperti berikut:
   
    ![View Detail Netowrk Device](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B10%5Dnetwork-device/view-network-device.png)

4. Klik tombol **Close** untuk menutup detail perangkat.