---
title: 'Jika saya ada kendala terkait payment, apa saja yang bisa saya tanyakan ke tim support netmonk secara langsung?'
category: 'payment'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Anda dapat menanyakan dan kami bisa *support* langsung antara lain:'
---

# jika saya ada kendala terkait payment, apa saja yang bisa saya tanyakan ke tim support netmonk secara langsung?

Anda dapat menanyakan dan kami bisa *support* langsung antara lain
- Meminta faktur pajak jika dibutuhkan perusahaan untuk melakukan rekon pajak jika faktur pajak tidak muncul pada menu yang sudah tersedia.
- Melakukan konfirmasi jika status pembayaran tidak berubah lebih dari 1x24 jam (hari kerja).
- Membutuhkan proses pembayaran yang terpisah jika pelanggan adalah* reseller* Netmonk Prime.
