---
title: 'Apa saja fitur Netmonk Prime?'
category: 'pengenalan-netmonk-prime'
is_sticky: true
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Saat ini, Netmonk Prime mempunyai 3 modul utama, yaitu Network Monitoring, Website/API Monitoring, dan Server Monitoring'
---

# Apa saja fitur Netmonk Prime?

Saat ini, Netmonk Prime mempunyai 3 modul utama, yaitu *Network Monitoring*, *Web/API Monitoring*, dan *Server Monitoring*.

## Network Monitoring
*Network Monitoring* merupakan modul Netmonk Prime yang memungkinkan Anda memonitoring perangkat jaringan *multi-vendor* dengan memanfaatkan protokol SNMP dan ICMP. Disajikan dalam bentuk *dashboard* analitik yang mudah dipahami sehingga tim operasional IT dapat melakukan pengelolaan dan pengauditan perangkat jaringan dengan lebih efektif. *Network Monitoring* terdiri dari beberapa fitur, di antaranya sebagai berikut:

1. **Dashboard Network Summary**, *dashboard* yang menampilkan kondisi terkini perangkat jaringan yang dimonitor, seperti status perangkat (*Up* atau *Down*) dan persentase penggunaan *resource* dari perangkat tersebut (*Link*, CPU, RAM, *Storage*).  

2. **Dashboard Network Map**, *dashboard* yang menampilkan gambaran persebaran perangkat-perangkat yang dimonitor, sehingga mempermudah pengguna dalam melihat perangkat mana yang sedang bermasalah dan perlu pengawasan.
   
3. **Device Status**, menampilkan informasi berupa *availability* atau tingkat ketersediaan perangkat melalui deteksi via ICMP maupun SNMP.
   
4. **Link Utilization**, menampilkan informasi trafik data dari *link/port/interface* suatu perangkat yang dimonitor.
   
5. **CPU, RAM, Storage, Status**, menampilkan informasi berupa status penggunaan dari CPU, RAM, maupun *Storage* suatu perangkat dari total kapasitas.
    
6. **Predictive Analysis**, menampilkan tren trafik *link/port/interface* dan tren penggunaan *resource* perangkat dengan memanfaatkan data historis yang tersimpan di dalam *database*.
   
7. **Reporting**, menyajikan rangkuman data-data terkait perangkat yang dimonitor selama periode waktu tertentu dalam bentuk laporan yang dapat diunduh menjadi file PDF.
   
8. **Alerting**, mengirimkan notifikasi atau *alert* kepada pengguna ketika penggunaan *link* dan *resource* perangkat mencapai ambang batas penggunaan, sehingga tim operasional IT tidak perlu berada di depan laya 24x7.


## Web/API Monitoring
*Web/API Monitoring* merupakan modul Netmonk Prime yang dapat digunakan untuk memonitoring *website* atau *API* berbasis HTTP/HTTPS. *Web/API Monitoring* terdiri dari beberapa fitur, yaitu sebagai berikut:

1. **Dashboard Website/API Status**, *dashboard* yang menampilan histori status, grafik *response time*, serta persentase nilai *downtime* dan *uptime* dari *website* atau *API* yang dimonitor.
   
2. **Reporting**, memungkinkan pengguna untuk mengunduh laporan terkait *response time* dan riwayat status dari *website/API* selama periode waktu tertentu dalam bentuk file PDF.
   
3. **Alerting**, mengirimkan notifikasi atau *alert* kepada pengguna ketika *website/API* terdeteksi down atau tidak tersedia dan ketika *response time* dari *website/API* tersebut melebihi ambang batas atau *threshold* yang sudah ditentukan.


## Linux Server Monitoring
*Linux Server Monitoring* merupakan modul Netmonk Prime yang memungkinkan Anda memonitoring *Linux Server* untuk memastikan *Linux Server* tersebut beroperasi sesuai ekspektasi sehingga dapat mendukung bisnis berjalan dengan optimal. *Linux Server Monitoring* terdiri dari beberapa fitur, yaitu sebagai berikut:

1. **Linux Dashboard**, *dashboard* yang menampilan jumlah *Linux Server* yang terdeteksi mati atau hidup (*Up/Down*) dari semua *Linux Server* yang dimonitor.
   
2. **Linux Server Status**, menampilkan informasi terkini dari *Linux Server* yang dimonitor, seperti penggunaan *CPU*, *Memory*, dan *Network* pada *Linux Server*, rata-rata operasi *input/output* dari suatu *disk*, serta daftar *process* yang sedang berjalan.
   
3. **Reporting**, memungkinkan pengguna untuk mengunduh laporan *uptime/downtime* dan penggunaan sumber daya *Linux Server* yang dimonitor selama periode waktu tertentu dalam bentuk file PDF.
   
4. **Alerting**, mengirimkan notifikasi atau *alert* kepada pengguna ketika *Linux Server* terdeteksi *down* atau ketika penggunaan *CPU* dan *Memory* melebihi batas normal.
