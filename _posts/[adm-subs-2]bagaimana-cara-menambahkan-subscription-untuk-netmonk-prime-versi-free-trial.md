---
title: 'Bagaimana cara menambahkan subscription untuk Netmonk Prime versi Free Trial?'
category: 'subscription'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Masuk ke halaman subscription'
---

# Bagaimana cara menambahkan subscription untuk Netmonk Prime versi Free Trial?

1. Masuk ke halaman *subscription* melalui menu **Subscription** seperti berikut:

    ![Menu Subscription](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/subscription/menu-subscription.png)

2. Klik tombol **Tambah Subscription**

    ![Tombol Tambah Subscription](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/subscription/tombol-tambah-subscription.png)

3. Pilih **Coba dengan versi Free Trial**

    ![Tambah Subscription](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/subscription/tambah-subscription.png)

4. Maka akan tampil *form* untuk mengisi data perusahaan. Isi *form* tersebut, kemudian klik tombol **Lanjutkan**

    ![Form Request Free Trial](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/subscription/request-free-trial.png)

5. Untuk menjaga keamanan akun, lakukan verifikasi dengan menginputkan nomor *handphone* Anda ke *form* berikut, kemudian klik tombol **Kirim OTP**

    ![Form OTP](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/subscription/form-otp.png)

6. Selanjutnya, sistem akan mengirimkan pesan yang berisi kode OTP seperti contoh berikut:

    ![Contoh Pesan OTP](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/subscription/pesan-otp.jpg)

7. Inputkan kode OTP yang Anda terima, kemudian klik tombol **Verifikasi**

    ![Input Kode OTP](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/subscription/input-kode-otp.png)

8. Jika kode OTP yang Anda inputkan salah, maka akan muncul pesan kesalahan seperti berikut:

    ![Invalid Kode OTP](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/subscription/invalid-kode-otp.png)

9.  Sedangkan jika Anda tidak menerima kode OTP atau kode OTP yang Anda inputkan sudah expired seperti contoh berikut, klik pada teks Kirim Ulang OTP untuk mendapatkan kode OTP yang baru

    ![Expired OTP](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/subscription/expired-otp.png)

10. Setelah berhasil melakukan verifikasi nomor handphone, berikutnya akan muncul kotak dialog yang memberikan informasi bahwa order sudah berhasil dibuat dan Anda perlu menunggu hingga order teraktivasi. Tutup kotak dialog tersebut dengan menekan tombol **Baik, saya mengerti**

    ![Order Berhasil Dibuat](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/subscription/order-berhasil-dibuat.png)

11. Order yang berhasil dibuat beserta statusnya dapat Anda lihat di halaman utama menu **Subscription** pada kolom **Status Order Terakhir** seperti berikut:

    ![Status Order Masuk Antrian](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/subscription/free-trial-masuk-antrian.png)

12. Dari gambar di atas, klik tombol **Detail**, maka Anda akan melihat rincian status order di bagian **Rincian Order Subscription** seperti berikut:

    ![Rincian Status Order](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/subscription/rincian-order-free-trial.png)

13. Jika order sudah teraktivasi, Anda akan mendapatkan alamat Netmonk Prime beserta *username* dan *password* yang dapat Anda gunakan seperti pada tampilan berikut:

    ![Detail Subscription](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/subscription/detail-subscription-free-trial.png)

14. Jika kembali ke halaman utama menu *subscription*, status order akan berubah menjadi **Order Teraktivasi** dan status *subscription* berubah menjadi **Active**

    ![Status Order Teraktivasi](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/subscription/free-trial-teraktivasi.png)