---
title: 'Bagaimana cara melihat detail penggunaan disk tertentu?'
category: 'diskio-status'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Akses menu Network Monitoring → DiskIO Status'
---

# Bagaimana cara melihat detail penggunaan disk tertentu?

1. Akses menu **Network Status → DiskIO Status**

    ![Menu DiskIO Status](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B7%5Ddiskio-status/menu-diskio-status-v2.png)

2. Dari halaman utama menu *DiskIO Status*, klik tombol **View** di salah satu baris perangkat.

    ![Tombol View Detail](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B7%5Ddiskio-status/tombol-view-detail.png)

3. Maka akan muncul tampilan seperti berikut:
   
   ![Detail DiskIO Status](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B7%5Ddiskio-status/detail-diskio-status-v2.png)

   Keterangan:
   a. Informasi dasar seperti nama perangkat, alamat IP, dan nama *disk*.
   b. Grafik *history* rata-rata operasi *input/output disk* pada periode waktu tertentu.
   c. Kategori status rata-rata operasi *input/output disk* yang mewakili rata-rata beban *disk*.
   d. Grafik *history* jumlah data yang ditulis ke dalam *disk* dan jumlah data yang dibaca oleh *disk* pada periode waktu tertentu.
