---
title: 'Bagaimana jika subscription saya expired?'
category: 'subscription'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Jika subscription Anda sudah expired'
---

# Bagaimana jika subscription saya expired?

Jika *subscription* Anda sudah *expired*, Anda harus melakukan perpanjangan terlebih dahulu untuk dapat mengakses kembali halaman Netmonk Prime. Pastikan perpanjangan dilakukan maksimal 14 hari setelah tanggal *expired*. Jika melebihi waktu tersebut, Anda harus menambahkan *subscription* baru untuk berlangganan Netmonk Prime karena data-data Anda pada *subscription* sebelumnya sudah terhapus oleh sistem. 