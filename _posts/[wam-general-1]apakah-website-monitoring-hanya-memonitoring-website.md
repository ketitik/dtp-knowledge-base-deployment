---
title: 'Apakah website monitoring hanya memonitor website saja?'
category: 'general'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Selain memonitor website, fitur Website Monitoring dapat digunakan untuk memonitor API'
---

# Apakah website monitoring hanya memonitor website saja?

Tidak. Selain memonitor *website*, fitur *Website Monitoring* dapat digunakan untuk memonitor *API* dan *service-service* yang berbasis HTTP/HTTPS.