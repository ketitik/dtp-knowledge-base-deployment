---
title: 'Bagaimana jika saya ingin melihat dashboard dengan mode managerial?'
category: 'dashboard'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Akses menu Dashboard → Network Summary'
---

# Bagaimana jika saya ingin melihat dashboard dengan mode managerial?

1. Akses menu **Dashboard → Network Summary**

    ![Menu Dashboard Network Summary](
    https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B1%5Ddashboard/menu-dashboard-network-summary-v2.png)

2. Dari halaman utama *Dashboard Network Summary*, klik tombol berikut:

    ![Tombol Management Mode](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B1%5Ddashboard/tombol-management-mode.png)

3. Maka akan tampil *dashboard* seperti berikut:
   
   ![Dashboard Management Mode](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B1%5Ddashboard/dashboard-management-mode-v2.png)

   Keterangan:
   a. Grafik jumlah perangkat yang mati (diwakili warna merah) atau hidup (diwakili warna hijau) yang dikelompokkan berdasarkan lokasi.
   b. Jumlah perangkat yang mati (*down*) dari keseluruhan perangkat yang dimonitor.
   c. Jumlah *link/port/interface* sesuai dengan kategori penggunaannya.
   d. *Uptime Availability* (SLA) perangkat yang dimonitor sesuai kategorinya pada hari sebelumnya.e. Grafik trafik data yang dikelompokkan berdasarkan lokasi.
   f. Grafik *history* penggunaan *link/port/interface* yang dikategorikan sebagai *starred link*.

4. Untuk menampilkan *dashboard* memenuhi layar atau *full screen*, tekan tombol **F** pada *keyboard* atau klik tombol berikut:

    ![Tombol Full Screen Mode](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B1%5Ddashboard/tombol-full-screen-mode.png)

5. Sedangkan untuk beralih ke mode *dashboard* operasional, tekan tombol **Esc** pada *keyboard* atau klik tombol berikut:

    ![Beralih ke Mode Dashboard Operasional](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B1%5Ddashboard/nonactivate-management-mode-v2.png)
    