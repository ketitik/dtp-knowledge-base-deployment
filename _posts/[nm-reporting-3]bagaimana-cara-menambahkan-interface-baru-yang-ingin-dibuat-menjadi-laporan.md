---
title: 'Bagaimana cara menambahkan link/port/interface baru yang ingin dibuat menjadi laporan?'
category: 'reporting'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Akses menu Network Reporting → Interface Reporting'
---

# Bagaimana cara menambahkan link/port/interface baru yang ingin dibuat menjadi laporan?

1. Akses menu **Network Reporting → Interface Reporting**.

    ![Menu Interface Reporting](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B13%5Dreporting/menu-interface-reporting-v2.png)

2. Dari halaman utama menu *Interface Reporting*, klik tombol **Add Interface**

    ![Tombol Add Interface](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B13%5Dreporting/tombol-tambah-interface-reporting.png)

3. Selanjutnya akan muncul *form* untuk mengisi detail dari *interface* baru yang akan dibuat laporannya berikut:

    ![Form Add Interface](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B13%5Dreporting/form-tambah-interface-reporting.png)

4. Isi data pada *form* tersebut dengan rincian sebagai berikut:

    | **Jenis Isian** | **Keterangan** |
    | ------ | ------|
    | Hostname | Perangkat yang dimonitor.|
    | Interface | Interface dari perangkat yang sudah dipilih dan akan dibuat laporannya.|
    | Title | Judul laporan.|
    |Prepared for | Pihak yang akan menerima laporan. |

5. Klik tombol **Add** untuk menambahkan *interface* baru.