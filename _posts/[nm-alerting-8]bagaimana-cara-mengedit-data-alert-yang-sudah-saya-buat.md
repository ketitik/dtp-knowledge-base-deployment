---
title: 'Bagaimana cara mengedit data alert yang sudah saya buat?'
category: 'alerting'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Akses menu Settings → Alert'
---

# Bagaimana cara mengedit data alert yang sudah saya buat?

1. Akses menu **Settings → Alert**.

    ![Menu Alert](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B14%5Dalerting/menu-alert.png)

2. Dari halaman utama menu *Alert Setting* yaitu *tab* **Alert Notification**, klik tombol **Edit** di salah satu baris data,

    ![Tombol Edit](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B14%5Dalerting/tombol-edit-alert.png)

3. Selanjutnya akan tampil *form* untuk mengedit data seperti berikut:

    ![Form Edit Alert](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B14%5Dalerting/form-edit-alert.png)

4. Edit data sesuai kebutuhan, kemudian klik tombol **Save** untuk menyimpan perubahan pada data.