---
title: 'Bagaimana cara melihat detail penggunaan CPU tertentu?'
category: 'cpu-status'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Akses menu Network Monitoring → CPU Status'
---

# Bagaimana cara melihat detail penggunaan CPU tertentu?

1. Akses menu **Network Status → CPU Status**

    ![Menu CPU Status](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B4%5Dcpu-status/menu-cpu-status-v2.png)

2. Dari halaman utama menu *CPU Status*, klik tombol **View** di salah satu baris perangkat.

    ![Tombol View Detail](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B4%5Dcpu-status/tombol-view-detail.png)

3. Maka akan muncul tampilan seperti berikut:
   
    ![Detail CPU Status](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B4%5Dcpu-status/detail-cpu-status-v2.png)

   Keterangan:
   a. Informasi dasar seperti nama perangkat, alamat IP, dan indeks CPU.
   b. Grafik *history* penggunaan CPU pada periode waktu tertentu.
   c. Kategori status penggunaan dan persentase penggunaan terakhir CPU.
