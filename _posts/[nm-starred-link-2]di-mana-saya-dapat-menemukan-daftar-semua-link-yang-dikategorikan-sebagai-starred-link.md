---
title: 'Di mana saya dapat menemukan daftar semua link/port/interface yang dikategorikan sebagai starred link?'
category: 'starred-link'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Dari Dashboard Network Summary mode operasional'
---

# Di mana saya dapat menemukan daftar semua link/port/interface yang dikategorikan sebagai starred link?

Dari **Dashboard Network Summary** mode operasional, klik **View All** pada bagian **Starred Link Status** atau akses melalui menu **Network Status → Starred Link**. Berikut contoh halaman utama *Starred Link* yang menampilkan daftar seluruh *link/port/interface* yang dikategorikan sebagai *starred link*:

![Menu Starred Link](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B3%5Dstarred-link/starred-link-v2.png)

Keterangan:
a. Total trafik data yang keluar dari semua *starred link*.
b. Total trafik data yang masuk ke semua *starred link*.
c. *Hostname* yang terkonfigurasi di perangkat.
d. Nama perangkat yang terdaftar di *inventory*.
e. Alamat IP dari perangkat yang dimonitor.
f. Nama masing-masing *starred link*.
g. Jumlah trafik data yang masuk dan keluar untuk masing-masing *starred link*.

