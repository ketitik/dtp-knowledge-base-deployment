---
title: 'Bagaimana cara melihat detail status dari perangkat yang dimonitor Netmonk Prime?'
category: 'network-device'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Akses menu Network Status → Device Status'
---

# Bagaimana cara melihat detail status dari perangkat yang dimonitor Netmonk Prime?

1. Akses menu **Network Status → Device Status**

    ![Menu Device Status](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B10%5Dnetwork-device/menu-network-device-v2.png)

2. Dari halaman utama menu *Device Status*, klik tombol **View** di salah satu baris perangkat.

    ![Tombol View Detail](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B10%5Dnetwork-device/tombol-view-detail.png)

3. Maka akan muncul tampilan seperti berikut:
   
   ![Detail Device Status](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B10%5Dnetwork-device/detail-device-status-v2.png)

   Keterangan:
   a. Informasi dasar seperti nama perangkat dan alamat IP perangkat.
   b. Grafik *history* status perangkat dari deteksi via ICMP pada periode waktu tertentu.c. *Ratio availability* (tingkat ketersediaan) perangkat dari deteksi via ICMP pada periode waktu tertentu.
   d. Grafik *history* status perangkat  dari deteksi via SNMP *Uptime* pada periode waktu tertentu.
   e. *Ratio availability* (tingkat ketersediaan) perangkat dari deteksi via SNMP *Uptime* pada periode waktu tertentu.
   f. Detail konfigurasi dari perangkat.
   g. Daftar *link/port/interface* dan statusnya, serta konfigurasinya seperti alamat IP, alamat Mac, dan jumlah maksimum *bandwidth*.
   h. Daftar indeks CPU dan persentase penggunaannya.
   i. Daftar indeks memori dan persentase penggunaannya.
   j. Daftar indeks media penyimpanan dan persentase penggunaannya.
   k. Daftar *DiskIO*  beserta total dan rata-rata penggunaannya.