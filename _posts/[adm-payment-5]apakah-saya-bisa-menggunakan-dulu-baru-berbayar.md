---
title: 'Apakah saya bisa menggunakan dulu baru berbayar?'
category: 'payment'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Tidak bisa, karena sistem pembayaran yang dipakai oleh netmonk adalah sistem pre-paid.'
---

# Apakah saya bisa menggunakan dulu baru berbayar?

Tidak bisa, karena sistem pembayaran yang dipakai oleh netmonk adalah sistem *pre-paid*, bukan *post-paid*. Jika ingin melakukan *trial* terbatas, dapat memanfaatkan program *free trial* kami pada [*link*](https://netmonk.id/free-trial) berikut: