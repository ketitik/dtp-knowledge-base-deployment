---
title: 'Bagaimana cara mengubah periode waktu untuk menampilkan data Network Monitoring sesuai dengan kebutuhan saya?'
category: 'network-device'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Akses sub menu Network Status sesuai kebutuhan Anda'
---

# Bagaimana cara mengubah periode waktu untuk menampilkan data Network Monitoring sesuai dengan kebutuhan saya?

Akses sub menu **Network Status** sesuai kebutuhan Anda, klik tombol **View** di salah satu baris data sehingga sistem beralih ke halaman detail. Dari halaman detail tersebut, klik pada kolom **Last 1 Hour** yang berada di kanan atas seperti berikut, maka akan muncul pilihan periode (*Quick Range*) atau rentang waktu (*Custom Range*) untuk menampilkan data sesuai dengan kebutuhan.

![Custom Period](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B10%5Dnetwork-device/custom-period-v2.png)