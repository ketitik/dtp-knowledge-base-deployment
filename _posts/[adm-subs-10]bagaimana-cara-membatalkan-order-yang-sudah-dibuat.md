---
title: 'Bagaimana cara membatalkan order yang sudah dibuat?'
category: 'subscription'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Masuk ke halaman subscription'
---

# Bagaimana cara membatalkan order yang sudah dibuat?

1. Masuk ke halaman *subscription* melalui menu **Subscription** seperti berikut:

    ![Menu Subscription](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/subscription/menu-subscription.png)

2. Klik tombol **Detail** pada data order yang belum dilakukan pembayaran seperti contoh berikut:
   
    ![Status Order Butuh Pembayaran](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/subscription/paket-berbayar-butuh-pembayaran.png)

3.  Setelah masuk halaman **Detail Subscription**, klik pada tulisan **Klik untuk lihat cara pembayaran** seperti di bawah ini:

    ![Lihat Cara Pembayaran](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/subscription/lihat-cara-pembayaran.png)

4.  Selanjutnya, akan tampil **Detail Pembayaran Order** dari order yang bersangkutan seperti berikut:
   
    ![Detail Pembayaran Order](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/subscription/detail-order.png)

5.  Klik tombol **Batalkan Order** untuk membatalkan order yang sudah dibuat. Order juga akan terbatalkan secara otomatis oleh sistem jika pembayaran tidak dilakukan dalam kurun waktu 2 x 24 jam terhitung sejak order tersebut dibuat.