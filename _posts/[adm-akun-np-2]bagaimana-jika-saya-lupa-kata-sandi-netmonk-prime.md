---
title: 'Bagaimana jika saya lupa kata sandi Netmonk Prime?'
category: 'akun-netmonk-prime'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Kata sandi Netmonk Prime'
---

# Bagaimana jika saya lupa kata sandi Netmonk Prime?

Kata sandi Netmonk Prime dapat Anda lihat di [Customer Portal](https://netmonk.id/portal/customer/login) di halaman **Detail Subscription**

![detail-subscription](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/akun-np/detail-subscription.png)

Jika Anda sudah pernah mengubahnya, silakan hubungi kami melalui *WhatsApp* melalui nomor [berikut](http://wa.me/6281382676435) untuk bantuan
lebih lanjut.
