---
title: 'Bagaimana cara mendaftarkan API baru yang akan dimonitor?'
category: 'api'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Akses modul Web/API Monitoring → Web/API Status'
---

# Bagaimana cara mendaftarkan API baru yang akan dimonitor?

1. Akses modul **Web/API Monitoring → Web/API Status**.

    ![Menu Web Status](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/web-api-monitoring/%5B3%5Dapi/menu-web-status-v2.png)

2. Dari halaman utama *Web Status*, klik tombol **Add Web/API**,

    ![Tombol Tambah API](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/web-api-monitoring/%5B3%5Dapi/tombol-tambah-web.png)

3. Selanjutnya akan muncul *form* untuk mengisi detail *API* yang akan dimonitor.

    ![Form Tambah API](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/web-api-monitoring/%5B3%5Dapi/form-tambah-web.png)

4. Isi data pada *form* tersebut dengan rincian sebagai berikut:

    | **Jenis Isian** | **Keterangan** |
    | ----------- | ----------- |
    | Name  | Nama dari *API* yang akan dimonitor.| 
    | Interval check | Rentang waktu diperiksanya status *API* secara berkala. |
    | Probe | Pilihan *probe* yang akan bertugas mengambil data dari *API* yang dimonitor. Jika ingin menambahkan *probe* baru, klik **Add New Probe**. |
    | URL/IP | Alamat URL atau alamat IP dari *API* yang akan dimonitor. Mendukung 2 protokol, yaitu HTTP dan HTTPS. |
    | Tags | Kata kunci yang ditambahkan untuk mendeskripsikan *API* yang akan dimonitor. (Opsional) |
    | Alert Threshold | Ambang batas *response time* yang ditentukan untuk *API* yang akan dimonitor (diisi dalam satuan *milisecond*) |

5. Selanjutnya, tambahkan *request* dan kondisi *response* yang diharapkan (opsional) dengan rincian sebagai berikut:

    <table>
    <tr>
        <th colspan="2">Jenis Isian</th>
        <th>Keterangan</th>
    </tr>
    <tr>
        <td rowspan="5">Request</td>
        <td>Method</td>
        <td>Request <i>method</i> yang menunjukkan tindakan yang diinginkan untuk dilakukan terhadap <i>API</i> yang akan dimonitor.</td>
    </tr>
    <tr>
        <td>Request headers</td>
        <td>Request <i>header</i> yang dapat digunakan dalam HTTP <i>request</i> untuk memberikan informasi tentang konteks permintaan.</td>
    </tr>
    <tr>
        <td>Body</td>
        <td>Data opsional yang terkait dengan <i>request</i>.</td>
    </tr>
    <tr>
        <td>Username</td>
        <td><i>Username</i> yang digunakan untuk mengakses <i>API</i> jika <i>API</i> tersebut memerlukan autentikasi.</td>
    </tr>
    <tr>
        <td>Password</td>
        <td><i>Password</i> yang digunakan untuk mengakses <i>API</i> jika <i>API</i> tersebut memerlukan autentikasi.</td>
    </tr>
    <tr>
        <td rowspan="3">Response Codition</td>
        <td>Status Code</td>
        <td>Kode status HTTP yang diharapkan keluar sebagai <i>response</i> dari <i>API</i> yang dimonitor.</td>
    </tr>
    <tr>
        <td>Match Header</td>
        <td><i>Response header</i> yang diharapkan keluar sebagai <i>response</i> dari <i>API</i> yang dimonitor.</td>
    </tr>
    <tr>
        <td>Match Body</td>
        <td>Data di dalam <i>response body</i> yang diharapkan keluar sebagai <i>response</i> dari <i>API</i> yang dimonitor.</td>
    </tr>
    <tr>
        <td colspan="3">Keterangan:
        <br>
        Jika <i>Status Code, Match Header,</i> atau <i>Match Body</i> yang diinputkan tidak ada di dalam <i>response</i>, maka status <i>API</i> tersebut dianggap <i>down</i> karena tidak sesuai dengan kondisi yang diharapkan. </td>
    </tr>
    </table>

6. Klik tombol **Save** untuk menambahkan *API* baru.


## Contoh Menambahkan API Baru

### Basic Authentication

Sebagai contoh skenario, Anda ingin memonitor sebuah *API* yang menggunakan *Basic Authentication* setiap 1 menit sekali. Anda ingin memastikan bahwa *API* tersebut berjalan dengan lancar dalam arti memberikan kode respon 200. Untuk kebutuhan *Authentication*, Anda perlu menginputkan *username* dan *password*. Anda pun dapat menambahkan *Tags* untuk mendeskripsikan *API* yang ingin Anda monitor tersebut. Maka contoh isi *form* **Add Web/API** pada skenario ini adalah sebagai berikut:

![Contoh Tambah API](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/web-api-monitoring/%5B3%5Dapi/contoh-tambah-web-basic1.png) ![Contoh Tambah API](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/web-api-monitoring/%5B3%5Dapi/contoh-tambah-web-basic2.png)
![Contoh Tambah API](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/web-api-monitoring/%5B3%5Dapi/contoh-tambah-web-basic3.png)


### Bearer Token Authentication

Sebagai contoh skenario, Anda ingin memonitor sebuah *API* yang menggunakan *Bearer Token Authentication* setiap 1 menit sekali. Anda ingin memastikan bahwa I tersebut berjalan dengan lancar dalam arti memberikan kode respon 200. Untuk kebutuhan I, Anda perlu menginputkan *Bearer Token*. Anda pun dapat menambahkan kondisi dari *Response Header* yang diharapkan keluar sebagai *response* dari *API* yang ingin Anda monitor tersebut pada *kolom Match Header*. Maka contoh isi *form* **Add Web/API** pada skenario ini adalah sebagai berikut:

![Contoh Tambah API](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/web-api-monitoring/%5B3%5Dapi/contoh-tambah-web-bearer1.png) ![Contoh Tambah API](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/web-api-monitoring/%5B3%5Dapi/contoh-tambah-web-bearer2.png)
![Contoh Tambah API](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/web-api-monitoring/%5B3%5Dapi/contoh-tambah-web-bearer3.png)
