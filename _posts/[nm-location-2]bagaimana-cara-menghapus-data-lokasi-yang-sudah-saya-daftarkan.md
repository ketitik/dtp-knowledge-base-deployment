---
title: 'Bagaimana cara menghapus data lokasi yang sudah saya daftarkan?'
category: 'location'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Akses menu Settings → Locations'
---

# Bagaimana cara menghapus data lokasi yang sudah saya daftarkan?

1. Akses menu **Settings → Locations**

    ![settings-locations](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B9%5Dlocation/settings-locations.png)

2. Dari halaman utama menu *Locations Settings* atau manajemen lokasi, klik tombol **Delete** di salah satu baris data lokasi.

    ![delete-button-generic](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B9%5Dlocation/delete-location-button.png)

3. Selanjutnya akan tampil kotak dialog seperti berikut:

    ![delete-prompt-generic](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B9%5Dlocation/hapus-data.png)

4. Klik tombol **Delete** untuk melanjutkan proses hapus.