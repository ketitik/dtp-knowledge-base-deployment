---
title: 'Apa itu Customer Portal?'
category: 'panduan-pengguna-baru'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Customer Portal merupakan aplikasi DTP'
---

# Apa itu Customer Portal?

Customer Portal merupakan aplikasi *DTP (Digital Touch Point)* yang digunakan di sisi *customer* atau pelanggan Netmonk. Melalui aplikasi ini, pelanggan dapat membuat order untuk mencoba Netmonk Prime versi *Free Trial*, berlanggan Netmonk Prime versi berbayar, meng-*upgrade* Netmonk Prime *Free Trial* ke berbayar, atau memperpanjang masa aktif Netmonk Prime versi berbayar. Sebelum dapat mengakses aplikasi Customer Portal, pelanggan perlu membuat akun terlebih dahulu melalui *website* Netmonk di menu [berikut](https://netmonk.id/free-trial).
