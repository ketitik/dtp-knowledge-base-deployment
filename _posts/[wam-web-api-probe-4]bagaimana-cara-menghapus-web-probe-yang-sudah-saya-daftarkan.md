---
title: 'Bagaimana cara menghapus web/API probe yang sudah saya daftarkan?'
category: 'web-api-probe'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Akses menu Inventory → Web/API Probe'
---

# Bagaimana cara menghapus web/API probe yang sudah saya daftarkan?

1. Akses menu **Inventory → Web/API Probe**.

    ![Menu Web Probe](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/web-api-monitoring/%5B6%5Dweb-api-probe/menu-web-probe.png)

2. Dari halaman utama *Web/API Probe Inventory*, klik tombol **Delete** di salah satu baris data *web/API probe*,

    ![Tombol Delete Web Probe](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/web-api-monitoring/%5B6%5Dweb-api-probe/tombol-delete.png)

3. Selanjutnya akan tampil kotak dialog seperti berikut:

    ![Hapus Data](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/web-api-monitoring/%5B6%5Dweb-api-probe/hapus-data.png)

4. Klik tombol **Delete** untuk melanjutkan proses hapus.