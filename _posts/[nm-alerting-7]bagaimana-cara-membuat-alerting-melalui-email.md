---
title: 'Bagaimana cara membuat alert melalui email?'
category: 'alerting'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Akses menu Settings → Alert'
---

# Bagaimana cara membuat alerting melalui email?

1. Akses menu **Settings → Alert**.

    ![Menu Alert](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B14%5Dalerting/menu-alert.png)

2. Dari halaman utama menu *Alert Setting* yaitu *tab* **Alert Notification**, klik tombol **Add New Notification**,

    ![Add New Notification](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B14%5Dalerting/tombol-tambah-notifikasi.png)

3. Selanjutnya akan tampil *form* berikut:

    ![Form Add Notification](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B14%5Dalerting/form-tambah-notifikasi-email.png)

4. Isi *form* tersebut dengan rincian sebagai berikut, kemudian klik tombol **Add**.

    | **Jenis Isian** | **Keterangan** |
    | ----- | ----- |
    | Name | Nama profil *alerting*. Pengisian nama diharapkan tidak menggunakan spasi dan memanfaatkan dash (“-“) sebagai pemisah kata. |
    | Notification Type | Tipe *alerting*, diisi dengan pilihan “Email” |
    | SMTP Host | Nama host SMTP sebagai server email keluar. |
    | SMTP Port | Nomor port yang digunakan untuk mengirimkan email. |
    | SMTP Username | Username atau nama pengguna untuk mengakses SMTP. |
    | SMTP Password | Password atau kata sandi untuk mengakses SMTP. |
    | SMTP From | Alamat email pengirim *alert*. |
    | Sent To | Alamat email penerima *alert*. Tambahkan spasi untuk memasukan alamat email. |

5. Selanjutnya,  beralih ke *tab* **Alert Rules**, daftarkan profil *alerting* yang sudah dibuat ke *alert rules*, misal *Network Memory Status*.

    ![Alert Rules Memory Status](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B14%5Dalerting/alert-rule-memory-status.png)

6. Klik tombol **Edit** di baris *Network Memory Status*, maka akan muncul sebuah *form* untuk mengatur *rule memory status* seperti berikut:

    ![Form Alert Rules](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B14%5Dalerting/form-alert-rule-memory-status.png)

    >**Keterangan:**
    >*Form* di atas berlaku juga untuk mengatur *rule* pada *Network Link Status*, *Network CPU Status*, *Network Storage Status*, dan *Website Response Time Status*.

7. Isi *form* tersebut dengan rincian sebagai berikut:

    | **Jenis Isian** | **Keterangan** |
    | ----- | ----- |
    | Threshold | Ambang batas penggunaan memori. Diisi dalam satuan desimal (0.8 = 80 %) |
    | Baseline | Nilai yang digunakan sebagai *trigger* atau pemicu untuk mengirimkan *alert* kepada pengguna. Secara *default*, nilai *baseline* adalah -1. |
    | Checking Period | Lamanya waktu toleransi pengecekan penggunaan memori hingga dianggap mengalami melewati ambang batas sehingga sistem dapat mengirimkan *alert*. Diisi dalam satuan detik (s), menit (m), atau jam (h) |
    | Reminder Period | Periode waktu dikirimkannya *alert* secara berkala. Diisi dalam satuan detik (s), menit (m), atau jam (h) |
    | Alert Notification Profiles | Nama profil *alerting* yang sudah dibuat di bagian **Alert Notification**, bisa diisi lebih dari satu.|

8. Klik tombol **Save** untuk menyimpan perubahan atau tombol **Cancel** untuk membatalkan.
   
9. Terakhir, aktifkan *rule* tersebut dengan menekan tombol *switch* di baris *Network Memory Status* bagian kiri sehingga dari warna abu-abu berubah menjadi aktif berwarna hijau seperti contoh berikut: 
    
    ![Aktifkan Rule](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B14%5Dalerting/activate-alert-memory-status.png)