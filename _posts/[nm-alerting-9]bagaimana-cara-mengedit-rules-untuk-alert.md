---
title: 'Bagaimana cara mengedit rules atau aturan-aturan yang ditetapkan untuk suatu alert?'
category: 'alerting'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Akses menu Settings → Alert'
---

# Bagaimana cara mengedit rules atau aturan-aturan yang ditetapkan untuk suatu alert?

1. Akses menu **Settings → Alert**.

    ![Menu Alert](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B14%5Dalerting/menu-alert.png)

2. Dari halaman utama menu *Alert Setting* yaitu *tab* **Alert Notification**, pindah ke *tab* **Alert Rules**, kemudian klik tombol **Edit** di salah satu baris data seperti berikut:

    ![Tombol Edit Alert Rules](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B14%5Dalerting/tombol-edit-alert-rules.png)

3. Selanjutnya akan tampil *form* untuk mengedit data seperti berikut:

    ![Form Edit Alert Rule](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B14%5Dalerting/form-edit-alert-rule.png)

4. Edit data sesuai kebutuhan, kemudian klik tombol **Save** untuk menyimpan perubahan pada data.
   
5. Sedangkan untuk mengaktifkan atau menonaktifkan *alerting* untuk parameter tertentu, klik tombol *switch* berikut:
   
   ![Mengaktifkan atau Menonaktifkan Parameter](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B14%5Dalerting/list-alert-rule.png)
