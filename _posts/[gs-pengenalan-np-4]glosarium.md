---
title: 'Netmonk Prime Glosarium'
category: 'pengenalan-netmonk-prime'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Glosarium ini mendefinisikan istilah umum'
---

# Netmonk Prime Glosarium

Glosarium ini mendefinisikan istilah umum yang mungkin Anda lihat saat menggunakan Netmonk Prime maupun Customer Portal.

## A
### Active Subscription
Daftar *subscription* di mana order terbaru yang Anda buat sebelumnya sudah teraktivasi. Selama status *subscription* tersebut *Active*, Anda dapat menggunakan Netmonk Prime. Informasi alamat Netmonk Prime yang dapat Anda akses, *username*, dan *password* dapat Anda lihat di halaman *Detail Subscription* atau melalui email aktivasi yang Anda terima.

### Admin
Salah satu jenis *role* yang terdapat pada Netmonk Prime. Pengguna dengan *role* Admin memiliki hak akses ke sebagian besar fitur. Admin diizinkan untuk mengakses halaman *Users* (Manajemen Pengguna), tetapi tidak diizinkan untuk menambah, mengubah, atau menghapus data pengguna tersebut.

### Agent
Perangkat lunak yang bertugas mengumpulkan metrik data dari server yang dimonitor, dan mengirimkannya ke *Message Broker* untuk kebutuhan *Linux Server Monitoring*.

### Agent Time Interval
Interval waktu pengambilan data pada *agent* yang diinstal di server yang dimonitor.

### Alert Notification
Bagian dari menu *Alert Settings* yang menampilkan daftar jalur notifikasi yang sudah didaftarkan pengguna dan dapat digunakan untuk mengirimkan *alert*.

### Alert Rules
Bagian dari menu *Alert Settings* yang menampilkan daftar parameter yang dapat dikirim melalui *alert* atau notifikasi. Pengguna dapat mengaktifkan atau menonaktifkan notifikasi untuk suatu parameter dari jalur notifikasi tertentu, serta mengatur aturan yang akan ditetapkan untuk notifikasi tersebut, seperti nilai *Threshold*, *Checking Period*, dan *Reminder Period*.

### Authentication Protocol
Jenis protokol otentikasi yang digunakan, yang perlu Anda isi jika Anda ingin memonitor perangkat dengan SNMP versi 3.

### Authorization
Parameter pada *Request Header* yang digunakan untuk memberikan informasi kredensial yang mengotentikasi pengguna dengan server. *Authorization* ini diperlukan jika Anda ingin memonitor *website* atau *API* yang terlindungi, seperti adanya *Basic Authentication* atau *Bearer Token* sebagai syarat untuk dapat mengakses *website* atau *API* tersebut.

### Average CPU Usage
Rata-rata penggunaan CPU di semua perangkat yang terdeteksi.

### Average Disk IO
Rata-rata operasi *input/output disk* di semua server yang terdeteksi.

### Average In
Rata-rata trafik paket data yang masuk ke *link/port/interface* tertentu dari total kapasitas.

### Average Out
Rata-rata trafik paket data yang keluar dari *link/port/interface* tertentu dari total kapasitas.

### Average RAM Usage
Rata-rata penggunaan RAM di semua perangkat yang terdeteksi.

### Average Storage Usage
Rata-rata penggunaan storage di semua perangkat yang terdeteksi.

## B
### Baseline
Nilai yang digunakan sebagai *trigger* atau pemicu untuk mengirimkan notifikasi atau *alert* kepada pengguna, ketika penggunaan *link* dan *resource* perangkat (*CPU, Memory, Storage*) mencapai *threshold*. Secara default, nilai *baseline* adalah -1.

### Bot Token
Token dari bot telegram yang sudah dibuat. Bot token diperlukan untuk membuat notifikasi atau *alert*, dan diinputkan pada *form Add Notification* jika Anda memilih Telegram sebagai tipe notifikasi.

## C
### Capacity Usage
Pada menu *Predictive Analysis Memory* dan *Storage*, *Capacity Usage* merujuk pada grafik tren penggunaan memori dan *storage* dari suatu perangkat.

### Checking Period
Lamanya waktu toleransi pengecekan status dari perangkat yang dimonitor, hingga dianggap mengalami *down* atau penggunaan *resource* perangkat mencapai *threshold*, sehingga sistem dapat mengirimkan notifikasi.

### Command
Perintah yang diinputkan melalui *Terminal* atau *Command Prompt* agar sistem dapat melakukan operasi tertentu.

### CPU Status
Status penggunaan suatu CPU dari perangkat tertentu. Terdapat 3 kategori status penggunaan, yaitu *Normal*, *Warning*, dan *Critical*.

### CPU (User)
Persentase penggunaan CPU saat mengeksekusi proses di tingkat aplikasi pengguna.

### CPU (System)
Persentase penggunaan CPU saat mengeksekusi proses di tingkat *system*.

### Critical
Kategori penggunaan CPU, RAM, *Storage*, *Interface*, dan *DiskIO* yang penggunaannya di atas 80% dari total kapasitas.


### Custom Range
*Datepicker* yang memungkinkan Anda menentukan rentang tanggal sendiri dengan memilih tanggal mulai dan tanggal akhir ketika Anda ingin mengubah periode waktu dari data *Network Monitoring* yang ditampilkan sesuai dengan kebutuhan Anda.

### Customer Portal
Aplikasi DTP (*Digital Touch Point*) yang digunakan di sisi *customer* atau pelanggan Netmonk. Melalui aplikasi ini, pelanggan dapat membuat order untuk mendapatkan akses ke Netmonk Prime versi *Free Trial* maupun berbayar. Sebelum dapat mengakses aplikasi Customer Portal, pelanggan perlu membuat akun terlebih dahulu melalui *website* Netmonk.

## D
### Dashboard
Tampilan visual yang merupakan kombinasi teks dan grafik, yang berisi informasi-informasi terkini dari perangkat yang dimonitor agar pengguna dapat melihat hal-hal yang perlu diketahui dengan format yang lebih mudah dimengerti.

### Device Down
Perangkat yang berstatus mati.

### Device Up
Perangkat yang berstatus hidup.

### Directory Path
String yang menunjukkan di mana suatu direktori atau folder dapat ditemukan.

### Disk
Perangkat penyimpanan utama di komputer.

### Disk Name
Nama *disk* dari suatu server yang dimonitor oleh Netmonk Prime.

### Disk IO (Read)
Total data yang ditulis ke dalam *disk* dari setiap server yang termonitor.

### Disk IO Status
Status rata-rata operasi *input/output* suatu *disk* dari server tertentu. Terdapat 3 kategori status, yaitu *Normal*, *Warning*, dan *Critical*.

### Disk IO (Written)
Total data yang dibaca oleh *disk* dari setiap server yang termonitor.

### Disk Load History
Grafik yang menampilkan *history* atau riwayat rata-rata operasi *input/output* suatu *disk* pada periode waktu tertentu.

### Disk Load Status
Persentase rata-rata operasi *input/output* suatu *disk* pada periode waktu tertentu.

### Disk Read/Written History
Grafik yang menampilkan *history* atau riwayat jumlah data yang ditulis ke dalam *disk* dan jumlah data yang dibaca oleh *disk* pada periode waktu tertentu.

### Down
Kondisi di mana *website* atau *API* tidak dapat dimonitor dan tidak sesuai dengan apa yang diharapkan (*response condition* yang didefinisikan tidak terpenuhi) atau perangkat dan server yang dimonitor terdeteksi mati.

### Downstream
Pada menu *Predictive Analysis Link*, *Upstream* merujuk pada grafik tren trafik suatu *link/port/interface* untuk trafik data yang diterima oleh *link/port/interface* tersebut.

### Downtime
Istilah yang merujuk pada kondisi di mana suatu perangkat atau server terdeteksi mati, atau *website/API* yang dimonitor oleh Netmonk Prime tidak tersedia.

### Downward trend
Keterangan yang muncul pada menu *Predictive Analysis* jika tren penggunaan *resource* perangkat (*Link*, *Memory*, dan *Storage*) diprediksi menurun.

## E
### Edge
Istilah yang merujuk pada garis penghubung antara dua *node* atau perangkat yang dimonitor, yang dapat Anda lihat pada *Network Map*.

### Expired Subscription
Daftar *subscription* di mana masa aktif dari order terbaru yang Anda buat sebelumnya sudah berakhir. Jika *subscription* Anda *expired*, Anda harus melakukan perpanjangan terlebih dahulu untuk dapat mengakses kembali halaman Netmonk Prime. Pastikan perpanjangan dilakukan maksimal 14 hari setelah tanggal *expired*. Jika melebihi waktu tersebut, Anda harus menambahkan *subscription* baru untuk berlangganan Netmonk Prime karena data-data Anda pada *subscription* sebelumnya sudah terhapus oleh sistem. 

### Export to CSV
Di Netmonk Prime, Anda dapat mengunduh data-data *Network Monitoring* maupun *Predictive Analysis* ke dalam file CSV melalui tombol *Export to CSV*.

## F
### File System Type
Jenis sistem berkas yang membedakan bagaimana informasi pada suatu *disk* disimpan atau diambil.

### Free Trial
Versi Netmonk Prime yang dapat Anda gunakan secara gratis selama 14 hari untuk semua fitur. Setelah 14 hari, jika Anda ingin menggunakannya kembali, Anda perlu menambahkan *subscription* baru untuk berlangganan Netmonk Prime versi berbayar. Selain itu, Netmonk Prime *Free Trial* dibatasi hanya dapat memonitor 0-5 perangkat, sedangkan Netmonk Prime versi berbayar dapat memonitor lebih banyak perangkat sesuai dengan paket yang dipilih.

## G
### Generate Report
Proses yang dilakukan untuk menghasilkan rangkuman data-data terkait perangkat yang dimonitor selama periode tertentu dalam bentuk laporan dari informasi yang diinputkan pengguna.

## H
### History Link Traffic
Grafik yang menampilkan *history* atau riwayat trafik paket data yang masuk (diwakili warna biru) dan keluar (diwakili warna merah) pada periode waktu tertentu.

### History Usage
Grafik yang menampilkan *history* atau riwayat penggunaan CPU, RAM, dan *Storage* pada periode waktu tertentu.

### Hostname
Nama *host* yang terkonfigurasi di perangkat yang dimonitor Netmonk Prime.

## I
### ICMP
Singkatan dari *Internet Control Message Protocol* yang merupakan protokol lapisan jaringan yang digunakan Netmonk Prime untuk mengetahui status dari perangkat yang dimonitor, apakah hidup (*Up*) atau mati (*Down*).

### ICMP Availability
*Ratio availability* atau tingkat ketersediaan perangkat melalui deteksi via ICMP pada periode waktu tertentu.

### In (Latest)
Rata-rata trafik paket data yang masuk ke *interface* suatu server dari total kapasitas.

### Interface
Titik interkoneksi antara perangkat dengan jaringan privat maupun publik.

### Interval Check
Nilai yang menunjukkan rentang waktu diperiksanya status *website/API* secara berkala pada *Web/API Monitoring*.

### Inventory Name
Nama perangkat yang terdaftar di *Network Device Inventory*.

### IP Address
Alamat IP dari perangkat yang dimonitor, yang menunjukkan identitas setiap perangkat yang menggunakan protokol internet untuk berkomunikasi melalui jaringan.

## J
### Json Editor View
Tampilan pada saat mengedit *Network Map* yang digunakan untuk melihat dan mengedit data *Map* dalam format JSON.

## K
### Kode OTP
Kode yang digunakan untuk melakukan proses verifikasi nomor *handphone* di *Customer Portal* sebelum pengguna dapat mencoba Netmonk Prime versi *Free Trial*. OTP merupakan singkatan dari *One Time Password* yang ditujukan sebagai *password* sekali pakai.

### Kode Unik
Tiga digit terakhir dari total pembayaran suatu order Netmonk Prime yang tercantum di halaman *Detail Pembayaran*. Kode unik ini digunakan untuk mempermudah tim Netmonk dalam memvalidasi pembayaran yang sudah Anda lakukan.

## L
### Latest CPU Usage
Persentase penggunaan terakhir indeks CPU tertentu dari perangkat yang dimonitor Netmonk Prime.

### Latest RAM Usage
Persentase penggunaan terakhir indeks RAM tertentu dari perangkat yang dimonitor Netmonk Prime.

### Latest Storage Usage
Persentase penggunaan terakhir indeks *Storage* atau media penyimpanan tertentu dari perangkat yang dimonitor Netmonk Prime.

### Linear
Grafik yang menunjukkan tren trafik, tren penggunaan memori, dan tren penggunaan *storage*, jika diasumsikan trafik data, penggunaan memori, dan penggunaan *storage* di masa mendatang di antara 50% dan 80% dari total kapasitas.

### Linear Bottom
Grafik yang menunjukkan tren trafik, tren penggunaan memori, dan tren penggunaan *storage*, jika diasumsikan trafik data, penggunaan memori, dan penggunaan *storage* di masa mendatang di bawah 50% dari total kapasitas.

### Linear Top
Grafik yang menunjukkan tren trafik, tren penggunaan memori, dan tren penggunaan *storage*, jika diasumsikan trafik data, penggunaan memori, dan penggunaan *storage* di masa mendatang di atas 80% dari total kapasitas.

### Link Utilization
Persentase rata-rata trafik data dari *link/port/interface* tertentu yang dimonitor Netmonk Prime dari total kapasitas.

### Load Average
Persentase rata-rata operasi *input/output* suatu *disk* pada periode waktu tertentu.

### Load CPU
Jumlah *core* CPU yang digunakan untuk mengeksekusi proses, yang mewakili beban kerja CPU di setiap server.

### Location
Nama lokasi yang menunjukkan tempat perangkat yang dimonitor berada.

## M
### Management Mode
*Dashboard* yang ditujukan untuk pihak managerial sehingga disajikan dalam format yang lebih ringkas dan mudah dimengerti.

### Map Editor View
Tampilan pada saat mengedit *Network Map* yang digunakan untuk melihat dan mengedit data *Map* dalam format visual yang lebih mudah dimengerti.

### Masa Aktif Subscription
Masa atau periode waktu di mana pengguna dapat menggunakan Netmonk Prime.

### Match Body
Data di dalam *response body* yang diharapkan keluar sebagai *response* dari *website/API* yang dimonitor. Jika *Match Body* yang diinputkan tidak ada di dalam *response*, maka status *website/API* tersebut dianggap *down* karena tidak sesuai dengan kondisi yang diharapkan. 

### Match Header
*Response header* yang diharapkan keluar sebagai *response* dari *website/API* yang dimonitor. Jika *Match Header* yang diinputkan tidak ada di dalam *response*, maka status *website/API* tersebut dianggap *down* karena tidak sesuai dengan kondisi yang diharapkan. 

M### emory (User)
Persentase penggunaan *memory* di tingkat aplikasi pengguna.

### Message Broker Address
Alamat *message broker* atau perangkat lunak yang bertugas mengatur antrian dan lalu lintas data antar servis di Netmonk Prime.

### Method
*Request method* yang menunjukkan tindakan yang diinginkan untuk dilakukan terhadap *website/API* yang akan dimonitor.

### MTTR
Singkatan dari *Mean Time To Repair* yang merupakan rata-rata waktu yang dibutuhkan suatu *link/port/interface* untuk hidup kembali (*up*) dari kondisi mati (*down*) dalam satuan jam.

### Multiple Node
Bagian dari menu *Network Device Inventory* yang memungkinkan Anda menambahkan data perangkat dalam jumlah banyak secara sekaligus dengan memanfaatkan *template* file CSV yang sudah disediakan.

## N
### Network
Istilah yang merujuk pada jaringan telekomunikasi yang memungkinkan perangkat dapat saling berkomunikasi dengan bertukar data.

### Network Device
Istilah yang merujuk pada perangkat yang dimonitor oleh Netmonk Prime.

### Network Map
Gambaran persebaran perangkat-perangkat yang dimonitor sehingga mempermudah pengguna dalam melihat perangkat mana yang sedang bermasalah dan perlu pengawasan.

### Network Probe
Perangkat lunak yang bertugas mengumpulkan data dari perangkat jaringan yang akan dimonitor oleh Netmonk Prime. Secara *default*, *network probe* diinstal bersama Netmonk Prime di *cloud*. Hal ini memungkinkan monitoring perangkat jaringan yang memiliki alamat statis *IP Public*. Jika perangkat yang dimonitor berada di jaringan *private* atau tertutup, *network probe* perlu diinstal terpisah di server atau komputer yang berada di jaringan *private* tersebut dan dipastikan *network probe* dapat mengakses ke internet.

### Node
Istilah yang merujuk pada perangkat yang dimonitor oleh Netmonk Prime. Pada *Network Map*, *Node* digambarkan dengan lingkaran. Lingkaran akan berwarna hijau jika perangkat hidup (*Up*) dan berwarna merah jika perangkat mati (*Down*).

### Normal
Kategori penggunaan CPU, RAM, *Storage*, *Interface*, dan *DiskIO* yang penggunaannya di bawah 50% dari total kapasitas.

### Notification Type
Tipe notifikasi yang Anda pilih ketika akan membuat notifikasi. Terdapat 2 tipe notifikasi, yaitu Telegram dan Email.

### N/A
Istilah yang merujuk pada kondisi di mana *resource* perangkat yang dimonitor sedang tidak tersedia. *N/A* merupakan singkatan dari *Not Available*.

## O
### Order ID
Nomor unik yang membantu Anda dalam mengidentifikasi dan mencari order tertentu dari suatu *subscription*. Anda dapat melakukan banyak order seperti mencoba Netmonk Prime versi *Free Trial*, berlanggan Netmonk Prime versi berbayar, meng-*upgrade* Netmonk Prime Free Trial ke berbayar, atau memperpanjang masa aktif Netmonk Prime versi berbayar.

### Out (Latest)
Rata-rata trafik paket data yang keluar dari *interface* suatu server dari total kapasitas.

### Outages
Keadaan terjadinya perubahan dari suatu status (*Up*, *Pause*, atau *Unknown*) ke *Down* pada *Web/API Monitoring*.

## P
### Packet Discard
Jumlah paket data yang mengalami *discard* atau ditolak oleh penerima.

### Packet Error
Jumlah paket data yang mengalami *error* saat diterima.

### Partition Name
Nama partisi atau bagian tertentu dari suatu *disk* yang digunakan untuk menyimpan berbagai data.

### Pause
Kondisi di mana pengguna menghentikan sementara proses monitoring dari suatu *website* atau *API*, misalnya karena adanya keperluan untuk melakukan *maintenance*.

### PID
Nomor unik yang mengidentifikasi setiap *process*.

### Ping
Sebuah program utilitas yang dapat digunakan untuk memeriksa konektivitas internet dan kualitas suatu jaringan. Cara kerja Ping sebenarnya cukup sederhana, yakni perangkat klien mengirimkan sebuah paket, kemudian server yang dituju akan mengirimkan respon ke perangkat klien tersebut.

### Predictive Analysis
Menampilkan tren trafik semua *link/port/interface*, penggunaan resource perangkat, serta informasi jumlah sisa hari sebelum link/port/interface atau resource perangkat tersebut mencapai ambang batas penggunaan. Prediksi dilakukan dengan memanfaatkan data historis yang tersimpan di di dalam database sehingga pengguna dapat melakukan upaya-upaya pencegahan sebelum kegiatan operasional di lingkungan pengguna terhambat.

### Process
Setiap program yang sedang berjalan atau dieksekusi oleh server.

## Q
### Quick Range
*Databox* berupa pilihan periode waktu yang sudah ditentukan sistem sehingga Anda dapat mengubah periode waktu dari data Network Monitoring yang ditampilkan sesuai dengan kebutuhan Anda. Beberapa contoh pilihan yang disediakan yaitu “Today”, “Last 24 hours”, dan “This Month”.

## R
### RAM Status
Status penggunaan suatu RAM dari perangkat tertentu. Terdapat 3 kategori status penggunaan, yaitu *Normal*, *Warning*, dan *Critical*.

### Remaining Days to Threshold
Jumlah hari yang menunjukkan berapa lama lagi trafik atau penggunaan *resource* perangkat (*Link*, *Memory*, dan *Storage*) akan mencapai ambang batas. Jika tren diprediksi menurun, keterangan yang muncul adalah *downward trend*.

### Reminder Period
Periode waktu dikirimkannya notifikasi atau *alert* secara berkala.

### Request Header
*Request header* yang dapat digunakan dalam *HTTP request* untuk memberikan informasi tentang konteks permintaan.

### Response Time
Total waktu yang diperlukan sistem untuk menanggapi permintaan (*request*) setelah menerimanya dari klien. *Response time* atau waktu respon dipengaruhi oleh waktu pemrosesan permintaan yang diperlukan di server dan *latency* atau waktu *delay* permintaan untuk mencapai server dan kembali lagi ke klien.

### Role
Tingkatan pengguna yang memiliki hak akses berbeda. Terdapat tiga tingkatan pengguna, yaitu Root, Admin, dan User.

### Root
Salah satu jenis *role* yang terdapat pada Netmonk Prime. Root merupakan *role* dengan kedudukan tertinggi sehingga pengguna memiliki hak akses ke semua fitur.

## S
### Scarpe Interval
Interval waktu pengambilan data pada *network probe* dalam satuan menit.

### Search
Kolom pencarian yang dapat Anda gunakan untuk menemukan informasi yang Anda butuhkan.

### Security Level
Level keamanan yang perlu Anda pilih jika Anda ingin memonitor perangkat dengan SNMP versi 3. Terdapat 3 jenis *security level* yaitu authPriv yang menggunakan otentikasi dan *privacy*, authNoPriv yang hanya menggunakan otentikasi, dan noAuthNoPriv yang tidak menggunakan keduanya.

### Server
Sistem komputer yang memiliki *resource* besar dan menyediakan fungsionalitas atau layanan khusus untuk kebutuhan komputer atau perangkat lain yang biasanya disebut sebagai klien.

### Server Down
Server yang berstatus mati.

### Server Up
Server yang berstatus hidup.


### SNMP
Singkatan dari *Simple Network Management Protocol* yang digunakan untuk memonitor dan mengelola berbagai perangkat jaringan. Netmonk Prime memanfaatkan SNMP untuk mendapatkan data trafik dan *uptime*.

### SMTP
Singkatan dari *Simple Mail Transfer Protocol* yang merupakan protokol untuk mengirim email dari dari satu akun email ke akun email lainnya melalui internet.

### Starred Link
*Starred Link* memungkinkan Anda untuk mengelompokkan atau menandai *link/port/interface* tertentu yang dianggap perlu lebih diperhatikan karena hal tertentu, sehingga Anda dapat dengan mudah menemukannya di kemudian hari.

### Status Code
Kode status HTTP yang diharapkan keluar sebagai response dari *website/API* yang dimonitor. Jika *Status Code* yang diinputkan tidak ada di dalam *response*, maka status *website/API* tersebut dianggap *down* karena tidak sesuai dengan kondisi yang diharapkan. 

### Status History
Grafik yang menampilkan *history* atau riwayat status dari server yang dimonitor. Pada *Network Monitoring*, grafik ini menampilkan riwayat status perangkat atau *link/port/interface* dari perangkat tersebut melalui deteksi via ICMP pada periode waktu tertentu.

### Status Ratio
*Ratio availability* atau tingkat ketersediaan dari suatu *link/port/interface* pada periode waktu tertentu.

### Storage Status
Status penggunaan suatu *storage* atau media penyimpanan dari perangkat tertentu. Terdapat 3 kategori status penggunaan, yaitu *Normal*, *Warning*, dan *Critical*.

### Subscription ID
Nomor unik yang merujuk pada suatu *subscription*. Dalam 1 *subscription*, Anda dapat melakukan banyak order seperti mencoba Netmonk Prime versi *Free Trial*, berlanggan Netmonk Prime versi berbayar, meng-*upgrade* Netmonk Prime *Free Trial* ke berbayar, atau memperpanjang masa aktif Netmonk Prime versi berbayar.

### Synchronize
Proses sinkronisasi data daftar perangkat yang baru ditambahkan dengan *network probe* yang bertugas mengambil data dari perangkat jaringan.

### System Performance
Menu pada Netmonk Prime yang menampilkan informasi performa sistem atau server tempat Netmonk Prime diinstal, seperti persentase penggunaan memori, *storage*, dan CPU, total trafik data dari setiap *network*, serta daftar semua proses yang berjalan pada sistem dan statusnya.

## T
### Tags
Kata kunci yang dapat Anda tambahkan untuk mendeskripsikan *website/API* yang akan dimonitor, sehingga Anda dapat dengan mudah menemukannya di kemudian hari.

### Thread
Jumlah *thread* yang bertugas mengeksekusi suatu proses. *Thread* sendiri merupakan komponen yang membagi inti CPU menjadi beberapa inti virtual sehingga memungkinkan CPU untuk mengeksekusi banyak tugas dari suatu *process* dalam satu waktu yang sama.

### Threshold
Nilai yang menunjukkan ambang batas penggunaan *resource* perangkat (*Link*, CPU, RAM, *Storage*).

### Total Read
Total data yang ditulis ke dalam *disk* dari setiap server yang termonitor.

### Total Written
Total data yang dibaca oleh *disk* dari setiap server yang termonitor.

### Trafik
Komponen yang digunakan untuk melakukan pengukuran atau pengendalian lalu lintas jaringan. Trafik mengacu pada jumlah data yang bergerak melintasi jaringan pada titik waktu tertentu.

## U
### Unknown
Kondisi di mana sistem tidak dapat memonitor *website* atau *API* yang sudah Anda daftarkan.

### Up
Kondisi di mana *website* atau *API* dapat dimonitor dan sesuai dengan apa yang diharapkan *(response condition* yang didefinisikan tidak terpenuhi) atau perangkat dan server yang dimonitor berstatus hidup.

### Upstream
Pada menu *Predictive Analysis Link*, *Upstream* merujuk pada grafik tren trafik suatu *link/port/interface* untuk trafik data yang dikirim dari *link/port/interface* tersebut.

### Uptime
Istilah yang merujuk pada waktu hidup perangkat, server, atau *website/API* yang dimonitor oleh Netmonk Prime. *Uptime* bisa menjadi metrik untuk mengukur tingkat ketersediaan perangkat, server, atau *website/API* yang menyatakan persentase waktu perangkat, server, atau *website/API* tersebut berhasil beroperasi. 

### Uptime Availability
*Ratio availability* atau tingkat ketersediaan perangkat dari deteksi via SNMP *Uptime* pada periode waktu tertentu.

### Uptime History
Grafik yang menampilkan *history* atau riwayat status perangkat  dari deteksi via SNMP *Uptime* pada periode waktu tertentu.

### Usage Status
Grafik yang menampilkan *history* atau riwayat penggunaan suatu *disk* pada periode waktu tertentu.

### User
Salah satu jenis *role* yang terdapat pada Netmonk Prime. Pengguna dengan *role* User memiliki hak akses paling terbatas. User tidak diizinkan mengakses halaman *Users*, serta tidak diizinkan untuk menambah, mengubah, atau menghapus data pada Netmonk Prime.

## V
Tidak ada item glosarium.

## W
### Warning
Kategori penggunaan CPU, RAM, *Storage*, *Interface*, dan *DiskIO* yang penggunaannya di antara 50% s.d 80% dari total kapasitas.

### Web/API Probe
Perangkat lunak yang bertugas mengumpulkan data dari *website* atau *API* berbasis HTTP/HTTPS yang akan dimonitor oleh Netmonk Prime. Secara *default*, *web/API probe* diinstal bersama Netmonk Prime di *cloud*. Jika *website* atau *API* yang dimonitor berada di sebuah server atau komputer dan tidak di-*publish* ke internet, *web/API probe* perlu diinstal terpisah di server atau komputer tersebut dan dipastikan *web/API probe* dapat mengakses ke internet.

## X
Tidak ada item glosarium.

## Y
Tidak ada item glosarium.

## Z
Tidak ada item glosarium.
