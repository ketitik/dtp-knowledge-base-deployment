---
title: 'Bagaimana cara mengedit data website yang sudah saya daftarkan?'
category: 'website'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Akses menu Web/API Monitoring → Web/API Status'
---

# Bagaimana cara mengedit data website yang sudah saya daftarkan?

1. Akses menu **Web/API Monitoring → Web/API Status.**

    ![Menu Web Status](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/web-api-monitoring/%5B2%5Dwebsite/menu-web-status.png)

2. Dari halaman utama *Web/API Status*, klik tombol **Edit Data** di salah satu baris data.

    ![Tombol Edit Data Web](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/web-api-monitoring/%5B2%5Dwebsite/tombol-edit-data-web.png)

3. Selanjutnya akan tampil *form* untuk mengedit data seperti berikut:

    ![Form Edit Data Web](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/web-api-monitoring/%5B2%5Dwebsite/form-edit-data-web.png)

4. Edit data sesuai kebutuhan, kemudian klik tombol **Save** untuk menyimpan perubahan pada data.