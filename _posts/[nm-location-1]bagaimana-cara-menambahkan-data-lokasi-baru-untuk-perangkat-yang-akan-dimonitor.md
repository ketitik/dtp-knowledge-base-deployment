---
title: 'Bagaimana cara menambahkan data lokasi baru untuk perangkat yang akan dimonitor?'
category: 'location'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Akses menu Settings → Locations'
---

# Bagaimana cara menambahkan data lokasi baru untuk perangkat yang akan dimonitor?

1. Akses menu **Settings → Locations**

    ![settings-locations](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B9%5Dlocation/settings-locations.png)

2. Dari halaman utama menu *Locations Settings* atau manajemen lokasi, klik tombol **Add New Location**.

    ![add-location-button](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B9%5Dlocation/add-location-button.png)

3. Selanjutnya akan muncul *form* berikut:

    ![add-location-form](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B9%5Dlocation/add-location-form.png)

4. Isi *form* teresbut dengan nama lokasi tempat perangkat yang akan dimonitor berada.

5. Klik tombol **Add** untuk menambahkan lokasi baru.