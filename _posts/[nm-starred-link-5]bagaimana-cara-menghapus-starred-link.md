---
title: 'Bagaimana cara menghapus starred link yang sudah saya daftarkan?'
category: 'starred-link'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Akses menu menu Network Status → Starred Link'
---

# Bagaimana cara menghapus starred link yang sudah saya daftarkan?

1. Akses menu menu **Network Status → Starred Link**

    ![Menu Starred Link](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B3%5Dstarred-link/menu-starred-link-v2.png)

2. Dari halaman utama menu *Starred Link*, klik tombol **Delete** di salah satu baris data *starred link*,

    ![Tombol Hapus Starred Link](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B3%5Dstarred-link/tombol-hapus-starred-link.png)

3. Selanjutnya akan tampil kotak dialog seperti berikut:

    ![Hapus Data](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B3%5Dstarred-link/hapus-data.png)

4. Klik tombol **Delete** untuk melanjutkan proses hapus