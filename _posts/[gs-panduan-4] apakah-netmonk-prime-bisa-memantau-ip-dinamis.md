---
title: 'Apakah Netmonk Prime bisa memantau IP dinamis?'
category: 'panduan-pengguna-baru'
is_sticky: true
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Secara prinsip, alamat perangkat yang didaftarkan di Netmonk Prime adalah alamat perangkat yang bersifat statis.'
---

# Apakah Netmonk Prime bisa memantau IP dinamis?

Secara prinsip, alamat perangkat yang didaftarkan di Netmonk Prime adalah alamat perangkat yang bersifat statis, baik itu alamat IP maupun alamat domain. Dan apabila perangkat yang akan dimonitoring menggunakan alamat IP dinamis, maka kami merekomendasikan untuk menggunakan DDNS agar perangkat tetap dapat dikenali. DDNS akan membuat perangkat yang memiliki IP  dinamis untuk memiliki domain yang statis.

> Note : DDNS tidak termasuk dalam biaya berlangganan Netmonk Prime.