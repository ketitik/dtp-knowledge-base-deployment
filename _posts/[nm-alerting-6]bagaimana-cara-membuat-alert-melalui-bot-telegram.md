---
title: 'Bagaimana cara membuat alert melalui bot telegram yang sudah dibuat?'
category: 'alerting'
is_sticky: true
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Akses menu Settings → Alert, dari halaman utama menu Alert Setting, klik tombol Add New Notification.'
---

# Bagaimana cara membuat alert melalui bot telegram yang sudah dibuat?

1. Akses menu **Settings → Alert**.

    ![Menu Alert](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B14%5Dalerting/menu-alert.png)

2. Dari halaman utama menu *Alert Setting* yaitu *tab* **Alert Notification**, klik tombol **Add New Notification**, 

    ![Tombol Add New Notification](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B14%5Dalerting/tombol-tambah-notifikasi.png)

3. Selanjutnya akan tampil *form* berikut:

    ![Form Add Notification](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B14%5Dalerting/form-tambah-notifikasi.png)

4. Isi *form* tersebut dengan rincian sebagai berikut, kemudian klik tombol **Add**.

    | **Jenis Isian** | **Keterangan** |
    | ----- | ----- |
    | Name | Nama profil *alerting*. Pengisian nama diharapkan tidak menggunakan spasi dan memanfaatkan dash (“-“) sebagai pemisah kata. |
    | Notification Type | Tipe *alerting*, diisi dengan pilihan “Telegram”. |
    | Bot Token | Token dari bot telegram yang sudah dibuat [di sini]([nm-alerting-4]bagaimana-cara-membuat-bot-telegram-dan-mendapatkan-bot-token).|
    | Recipients | Chat-id di mana telegram akan mengirimkan *alert*. Cara mendapatkan chat-id tersebut dapat dilihat [di sini]([nm-alerting-5]bagaimana-cara-mendapatkan-chat-id-telegram)|

5. Selanjutnya, beralih ke *tab* **Alert Rules**, daftarkan profil *alerting* yang sudah dibuat ke *alert rules*, misal *Network Device Status*.

    ![Alert Rules](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B14%5Dalerting/alert-rules-device-status.png)

6. Klik tombol **Edit** di baris *Network Device Status*, maka akan muncul sebuah *form* untuk mengatur *rule device status* seperti berikut:

    ![Edit Alert Rules](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B14%5Dalerting/form-alert-rule-device-status.png)

    >**Keterangan:**
    >*Form* di atas berlaku juga untuk mengatur *rule* pada *Website Status.*

7. Isi *form* tersebut dengan rincian sebagai berikut:

    | **Jenis Isian** | **Keterangan** |
    | ----- | -----|
    | Checking Period | Lamanya waktu toleransi pengecekan status dari device hingga dianggap mengalami down sehingga sistem dapat mengirimkan *alert*. Diisi dalam satuan detik (s), menit (m), atau jam (h) |
    | Reminder Period | Periode waktu dikirimkannya  *alert* secara berkala. Diisi dalam satuan detik (s), menit (m), atau jam (h) |
    | Alert Notification Profiles | Nama profil *alerting* yang sudah dibuat di bagian **Alert Notification**, bisa diisi lebih dari satu.|

8. Klik tombol **Save** untuk menyimpan perubahan atau tombol **Cancel** untuk membatalkan.
   
9.  Terakhir, aktifkan *rule* tersebut dengan menekan tombol *switch* di baris *Network Device Status* bagian kiri sehingga dari warna abu-abu berubah menjadi aktif berwarna hijau seperti contoh berikut:

    ![Aktifkan Rule](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B14%5Dalerting/activate-alert-device-status.png)


Beberapa contoh tampilan dari *alerting* yang dikirimkan oleh *bot telegram* ketika *device* berstatus *down*:

![Contoh Alert Telegram](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B14%5Dalerting/contoh-alert-telegram.png)


Berikut video singkat terkait contoh cara membuat *alert* melalui bot telegram:

<iframe width="550" height="300"
src="https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B14%5Dalerting/cara-membuat-notifikasi-melalui-bot-telegram.mp4">
</iframe>