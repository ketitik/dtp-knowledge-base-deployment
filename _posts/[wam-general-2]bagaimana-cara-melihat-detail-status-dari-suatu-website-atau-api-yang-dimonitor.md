---
title: 'Bagaimana cara melihat detail status dari suatu website atau API yang dimonitor?'
category: 'general'
is_sticky: true
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Akses menu Web/API Monitoring → Web/API Status, dari halaman utama Web/API Status, klik tombol View Data.'
---

# Bagaimana cara melihat detail status dari suatu website atau API yang dimonitor?

1. Akses menu **Web/API Monitoring → Web/API Status**.

    ![Menu Web Status](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/web-api-monitoring/%5B1%5Dgeneral/menu-web-status.png)

2. Dari halaman utama *Web/API Status*, klik tombol **View Data** di salah satu baris data.

    ![Tombol View Web Status](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/web-api-monitoring/%5B1%5Dgeneral/tombol-view-detail-web-status.png)

3. Maka akan muncul tampilan seperti berikut:

    ![Halaman Detail Web Status](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/web-api-monitoring/%5B1%5Dgeneral/detail-web-status.png)

    Keterangan:
    a. Informasi dasar seperti nama dan alamat *website* atau *API*, serta tipe protokol yang digunakan.

    b. Grafik *average response time* atau rata-rata waktu respon dari *website* atau *API* yang dimonitor pada periode waktu tertentu, beserta ambang batas atau *threshold response time* yang sudah ditentukan (diwakili garis putus-putus berwarna merah).

    c. Status hidup atau tidaknya *website* atau *API* pada periode waktu tertentu. Terdapat 4 status, yaitu sebagai berikut:
      - *Up*: kondisi di mana *website* atau *API* dapat dimonitor dan sesuai dengan apa yang diharapkan (*response condition* yang didefinisikan terpenuhi)
      - *Down*: kondisi di mana *website* atau *API* tidak dapat dimonitor dan tidak sesuai dengan apa yang diharapkan (*response condition* yang didefinisikan tidak terpenuhi)
      - *Pause*: kondisi di mana pengguna menghentikan sementara proses *monitoring* dari suatu *website* atau *API*, misalnya karena adanya keperluan untuk *maintenance*.
      - *Unknown*: kondisi di mana sistem tidak dapat memonitor *website* atau *API*.
  
    d. Nilai *downtime* dan *uptime* dari *website* atau *API* yang dimonitor pada periode waktu tertentu beserta persentasenya.

    e. Rincian histori status dari *website* atau *API* yang dimonitor pada periode waktu tertentu.

    > **Notes:**
    > *Outages* merupakan keadaan terjadinya perubahan dari suatu status (*Up, Pause, Unknown*) ke *Down*.

Berikut video singkat terkait cara melihat detail status dari suatu *website* atau *API* yang dimonitor:

<iframe width="425" height="275"
src="https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/web-api-monitoring/%5B1%5Dgeneral/cara-melihat-detail-web-api-status.mp4">
</iframe>