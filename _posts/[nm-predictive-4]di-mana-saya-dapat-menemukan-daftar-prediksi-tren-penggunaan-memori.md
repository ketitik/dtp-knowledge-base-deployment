---
title: 'Di mana saya dapat menemukan daftar prediksi tren penggunaan semua indeks memori yang termonitor Netmonk Prime?'
category: 'predictive-analysis'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Akses menu Predictive Analysis → Memory'
---

# Di mana saya dapat menemukan daftar prediksi tren penggunaan semua indeks memori yang termonitor Netmonk Prime?

1. Akses menu **Predictive Analysis → Memory**

    ![Menu Predictive Analysis Memory](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B12%5Dpredictive-analysis/menu-predictive-analysis-memory-v2.png)

2. Maka akan tampil daftar prediksi tren penggunaan semua indeks memori yang termonitor oleh sistem seperti berikut:

    ![Menu Predictiev Analysis Memory](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B12%5Dpredictive-analysis/predictive-memory-v2.png)

    Keterangan:
    a. *Hostname* yang terkonfigurasi di perangkat.
    b. Alamat IP dari perangkat yang dimonitor.
    c. Nama setiap indeks memori dari perangkat yang dimonitor.
    d. Kapasitas maksimal masing-masing memori.
    e. Ambang batas penggunaan memori.
    f. Jumlah hari yang menunjukkan berapa lama lagi penggunaan memori akan mencapai ambang batas. Jika tren diprediksi menurun, keterangan yang muncul adalah *downward trend*.