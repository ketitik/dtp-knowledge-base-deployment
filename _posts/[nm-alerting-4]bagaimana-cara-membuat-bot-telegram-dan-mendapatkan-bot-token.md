---
title: 'Bagaimana cara membuat bot telegram dan mendapatkan bot token untuk membuat alerting atau melalui telegram?'
category: 'alerting'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Bot token yang dibutuhkan untuk membuat alert melalui telegram'
---

# Bagaimana cara membuat bot telegram dan mendapatkan bot token untuk membuat alerting melalui telegram?

Bot token yang dibutuhkan untuk membuat *alerting* melalui telegram bisa didapatkan dengan cara berikut:

1. Masuk ke aplikasi Telegram Anda.
2. Di kolom pencarian, cari akun dengan nama berikut:

    ![BotFather](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B14%5Dalerting/bot-father.png)

3. Kirimkan pesan /start, maka akan muncul pesan balasan yang mencantumkan daftar perintah yang dapat digunakan untuk mengatur bot tersebut.

    ![Start](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B14%5Dalerting/start.png)

4. Buat bot baru dengan mengirimkan pesan /newbot.

    ![Newbot](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B14%5Dalerting/newbot.png)

5. Berikan nama untuk bot yang akan dibuat, dilanjutkan dengan mengirim *username* untuk bot tersebut. *Username* harus selalu diakhiri dengan kata “bot”, misal ketitikbot.

    ![Nama dan Username Bot](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B14%5Dalerting/ketitik-bot.png)

6. Untuk mendapatkan token API dari bot telegram yang sudah dibuat, ketikkan /mybots, maka akan muncul tampilan seperti berikut:
   
   ![Token API dari Bot Telegram](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B14%5Dalerting/mybots.png)