---
title: 'Di mana saya dapat menemukan daftar semua link/port/interface yang termonitor Netmonk Prime?'
category: 'link-utilization'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Dari Dashboard Network Summary mode operasional'
---

# Di mana saya dapat menemukan daftar semua link/port/interface yang termonitor Netmonk Prime?

Dari **Dashboard Network Summary** mode operasional, klik **View All** pada bagian **Link Utilization** atau akses melalui menu **Network Status → Link Utilization**. Berikut contoh halaman utama *Link Utilization* yang menampilkan daftar seluruh *link/port/interface* dari perangkat yang termonitor:

![Link Utilization](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B2%5Dlink-utilization/link-utilization-v2.png)

Keterangan:
a. Jumlah *link/port/interface* dengan penggunaan di bawah 50%.
b. Jumlah *link/port/interface* dengan penggunaan di antara 50% s.d 80%.
c. Jumlah *link/port/interface* dengan penggunaan di atas 80%.
d. *Hostname* yang terkonfigurasi di perangkat.
e. Nama perangkat yang terdaftar di *inventory*.
f. Alamat IP dari perangkat yang dimonitor.
g. Nama setiap *link/port/interface* dari perangkat yang dimonitor.
h. Persentase penggunaan masing-masing *link/port/interface*.
i. Kategori status penggunaan *link/port/interface*.