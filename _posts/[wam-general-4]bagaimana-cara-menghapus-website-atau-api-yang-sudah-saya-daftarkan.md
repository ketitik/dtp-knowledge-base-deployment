---
title: 'Bagaimana cara menghapus website ata API yang sudah saya daftarkan?'
category: 'general'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Akses menu Web/API Monitoring → Web/API Status'
---

# Bagaimana cara menghapus website ata API yang sudah saya daftarkan?

1. Akses menu **Web/API Monitoring → Web/API Status**.

    ![Menu Web Status](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/web-api-monitoring/%5B1%5Dgeneral/menu-web-status.png)

2. Dari halaman utama *Web/API Status*, klik tombol **Delete Data** di salah satu baris data.

    ![Tombol Delete Web](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/web-api-monitoring/%5B1%5Dgeneral/tombol-delete-web.png)

3. Selanjutnya akan tampil kotak dialog seperti berikut:

    ![Tombol Hapus Web](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/web-api-monitoring/%5B1%5Dgeneral/hapus-data.png)

4. Klik tombol **Delete** untuk melanjutkan proses hapus.