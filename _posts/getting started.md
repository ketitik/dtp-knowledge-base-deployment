---
title: 'Getting Started'
accordion: 'getting started'
category: 'getting-started'
icon: rocket
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Apa itu Netmonk Prime?'
---

# PENGENALAN NETMONK PRIME

Apa itu Netmonk Prime?

Apa saja fitur Netmonk Prime?

Bagaimana cara menggunakan Netmonk prime ?

## PANDUAN PENGGUNA BARU

Apa syarat jaringan yang dapat dimonitor oleh Netmonk Prime?

Berapa lama waktu instalasi Netmonk Prime?

Apakah Netmonk Prime dapat digunakan untuk memonitor perangkat yang berada di jaringan privat atau tertutup?

Apa itu Probe pada Netmonk Prime?

Apa spesifikasi dari server atau komputer yang akan di install probe Netmonk Prime?

Bagaimana cara mengetahui performa sistem atau server tempat Netmonk Prime di install?

Apakah perbedaan Netmonk Basic Free Trial dan Berbayar?

Paket apa saja yang ditawarkan untuk Netmonk Basic versi Berbayar?

Apa itu Customer Portal?

Bagaimana registrasi akun Customer Portal?

## UNDUH MANUAL BOOK