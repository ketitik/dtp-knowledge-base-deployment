---
title: 'Apa itu Starred Link?'
category: 'starred-link'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Starred Link memungkinkan Anda untuk mengelompokkan atau menandai link/port/interface'
---

# Apa itu Starred Link?

*Starred Link* memungkinkan Anda untuk mengelompokkan atau menandai *link/port/interface* tertentu yang dianggap perlu lebih diperhatikan karena hal tertentu, sehingga Anda dapat dengan mudah menemukannya di kemudian hari.