---
title: 'Apakah data akun saya dapat diedit?'
category: 'akun-customer-portal'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Ya, Anda dapat mengedit data akun'
---

# Apakah data akun saya dapat diedit?

Ya, Anda dapat mengedit data akun Anda melalui menu **Pengaturan → Akun**. Untuk lebih jelasnya, ikuti langkah-langkah berikut:

1. Masuk ke halaman pengaturan akun melalui menu **Pengaturan → Akun**

    ![Menu Akun](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/akun-cp/menu-akun.png)

2. Selanjutnya, akan tampil informasi data diri PIC perusahaan dan data perusahaan yang akan digunakan untuk berlangganan Netmonk Prime seperti berikut:

    ![Detail Akun](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/akun-cp/detail-akun.png)

3. Klik tombol **Edit Akun** di sisi kanan bawah
   
    ![Tombol Edit Akun](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/akun-cp/tombol-edit-akun.png)

4. Selanjutnya, akan tampil *form* untuk mengedit data akun seperti berikut:

    ![Edit Akun](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/akun-cp/edit-akun.png)

5. Edit data sesuai kebutuhan, kemudian klik tombol **Simpan Akun**. Maka akan muncul kotak dialog berikut:

    ![Simpan Akun](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/akun-cp/simpan-akun.png)

6. Klik tombol **Baik, saya mengerti** untuk menyimpan perubahan pada data