---
title: 'Paket apa saja yang ditawarkan untuk Netmonk Prime versi berbayar?'
category: 'panduan-pengguna-baru'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Terdapat 2 pilihan paket'
---

# Paket apa saja yang ditawarkan untuk Netmonk Prime versi berbayar?

Terdapat 2 pilihan paket yang dibedakan berdasarkan masa aktif penggunaan Netmonk Prime, yaitu **Paket 1 Bulan** dan **Paket 3 Bulan**. Setiap paket memiliki harga yang beragam sesuai dengan jumlah perangkat yang akan Anda monitor. Berikut tabel daftar harga untuk masing-masing paket:

![Daftar Harga Paker](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/getting-started/%5B2%5Dpanduan-pengguna/daftar-harga-paket.PNG)