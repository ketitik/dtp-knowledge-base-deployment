---
title: 'Bagaimana cara melihat histori order yang pernah dibuat?'
category: 'subscription'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Masuk ke halaman subscription'
---

# Bagaimana cara melihat histori order yang pernah dibuat?

1. Masuk ke halaman *subscription* melalui menu **Subscription** seperti berikut:

    ![Menu Subscription](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/subscription/menu-subscription.png)

2. Klik tombol **Detail** pada data yang bersangkutan

    ![List Subscription](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/subscription/list-subscription.png)

3. Setelah masuk ke halaman **Detail Subscription**, histori order yang pernah dibuat sebelumnya akan tampil pada bagian **Rincian Order Subscription** seperti contoh berikut:
   
![Histori Order](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/subscription/histori-order.png)
