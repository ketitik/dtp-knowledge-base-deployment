---
title: 'Bagaimana cara mendaftarkan perangkat baru yang akan dimonitor?'
category: 'network-device'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Akses menu Network Inventory → Network Device'
---

# Bagaimana cara mendaftarkan perangkat baru yang akan dimonitor?

1. Akses menu **Network Inventory → Network Device**

    ![Menu Network Device](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B10%5Dnetwork-device/menu-network-device-v2.png)

2. Dari halaman utama *Network Device Inventory*, klik tombol **Add Network Device**,

    ![Tombol Tambah Network Device](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B10%5Dnetwork-device/tombol-tambah-network-device.png)

3. Selanjutnya akan muncul *form* untuk mengisi detail perangkat baru yang akan ditambahkan. Secara *default*, *tab* yang terpilih adalah **Add Single Node**.

    ![Form Tambah Network Device](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B10%5Dnetwork-device/form-tambah-network-device.png)

4. Isi data pada *form* di atas dengan rincian sebagai berikut:

    | **Jenis Isian** | **Keterangan**|
    | ----- | ----- |
    | Device Name | Nama dari perangkat yang akan tampil di *inventory*.|
    | IP Management | Alamat IP dari perangkat yang akan dimonitor.|
    | Probe Name | Nama *probe*  yang bertugas mengambil data dari perangkat.|
    | Forecasting Threshold | Ambang batas yang digunakan untuk kebutuhan *Predictive Analysis*. |
    | Location | Lokasi dari perangkat yang akan dimonitor. |
    | Description | Informasi tambahan terkait perangkat. |
    | Monitoring Options | Pilihan modul yang ingin dimonitoring dari perangkat yang akan didaftarkan.|

5. Klik tombol **Add** untuk menambahkan perangkat baru.
   
6. Setelah menambahkan perangkat baru, klik tombol **Synchronize** untuk mensinkronisasi data perangkat di *Network Device Inventory* dengan *probe* yang bertugas mengambil data dari perangkat jaringan.

    ![Tombol Synchonize](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B10%5Dnetwork-device/tombol-synchronize-v2.png)

7. Selanjutnya, akan muncul kotak dialog seperti berikut:

    ![Form Synchronize](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B10%5Dnetwork-device/form-synchronize-v2.png)

    >**Tips!**
    >Misalkan ada 10 perangkat yang akan ditambahkan. *Add* sebanyak sepuluh perangkat tersebut, baru lakukan sinkronisasi satu kali saja di akhir penambahan.

8. Klik **Synchronize** untuk melanjutkan proses sinkronisasi data.