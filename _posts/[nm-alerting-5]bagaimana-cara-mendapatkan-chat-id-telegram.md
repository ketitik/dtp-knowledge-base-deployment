---
title: 'Bagaimana cara mendapatkan Chat-ID untuk mengaktifkan fitur alerting Netmonk Prime melalui telegram?'
category: 'alerting'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Skenario sederhana yang dapat dimanfaatkan'
---

# Bagaimana cara mendapatkan Chat-ID untuk mengaktifkan fitur alerting Netmonk Prime melalui telegram?

Skenario sederhana yang dapat dimanfaatkan untuk mengaktifkan fitur *alerting* Netmonk Prime melalui telegram adalah dengan membuat sebuah grup yang berisi pihak-pihak yang ingin mendapatkan *alert*. Pada grup tersebut, undang pihak-pihak yang ingin  mendapatkan *alert*, bot telegram yang sudah dibuat [di sini]([nm-alerting-4]bagaimana-cara-membuat-bot-telegram-dan-mendapatkan-bot-token), serta bot dengan username *@id_chat_bot* yang akan menampilkan chat-id seperti contoh berikut:

![Add Members](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B14%5Dalerting/add-members-telegram.png)

Kirim pesan /id, maka bot tersebut akan mengirimkan chat-id dari grup yang sudah dibuat:

![Chat ID Grup Telegram](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B14%5Dalerting/chat-id.png)

Jika Anda membuka telegram melalui *browser*, informasi chat-id dari grup yang dibuat dapat diketahui dengan melihat URL dari *browser*.

![Chat ID Telegram Web](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B14%5Dalerting/chat-id-telegram-web.png)