---
title: 'Berapa lama waktu instalasi Netmonk Prime?'
category: 'panduan-pengguna-baru'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Waktu maksimal instalasi Netmonk Prime'
---

# Berapa lama waktu instalasi Netmonk Prime?

Waktu maksimal instalasi Netmonk Prime adalah 2 hari kerja untuk versi *Free Trial* dan 5 hari kerja untuk versi berbayar.