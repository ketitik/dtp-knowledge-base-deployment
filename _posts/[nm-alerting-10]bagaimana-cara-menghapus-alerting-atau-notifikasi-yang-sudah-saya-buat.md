---
title: 'Bagaimana cara menghapus alerting yang sudah saya buat?'
category: 'alerting'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Akses menu Settings → Alert'
---

# Bagaimana cara menghapus alerting yang sudah saya buat?

1. Akses menu **Settings → Alert**.

    ![Menu Alert](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B14%5Dalerting/menu-alert.png)

2. Dari halaman utama menu *Alert Setting* yaitu *tab* **Alert Notification**, klik tombol **Edit** di salah satu baris data seperti berikut:

    ![Tombol Hapus Alert](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B14%5Dalerting/tombol-hapus-alert.png)

3. Selanjutnya akan tampil kotak dialog seperti berikut:

    ![Hapus Data](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B14%5Dalerting/hapus-data.png)

4. Klik tombol **Delete** untuk melanjutkan proses hapus atau tombol **Cancel** untuk membatalkan.