---
title: 'Di mana saya dapat menemukan daftar penggunaan RAM dari semua perangkat yang termonitor Netmonk Prime?'
category: 'ram-status'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Dari Dashboard Network Summary mode operasional'
---

# Di mana saya dapat menemukan daftar penggunaan RAM dari semua perangkat yang termonitor Netmonk Prime?

Dari **Dashboard Network Summary** mode operasional, klik **View All** pada bagian **RAM Status** atau akses melalui menu **Network Status → RAM Status**. Berikut contoh halaman utama *RAM Status* yang menampilkan daftar penggunaan RAM dari semua perangkat yang termonitor:

![RAM Status](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B5%5Dram-status/ram-status-v2.png)

Keterangan:
a. Rata-rata penggunaan RAM di semua perangkat yang terdeteksi.
b. Jumlah RAM dengan penggunaan di bawah 50% .
c. Jumlah RAM dengan penggunaan di antara 50% s.d 80% .
d. Jumlah RAM dengan penggunaan di atas 80% .
e. *Hostname* yang terkonfigurasi di perangkat.
f. Nama perangkat yang terdaftar di *inventory*.
g. Indeks dari masing-masing RAM.
h. Total kapasitas masing-masing RAM.
i. Persentase penggunaan RAM.
j. Kategori status penggunaan RAM
