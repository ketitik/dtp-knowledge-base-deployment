---
title: 'Di mana saya dapat menemukan username, password, dan alamat Netmonk Prime yang sudah saya order?'
category: 'subscription'
is_sticky: true
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Anda dapat menemukannya di halaman Detail Subscription jika status order sudah teraktivasi.'
---

# Di mana saya dapat menemukan username, password, dan alamat Netmonk Prime yang sudah saya order?

Anda dapat menemukannya di halaman **Detail Subscription** jika status order sudah teraktivasi. Untuk lebih jelasnya, ikuti langkah berikut:

1. Masuk ke halaman *subscription* melalui menu **Subscription** seperti berikut:

    ![Menu Subscription](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/subscription/menu-subscription.png)

2. Klik tombol **Detail** pada data order Netmonk Prime yang bersangkutan. Untuk mendapatkan *username*, *password*, dan alamat Netmonk Prime, pastikan status order sudah teraktivasi

    ![List Subscription](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/subscription/list-subscription.png)

3. Informasi terkait alamat Netmonk Prime serta username dan password yang dapat digunakan pada saat login Netmonk Prime tersebut tertera pada halaman **Detail Subscription** seperti contoh berikut:

    ![Informasi Username dan Password Netmonk Prime](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/subscription/informasi-username-password-nb.png)

Berikut video singkat terkait cara menambahkan subscription Netmonk Prime versi Free Trial serta cara mendapatkan username, password, dan alamat Netmonk Prime yang sudah teraktivasi tersebut:

<iframe width="550" height="300"
src="https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/administration/subscription/alamat-netmonk-prime-username-password-free-trial.mp4">
</iframe>