---
title: 'Di mana saya dapat menemukan daftar semua perangkat yang termonitor Netmonk Prime?'
category: 'network-device'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Dari Dashboard Network Summary mode operasional'
---

# Di mana saya dapat menemukan daftar semua perangkat yang termonitor Netmonk Prime?

Dari **Dashboard Network Summary** mode operasional, klik View All pada bagian Device Status atau akses melalui menu **Network Status → Device Status**. Berikut contoh halaman utama Device Status yang menampilkan daftar seluruh perangkat yang termonitor:

![Device Status](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B10%5Dnetwork-device/device-status-v2.png)

Keterangan:
a. Jumlah perangkat yang berstatus hidup (*Up*).
b. Jumlah perangkat yang berstatus mati (*Down*).
c. *Hostname* yang terkonfigurasi di perangkat.
d. Nama perangkat yang terdaftar di *Network Device Inventory*.
e. Alamat IP dari perangkat yang dimonitor.
f. Status dari masing-masing perangkat, yang dapat dideteksi menggunakan ICMP walaupun SNMP pada perangkat tersebut tidak aktif atau tidak tersedia.
g. Waktu yang menunjukkan sudah berapa lama perangkat hidup.
