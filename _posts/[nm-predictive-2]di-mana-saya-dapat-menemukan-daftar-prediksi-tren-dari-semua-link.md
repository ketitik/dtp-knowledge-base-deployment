---
title: 'Di mana saya dapat menemukan daftar prediksi tren dari semua link/port/interface yang termonitor Netmonk Prime?'
category: 'predictive-analysis'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Akses menu Predictive Analysis → Link'
---

# Di mana saya dapat menemukan daftar prediksi tren dari semua link/port/interface yang termonitor Netmonk Prime?

1. Akses menu **Predictive Analysis → Link**

    ![Menu Predictive Analysis Link](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B12%5Dpredictive-analysis/menu-predictive-analysis-link-v2.png)

2. Maka akan tampil daftar prediksi tren dari semua *link/port/interface* yang termonitor oleh sistem seperti berikut:

    ![Predictive Link](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B12%5Dpredictive-analysis/predictive-link-v2.png)

    Keterangan:

    a. *Hostname* yang terkonfigurasi di perangkat.
    b. Alamat IP dari perangkat yang dimonitor.
    c. Nama setiap *link/port/interface* dari perangkat yang dimonitor.
    d. Kapasitas maksimal yang terkonfigurasi di *interface*.
    f. Ambang batas penggunaan *link/port/interface*.
    g. Jumlah hari yang menunjukkan berapa lama lagi trafik atau penggunaan *link/port/interface* akan mencapai ambang batas. Jika tren diprediksi menurun, keterangan yang muncul adalah *downward trend*.