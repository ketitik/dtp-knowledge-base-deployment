---
title: 'Bagaimana cara mengedit data Network Map yang sudah saya daftarkan?'
category: 'network-map'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Akses menu Dashboard → Network Map'
---

# Bagaimana cara mengedit data Network Map yang sudah saya daftarkan?

1. Akses menu **Dashboard → Network Map**

    ![Menu Dashboard Map](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B8%5Dnetwork-map/menu-network-map.png)

2. Dari halaman utama *Dashboard Network Map*, klik tombol **Edit** di salah satu baris data *map*,

    ![Tombol Edit Network Map](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B8%5Dnetwork-map/tombol-edit-network-map.png)

3. Selanjutnya akan muncul tampilan seperti berikut:

    ![Form Edit Network Map](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B8%5Dnetwork-map/form-edit-network-map.png)

4. Edit nama *map* dan deskripsinya pada *form* berikut:

    ![Form Name dan Description](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B8%5Dnetwork-map/form-name-description.png)

5. Tambahkan *background* jika masih kosong, atau jika ingin mengganti *background* yang sudah ada, klik tombol **Change Background**.

    ![Tombol Change Background](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B8%5Dnetwork-map/tombol-change-background.png)

6. Selanjutnya akan muncul tampilan seperti berikut:

    ![Form Change Background](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B8%5Dnetwork-map/form-change-background.png)

7. *Drag* dan *drop* gambar atau klik **browse**, selanjutnya pilih gambar sesuai kebutuhan dan klik tombol **Open**.
   
8. Setelah memilih *background*, tambahkan *node* atau perangkat yang dimonitor ke dalam *map* dengan menekan tombol **Node**.

    ![Add Node](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B8%5Dnetwork-map/add-node.png)

9.  Tempatkan *node* tersebut pada titik koordinat tertentu pada area *frame* yang telah disediakan.

    ![Frame Add Node](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B8%5Dnetwork-map/frame-add-node.png)

10. Kemudian klik pada masing-masing *node* untuk menentukan tipe *node* dan nama dari *node* tersebut, atau untuk menghapus *node* yang sudah ditambahkan seperti berikut:

    ![Form Ubah Node Type](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B8%5Dnetwork-map/form-ubah-tipe-node.png)

11. Selanjutnya, tambahkan *edge* untuk menghubungkan *node* atau perangkat yang satu dengan yang lainnya dengan menekan tombol **Edge**.

    ![Tombol Add Edge](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B8%5Dnetwork-map/tombol-add-edge.png)

12. Pilih *source node* sebagai titik awal, kemudian pilih *target node* atau titik akhir dari *edge*. Berikut contoh hasil penambahan *edge* yang menghubungkan dua *node* yang sudah dibuat sebelumnya:

    ![Frame Add Edge](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B8%5Dnetwork-map/frame-add-edge.png)

13. Untuk melihat detail *map* dalam format *JSON*, klik tombol berikut:

    ![JSON Editor View](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B8%5Dnetwork-map/json-editor-view.png)

14. Tampilan *preview map* akan beralih ke **Json Editor View** seperti berikut:

    ![Form Edit Map JSON](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B8%5Dnetwork-map/form-edit-map-json-view.png)

15. Untuk kembali ke tampilan **Map Editor View**, klik tombol berikut:

    ![Map Editor View](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B8%5Dnetwork-map/ubah-ke-map-editor-view.png)

16. Terakhir, tekan tombol **Save Change** untuk menyimpan perubahan yang telah dilakukan.

    ![Tombol Save Change](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B8%5Dnetwork-map/tombol-save-change.png)