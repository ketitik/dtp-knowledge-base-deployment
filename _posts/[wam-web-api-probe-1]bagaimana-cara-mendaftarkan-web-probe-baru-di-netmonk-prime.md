---
title: 'Bagaimana cara mendaftarkan web/API probe baru di Netmonk Prime?'
category: 'web-api-probe'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Akses modul Web/API Monitoring → Web/API Probe'
---

# Bagaimana cara mendaftarkan web/API probe baru di Netmonk Prime?

1. Akses modul **Web/API Monitoring → Web/API Probe**.
   
    ![Menu Web Probe](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/web-api-monitoring/%5B6%5Dweb-api-probe/menu-web-probe-v2.png)

2. Dari halaman utama *Web/API Probe Inventory*, klik tombol **Add Web/API Probe**,

    ![Tombol Tambah Web Probe](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/web-api-monitoring/%5B6%5Dweb-api-probe/tombol-tambah-web-probe.png)

3. Selanjutnya akan muncul *form* untuk mengisi detail *web/API probe* yang akan ditambahkan.

    ![Form Tambah Web Probe](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/web-api-monitoring/%5B6%5Dweb-api-probe/form-tambah-web-probe.png)

4. Isi data pada *form* tersebut dengan rincian sebagai berikut:

    | **Jenis Isian** | **Keterangan** |
    | ----------- | ----------- |
    | Probe Name  | Nama dari *web/API probe* yang akan bertugas mengambil data dari *website* atau *API* yang dimonitor.|
    | Message Broker Address | Alamat *message broker* yang bertugas mengatur antrian dan lalu lintas data antara servis Netmonk Prime. Secara *default*, alamat *message broker* yaitu sebagai berikut: `message-broker.internal.netmonk.id:9092` |
    | Location | Lokasi dari *web/API probe* yang akan ditambahkan. |

5. Klik tombol **Add Data** untuk menambahkan *web/API probe* baru.