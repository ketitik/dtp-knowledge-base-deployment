---
title: 'Bagaimana cara menambahkan link/port/interface baru yang ingin ditambahkan sebagai starred link?'
category: 'starred-link'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Akses menu menu Network Status → Starred Link'
---

# Bagaimana cara menambahkan link/port/interface baru yang ingin ditambahkan sebagai starred link?

1. Akses menu menu **Network Status → Starred Link**

    ![Menu Starred Link](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B3%5Dstarred-link/menu-starred-link-v2.png)

2. Dari halaman utama menu *Starred Link*, klik tombol **Add New Starred Link**

    ![Tombol Add New Starred Link](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B3%5Dstarred-link/tombol-tambah-starred-link.png)

3. Selanjutnya akan muncul *form* untuk menambah *starred link* baru berikut:

    ![Form Add Starred Link](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B3%5Dstarred-link/form-tambah-starred-link.png)

4. Pilih alamat IP dari perangkat yang dimonitor pada kolom **IP Management**, kemudian pilih *link/port/interface* yang ingin dikategorikan sebagai *starred link* dari perangkat tersebut pada kolom **Interface** seperti contoh berikut:

    ![Pilih Interface](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B3%5Dstarred-link/form-pilih-interface-starred-link.png)

5. Klik tombol **Add** untuk menambahkan *starred link* baru.