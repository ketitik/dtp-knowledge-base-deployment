---
title: 'Apakah saya bisa mendaftarkan banyak perangkat sekaligus?'
category: 'network-device'
is_sticky: false
date: '2020-03-16T05:35:07.322Z'
excerpt: 'Akses menu Network Inventory → Network Device'
---

# Apakah saya bisa mendaftarkan banyak perangkat sekaligus?

1. Akses menu **Network Inventory → Network Device**

    ![Menu Network Device](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B10%5Dnetwork-device/menu-network-device-v2.png)

2. Dari halaman utama *Network Device Inventory*, klik tombol **Add Network Device**.

    ![Tombol Tambah Network Device](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B10%5Dnetwork-device/tombol-tambah-network-device.png)

3. Setelah muncul formulir untuk mengisi detail data perangkat, jika ingin menambahkan banyak data sekaligus, pindah ke *tab* **Add Multiple Node** seperti berikut:

    ![Form Tambah Multiple Network Device](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B10%5Dnetwork-device/form-tambah-multiple-network-device.png)

4. Siapkan file CSV yang berisi daftar perangkat baru yang akan ditambahkan ke *inventory* untuk dimonitor. Untuk mengunduh contoh format filenya, klik teks **Click Download Template** berikut:

    ![Download Template](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B10%5Dnetwork-device/click-download-template.png)

5. Jika file CSV sudah siap, klik **Browse File** dan pilih file tersebut. Berikut contoh tampilan setelah memilih file:

    ![Browse File](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B10%5Dnetwork-device/after-browse-file.png)

6. Klik **Upload**, maka semua data perangkat dari file .csv di atas akan tampil di halaman *Network Device Inventory*.
   
7. Selanjutnya, klik tombol **Synchronize** untuk mensinkronisasi data daftar perangkat yang baru ditambahkan tersebut dengan *network probe* yang bertugas mengambil data dari perangkat jaringan.

    ![Tombol Synchronize](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B10%5Dnetwork-device/tombol-synchronize-v2.png)

8. Setelah muncul kotak dialog berikut, klik **Synchronize** untuk melanjutkan proses sinkronisasi data.
   
   ![Form Synchronize](https://netmonk-public.s3.ap-southeast-1.amazonaws.com/knowledge-center/network-monitoring/%5B10%5Dnetwork-device/form-synchronize-v2.png)