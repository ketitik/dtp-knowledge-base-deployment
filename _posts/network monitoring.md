---
title: "Network Monitoring"
accordion: "network monitoring"
category: 'network-monitoring'
icon: website-probe
is_sticky: false
date: "2020-01-02"
excerpt: 'Data apa saja yang ditampilkan di dashboard network monitoring?'
---

## DASHBOARD

Data apa saja yang ditampilkan di dashboard network monitoring?

Bagaimana jika saya ingin melihat dashboard dengan mode "manajerial"?

Mengapa perangkat yang sudah saya daftarkan di SNMP Device Inventory tidak muncul di dashboard?

## LINK UTILIZATION

Dimana saya dapat menemukan daftar semua link/port/interface network yang termonitor?

Bagaimana cara melihat detail penggunaan dari link/port/interface tertentu?

Mengapa jumlah link/port/interface yang tercantum pada counter halaman utama menu Link Utilization berbeda dengan jumlah keseluruhan yang terdeteksi oleh Netmonk Basic?

## STARRED LINK

Apa itu starred link?

Dimana saya dapat menemukan daftar semua link/port/interface yang dikategorikan sebagai starred link?

Bagaimana cara melihat detail penggunaan dari link/port/interface yang dikategorikan sebagai starred link?

[13] Bagaimana cara menghapus starred link yang sudah saya daftarkan?

## CPU STATUS

[14] Dimana saya dapat menemukan daftar penggunaan CPU dari semua perangkat yang termonitor Netmonk Basic?

[15] Bagaimana cara melihat detail penggunaan CPU tertentu?

## RAM STATUS

[16] Dimana saya dapat menemukan daftar penggunaan RAM dari semua perangkat yang termonitor Netmonk Basic?

[17] Bagaimana cara melihat detail penggunaan RAM tertentu?

## STORAGE STATUS

[18] Dimana saya dapat menemukan daftar penggunaan Storage atau media penyimpanan dari semua perangkat yang termonitor Netmonk Basic?

[19] Bagaimana cara melihat detail penggunaan Storage tertentu?

## DISK IO STATUS

[20] Dimana saya dapat menemukan daftar penggunaan Disk dari semua server yang termonitor Netmonk Basic?

[21] Bagaimana cara melihat detail penggunaan Disk tertentu?

## NETWORK MAP

[1] Apa itu Dashboard Network Map?

[2] Bagaimana cara mendaftarkan Network Map baru?

[3] Bagaimana cara mengedit data Network Map yang sudah saya daftarkan?

[4] Bagaimana cara menghapus Network Map yang sudah saya buat?

## LOCATION

[1] Bagaimana cara menambahkan data lokasi baru untuk perangkat yang akan di monitor?

[2] Bagaimana cara menghapus data lokasi yang sudah saya daftarkan?

## SNMP Device

[1] Bagaimana cara mendaftarkan perangkat baru yang akan dimonitor?

[2] Apakah saya bisa mendaftarkan banyak perangkat sekaligus?

[3] Bagaimana cara melihat detail perangkat yang sudah saya daftarkan?

[4] Dimana saya dapat menemukan daftar semua perangkat network yang termonitor?

[5] Bagaimana cara melihat detail status dari perangkat network yang dimonitor?

[6] Bagaimana cara mengubah periode waktu untuk menampilkan data Network Monitoring sesuai dengan kebutuhan saya?

## SNMP Probe

[1] Bagaimana cara mendaftarkan probe baru unuk memonitor perangkat saya?

[2] Bagaimana cara melihat detail probe yang digunakan untuk memonitor perangkat saya?

[3] Bagaimana cara mengedit data SNMP probe yang sudah saya daftarkan?

[4] Bagaimana cara menghapus SNMP probe yang sudah saya daftarkan?

## PREDICTIVE ANALYSIS

[1] Apa itu Predictive Analysis?

[2] Dimana saya dapat menemukan daftar prediksi tren dari semua link/port/interface yang termonitor Netmonk Basic?

[3] Bagaimana cara melihat detail prediksi tren dari link/port/interface tertentu?

[4] Dimana saya dapat menemukan daftar prediksi tren penggunaan semua indeks memori yang termonitor Netmonk Basic?

[5] Bagaimana cara melihat detail prediksi tren penggunaan memori tertentu?

[6] Dimana saya dapat menemukan daftar prediksi tren penggunaan semua indeks storage yang termonitor Netmonk Basic?

[7] Bagaimana cara melihat detail prediksi tren penggunaan storage tertentu?

[8] Bagaimana cara mengubah periode waktu untuk menampilkan data prediksi tren sesuai dengan kebutuhan saya?

## REPORTING

[1] Data apa saja yang disajikan dalam reporting Netmonk Basic?

[2] Bagaimana cara membuat dan mengunduh reporting untuk perangkar yang dimonitor?

[3] Bagaimana cara menambahkan link/port/interface baru yang ingin dibuat menjadi laporan?

[4] Bagaimana cara data link/port/interface yang akan dibuat menjadi laporan?

[5] Bagaimana cara menghapus interface reporting tertentu?

[6] Bagaimana cara membuat dan mengunduh reporting untuk interface tertentu?

## ALERTING

[1] Data apa saja yang dapat dikirim sebagai alerting?

Alerting dikirimkan menggunakan apa?

Bisakah menambah opsi alerting selain telegram dan email?

[2] Bagaimana cara membuat bot telegram dan mendapatkan bot token untuk membuat alerting atau notifikasi melalu telegram?

[3] Bagaimana cara mendapatkan Chat-ID untuk mengaktifkan fitur alerting atau notifikasi Netmonk Basic melalui teleram?

[4] Bagaimana cara membuat notifikasi melalui bot telegram yang sudah dibuat?

[5] Bagaimana cara membuat notifikasi melalui email?

[6] Bagaimana cara mengedit data notifikasi yang sudah saya buat?

[7] Bagaimana cara mengedit rules atau aturan-aturan yang ditetapkan untuk suatu notifikasi?

[8] Bagaimana cara menghapus alerting atau notifikasi yang sudah saya buat?